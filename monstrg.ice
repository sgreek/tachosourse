// **********************************************************************
// Monitoring storage interface
// Copyright (c) 2013
// **********************************************************************

#ifndef STORAGE_ICE
#define STORAGE_ICE

#include "monitoring.ice"

module Monitoring
{
	/*************************************************************************************/
	/* Query options bits */

	const int StorageOptionTime =		0x0001;	// Mark for time collections
	const int StorageOptionDetail =		0x0002;	// Mark for master-detail link by itemid
	const int StorageOptionPartial =    0x0004; // Enable partial data return
	const int StorageOptionAnalize =	0x0008;	// Analize with master configuration
	const int StorageOptionFirst =		0x0100; // Select first record from range
	const int StorageOptionLast =		0x0200; // Select last record from range

	/*************************************************************************************/

    struct StorageQuery
	{
		int options;				// Any options, see below StorageOptionXXX constants
		int count;					// Count of items to receive (0 for all request), or period on time request
		int code;					// Requested item with code
		double time;				// Request only with more modified time (unix time in secconds)
		float interval;				// Request interval (unix time in secconds)
		string userid;				// Authorized user id or empty
		ParamArray params;			// Result params/props or empty for all
		Properties props;			// Requested items filter
	};

    struct StorageResult
	{
		int options;				// Any additional options
		int errcode;				// Error code or 0
		string errmsg;				// Error message
		double time;				// Modified time (unix time in secconds)
		ItemArray items;			// Result items array
	};

    interface Storage
	{
        ["ami", "amd", "cpp:const"] idempotent string info(optional(1) string param);
        ["ami", "amd"] idempotent string format(string name, optional(1) StorageQuery qry, optional(2) string fmt) throws Error;			
        ["ami", "amd"] idempotent StorageResult select(string name, optional(1) StorageQuery qry);	
        ["ami", "amd"] idempotent StorageResult update(string name, optional(1) StorageQuery qry, optional(2) ItemArray items);	
        ["ami", "amd"] idempotent StorageResult append(string name, optional(1) StorageQuery qry, optional(2) ItemArray items);	
        ["ami", "amd"] idempotent StorageResult remove(string name, optional(1) StorageQuery qry);	
	};

	/*************************************************************************************/
};

#endif
