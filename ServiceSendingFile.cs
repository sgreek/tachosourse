﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

//сервис для отправки данных
namespace TachoSource
{
    //ServiceSendingFile(SSF) - сервис отсылки файлов
    public class ServiceSendingFile
    {
        //возможные ошибки
        static public string ErrorMsgPerCode(int code)
        {
            switch(code)
            {
                case Monitoring.EFail.value:
                    return "Fail";
                case Monitoring.EConnect.value:
                    return "Connect";
                case Monitoring.ENotFound.value:
                    return "NotFound";
                case Monitoring.EInvalidArg.value:
                    return "InvalidArg";
                case Monitoring.ENotImpl.value:
                    return "NotImpl";
                case Monitoring.EDatabase.value:
                    return "Database";
                case Monitoring.EIncomplete.value:
                    return "Incomplete";
                case Monitoring.EInvalidTime.value:
                    return "InvalidTime";
                case Monitoring.ESession.value:
                    return "Session";
                case Monitoring.EAuthorize.value:
                    return "EAuthorize";
                case Monitoring.EInitialize.value:
                    return "Initialize";
                case Monitoring.EAccessDenied.value:
                    return "AccessDenied";
                case Monitoring.EWrongFileFormat.value:
                    return "WrongFileFormat";
                case Monitoring.EWrongFileAccess.value:
                    return "WrongFileAccess";
                case Monitoring.ESaveToDB.value:
                    return "SaveToDB";
                default:
                    return "Unknown error code";
            }
        }
        public enum CodeOfError
        {
            // ошибка
            Error = -1,
            // ошибка конекта к 
            ConnectError = -2,
            //     
            InitializeError = -3,
            //     
            FileNotExist = -4,
            //
            ReadDataFromFile = -5,
            //     
            SendDataToService = -6,
            //     
            GetFileInfo = -7,
            //     No error. (SCARD_S_SUCCESS)
            Success = 0,
        }

        static public bool FlagInStateStorageItem(int flag, int status)
        {
            int partStts = (status & Monitoring.SttsMask.value);
            return  partStts == flag;
            /*
            const int SttsMask = 0x00ff;		//маска состояния добавления файла в систему
            const int SttsDef = 0;				//закачка
            const int SttsProcessed = 0x0001;	//парсинг - сохранение в базу
            const int SttsWithError = 0x0002;	//парсинг - сохранение в базу
            const int SttsLoadedOk = 0x0003;
            /**/

        }
        public class ResultInfo
        {
            public int code = 0;
            public string file = "";
            public string msg = "";
            public Monitoring.FileInfo stInfo = null;
            public ResultInfo(string fileA, int codeA, Monitoring.FileInfo objRes)
            { file = fileA; code = codeA; stInfo = objRes; }
        }
        public class SSF_Exception : Exception
        {
            public CodeOfError codeOfError { get; set; }
            public SSF_Exception () { }

            public SSF_Exception(string message, CodeOfError code)
                : base(message) { codeOfError = code; }
            public SSF_Exception(string message, System.Exception inner, CodeOfError code)
                : base(message, inner) { codeOfError = code; }

            public SSF_Exception(string message)
                : base(message) { codeOfError = CodeOfError.Success; } 
            public SSF_Exception(string message, System.Exception inner)
                : base(message, inner) { codeOfError = CodeOfError.Success; }
            protected SSF_Exception(System.Runtime.Serialization.SerializationInfo info,  System.Runtime.Serialization.StreamingContext context)
                : base(info, context) { codeOfError = CodeOfError.Success; } 
        }

        //события
        public delegate void Message(string category, string msg, string file);
        public delegate void SendProgress(int progressPercentage, object userState, string file);
        public delegate void SendCompleted(ResultInfo result, Exception error, bool cancelled, string file);

        public event Message OnMessage;
        public event SendProgress OnSendProgress;
        public event SendCompleted OnSendCompleted;


        //...
        private readonly short CHUNK_SIZE = 10000;
        private Ice.Communicator communicatorPtr;
        private Monitoring.FileStoragePrx storage;
        public void Init(Ice.Communicator communicator) 
        {
            if (OnMessage != null) OnMessage("SSF", "Init....", "");
            communicatorPtr = communicator;
        }

        public void Done()
        {
            if (OnMessage != null) OnMessage("SSF", "Done", "");
        }

        public Monitoring.FileInfo GetFileInfo(string idAT, string fileName)
        {
            //получаем информацию о состоянии загруженного файла
            Monitoring.FileInfo sttInfo = null;
            if (storage == null)
            {
                if (OnMessage != null) OnMessage("SSF", "GetFileInfo. Storage == NULL", fileName);
                return sttInfo;
            }
            if (OnSendProgress != null) OnSendProgress(0, "GetFileInfo", fileName);
            try
            {
                sttInfo = storage.getInfo(idAT, fileName);
            }
            catch (Exception ex)
            {
                if (OnSendCompleted != null) OnSendCompleted(new ResultInfo(fileName, (int)CodeOfError.GetFileInfo, null), ex, false, fileName);
                throw new SSF_Exception("Could not get file info from service.", ex, CodeOfError.GetFileInfo);
            }

            return sttInfo;
        }

        public void SendFile(string idAT, string fileName)
        {
            string name = PreSendFile(fileName);
            int curPos = 0;
            try
            {
                byte totalProgress1 = 0;
                byte totalProgress2 = 0;
                if (OnSendProgress != null) OnSendProgress(0, "prepare sending", fileName);
                Monitoring.FileInfo sttItem = null;

                //отправляем файл кусочками
                using (FileStream fstream = File.OpenRead(fileName))
                {
                    while (curPos < fstream.Length)
                    {
                        //читаем порцию данных из файла
                        int chunk = ((curPos + CHUNK_SIZE) < fstream.Length) ? (int)CHUNK_SIZE : (int)(fstream.Length - curPos);
                        byte[] arrayByte = new byte[chunk];
                        try
                        {
                            fstream.Read(arrayByte, 0, arrayByte.Length);
                        }
                        catch (Exception ex)
                        {
                            if (OnSendCompleted != null) OnSendCompleted(new ResultInfo(fileName, (int)CodeOfError.ReadDataFromFile, null), ex, false, fileName);
                            throw new SSF_Exception("Could not read file.", ex, CodeOfError.ReadDataFromFile);
                        }
                        totalProgress1 = (byte)(((50 * curPos)/fstream.Length));
                        if (OnSendProgress != null) OnSendProgress(totalProgress1 + totalProgress2, String.Format("{0}:end sending", fileName), fileName);

                        curPos += chunk;
                        //отправляем данные
                        try
                        {
                            int byteWrite = -1;
                            int res = storage.writeFile(idAT, name, CHUNK_SIZE, arrayByte, out byteWrite, out sttItem);
                        }
                        catch (Exception ex)
                        {
                            if (OnSendCompleted != null) OnSendCompleted(new ResultInfo(fileName, (int)CodeOfError.SendDataToService, sttItem), ex, false, fileName);
                            throw new SSF_Exception("Could not send block to service.", ex, CodeOfError.SendDataToService);
                        }
                        totalProgress2 = totalProgress1;
                        if (OnSendProgress != null) OnSendProgress(totalProgress1 + totalProgress2, String.Format("{0}:chunk sending", fileName), fileName);
                    }
                }
                if (OnSendProgress != null) OnSendProgress(100, String.Format("{0}:end sending", fileName), fileName);
                if (OnSendCompleted != null) OnSendCompleted(new ResultInfo(fileName, (int)CodeOfError.Success, sttItem), null, false, fileName);

            }
            catch (Exception ex)
            {
                throw new SSF_Exception("ERROR send file", ex, CodeOfError.Error);
            }
            OnMessage("SSF", "Send file: is Ok", fileName);
        }

        public void SendFile_Async(string idAT, string fileName)
        {
            string name = PreSendFile(fileName);
            int curPos = 0;
            if (OnSendProgress != null) OnSendProgress(0, "prepare sending", fileName);

            SendChunk_Async(idAT, fileName, name, curPos, 0, null);
        }

        //Асинхронная отправка файлов
        private void SendChunk_Async(string idAT, string fileName, string name, int currPos, int errCode, Monitoring.FileInfo sstItem)
        {
            if (errCode < 0) {
                if (OnSendCompleted != null) OnSendCompleted(new ResultInfo(fileName, errCode, sstItem), null, false, fileName);
                return;
            }
            //читаем порцию данных из файла
            long fileLength = -1;
            byte[] arrayByte = null;
            try
            {
                arrayByte = GetChuckFile(fileName, currPos, ref fileLength);
            }
            catch (Exception ex)
            {
                if (OnSendCompleted != null) OnSendCompleted(new ResultInfo(fileName, (int)CodeOfError.ReadDataFromFile, null), ex, false, fileName);
                throw new SSF_Exception("Could not read file.", ex, CodeOfError.ReadDataFromFile);
            }


            //отправляем данные
            int chunk = arrayByte != null ? arrayByte.Length : 0;
            bool haveData = (chunk > 0);
            if (haveData)
            {
                byte progressFile = (byte)(((50 * currPos) / fileLength));
                byte progressSrv = 0;
                if (OnSendProgress != null) OnSendProgress(progressFile + progressSrv, String.Format("sending: {0}% ", (100 * currPos) / fileLength), fileName);

                currPos += chunk;
                storage.begin_writeFile(idAT, name, CHUNK_SIZE, arrayByte).whenCompleted(
                        (int resCode, int byteWrite, Monitoring.FileInfo objInfo) =>
                        {
                            progressSrv = progressFile;
                            if (OnSendProgress != null) OnSendProgress(progressFile + progressSrv, "was sent", fileName);
                            SendChunk_Async(idAT, fileName, name, currPos, resCode, objInfo);
                        },
                        (Ice.Exception ex) =>
                        {
                            if (OnSendCompleted != null) OnSendCompleted(new ResultInfo(fileName, (int)CodeOfError.Error, null), ex, false, fileName);
                            throw new SSF_Exception("Could not send block to service.", ex, CodeOfError.ReadDataFromFile);
                        }
                    );
            }
            else {
                if (currPos > 0) 
                {
                    if (OnSendProgress != null) OnSendProgress(100, String.Format("end sending:{0}", name), fileName);
                    if (OnSendCompleted != null) OnSendCompleted(new ResultInfo(fileName, (int)CodeOfError.Success, sstItem), null, false, fileName);
                }

            }
        }

        private byte[] GetChuckFile(string fileName, int curPos, ref long fileLength)
        {

            using (FileStream fstream = File.OpenRead(fileName))
            {
                if (curPos >= fstream.Length) return null;

                fileLength = fstream.Length;

                fstream.Seek(curPos, SeekOrigin.Begin); // curPos символов с начала потока

                //читаем порцию данных из файла
                int chunk = ((curPos + CHUNK_SIZE) < fstream.Length) ? (int)CHUNK_SIZE : (int)(fstream.Length - curPos);
                byte[] arrayByte = new byte[chunk];
                try
                {
                    fstream.Read(arrayByte, 0, arrayByte.Length);
                }
                catch (Exception ex)
                {
                    if (OnSendCompleted != null) OnSendCompleted(new ResultInfo(fileName, (int)CodeOfError.ReadDataFromFile, null), ex, false, fileName);
                    throw new SSF_Exception("Could not read file.", ex, CodeOfError.ReadDataFromFile);
                }
                return arrayByte;
            }
        }
        //Асинхронная отправка файлов
        //---------------------------------------------------------------
        private void InitStorage()
        {
            if (storage != null) return;
            string namePrx = communicatorPtr.getProperties().getProperty("Default.FileStorage");
            try
            {
                storage = Monitoring.FileStoragePrxHelper.uncheckedCast(communicatorPtr.stringToProxy(namePrx));
            }
            catch (Exception ex)
            {
                OnMessage("SSF", String.Format("Error init file storage: {0} ", ex), "");
            }
        }

        private string PreSendFile(string fileName)
        {
            OnMessage("SSF", "Send file begin", fileName);
            InitStorage();
            if (storage == null)
            {
                throw new SSF_Exception("Not object storage", CodeOfError.InitializeError);
            }
            FileInfo fileInf = new FileInfo(fileName);
            if (!fileInf.Exists)
            {
                throw new SSF_Exception(String.Format("File is not exist"), CodeOfError.FileNotExist);
            }

            return Path.GetFileName(fileName);
        }


    }


}
