﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

using System.Threading;
using System.ComponentModel;

//PCSCWorker обертка над BackgroundWorker – для считывания данных с карты в отдельном рабочем потоке
namespace TachoSource
{
    public class PCSCWorker : BackgroundWorker
    {
            //
        private readonly object s_lock = new object();

        public class PCSC_WorkerInfo //Это класс, который хранит данные
        {
            public static uint PROGRESS = 1;    //прогресс
            public static uint INITIALIZE = 2;  //данные инициализации в сообщении

            public string readerName;
            public uint code;
            public string message;
            public PCSC_WorkerInfo() { }
            public PCSC_WorkerInfo(string reader, string msg) { readerName = reader; code = PROGRESS; message = msg; }
            public PCSC_WorkerInfo(string reader, uint cd, string msg) { readerName = reader; code = cd; message = msg; }
            public override string ToString(){ return message; }
            // Явное преобразование типа к string
            public static explicit operator string(PCSC_WorkerInfo obj)
            {
                return obj.ToString();
            }

        }
        private MediatorPCSC m_pcscPtr;
        public string ReaderName { get; set; }
        public byte TypeCard { get; set; }
        public string storagePath_ = "";
        public string StoragePath { 
            get
            {
                string tmp = "";
                Monitor.Enter(s_lock);
                tmp = storagePath_;
                Interlocked.Exchange(ref tmp, storagePath_);
                Monitor.Exit(s_lock);
                return tmp;
            } 
            set
            {
                Monitor.Enter(s_lock);
                Interlocked.Exchange(ref storagePath_, value);
                Monitor.Exit(s_lock);
            } 
        }

        private byte currProgress_;
        private byte chankProgress_;

        public PCSCWorker()
        {
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
        }
        public void SetPCSC(ref MediatorPCSC ptr)
        {
            m_pcscPtr = ptr;
        }

        protected override void OnDoWork(DoWorkEventArgs e)
        {
            //e.Argument
            currProgress_ = 0;
            chankProgress_ = 100;
            DataSmartCardToDDD(e);
            /*            
                        ReportProgress(0, "идентифицируем карточку...");
                        byte percentComplete = 0 .. 10;
                           if (CancellationPending) 
                           {
                                e.Cancel = true;
                                return;
                           }
                        ReportProgress(10..70, "читаем данные...");
                           if (CancellationPending) 
                           {
                                e.Cancel = true;
                                return;
                           }

                        ReportProgress(70..100, "записываем файл...");
                        ReportProgress(100, "Готово!");
                        e.Result = "completed data";
            /**/
        }

        private int DataSmartCardToDDD(DoWorkEventArgs e)
        {
            int res = 0;
            //0..10
            //ОПРЕДЕЛЯЕМ тип - иденитифицируем карту
            ReportProgress(currProgress_, new PCSC_WorkerInfo(ReaderName, "Begin..."));
            byte typeCard = SmartCardRec.СT_RESERVED;
            try
            {
                m_pcscPtr.ConnectCard(ReaderName);
                ReportProgress(5, new PCSC_WorkerInfo(ReaderName, "Card identification..."));

                SmartCardRec.block_record[] smartCard;
                res = IdentificationSmartCard(ReaderName, out smartCard);
                if (res < 0)
                {
                    GlobalLog.warning(String.Format("DataSmartCardToDDD:error read header; reader:{0}", ReaderName));
                    e.Result = "Error identification card (read header)";
                    return res;
                }
                typeCard = SmartCardRec.GetCardType(smartCard);
            }
            catch (Exception ex)
            {
                GlobalLog.error(String.Format("DataSmartCardToDDD: {0}", ex.Message));
                e.Result = "Error: " + ex.Message;
                return -1;
            }
            finally
            {
                m_pcscPtr.DisonnectCard();
            }

            if (typeCard != SmartCardRec.СT_DRIVER)
            {
                ReportProgress(5, new PCSC_WorkerInfo(ReaderName, PCSC_WorkerInfo.INITIALIZE, "Unknown card type"));
                GlobalLog.warning(String.Format("DataSmartCardToDDD:unknown card type-{0}; reader:{1}", typeCard, ReaderName));
                e.Result = String.Format("Unknown card type-{0}", typeCard);
                return -1;
            }

            if (CancellationPending)
            {
                e.Result = "Reading cancel";
                e.Cancel = true;
                return 0;
            }

            //PIN
            if (m_pcscPtr.DataPIN.NeedCheck()) 
            {
                string resStr = "";
                int counter = -1;
                if (m_pcscPtr.DataPIN.GetResult(ReaderName, out counter, out resStr) != 0)
                {
                    e.Result = "Reading cancel";
                    e.Cancel = true;
                    return 0;
                }

            }

            //10..70
            // ЧИТАЕМ данные
            currProgress_ = 10;
            chankProgress_ = 60;
            ReportProgress(currProgress_, new PCSC_WorkerInfo(ReaderName, "Read data from card..."));

            GlobalLog.trace("DataSmartCardToDDD", String.Format("Type of card: {0} ", TypeCard));
            SmartCardRec.block_record[] dataCard = null;
            try
            {
                m_pcscPtr.ConnectCard(ReaderName);
                if (m_pcscPtr.DataPIN.NeedCheck()) m_pcscPtr.EnterPin();
                SmartCardRec.block_record[] driverCard;
                res = ReadDriverCard(ReaderName, out driverCard);
                if (res < 0)
                {
                    GlobalLog.warning(String.Format("DataSmartCardToDDD:data format is not valid; reader:{0}", ReaderName));
                    e.Result = "data format is not valid";
                    return -1;
                }
                dataCard = driverCard;
            }
            catch (Exception ex)
            {
                GlobalLog.error(String.Format("DataSmartCardToDDD: {0}", ex.Message));
                e.Result = "Error: " + ex.Message;
                return -1;
            }
            finally
            {
                m_pcscPtr.DisonnectCard();
            }


            if (CancellationPending)
            {
                e.Cancel = true;
                e.Result = "Reading cancel";
                return 0;
            }

            //70..100
            //записываем данные на диск
            currProgress_ += chankProgress_;
            chankProgress_ = 30;
            ReportProgress(currProgress_, new PCSC_WorkerInfo(ReaderName, "Save data to file..."));

            SmartCardRec.IdentificationInfo idntInf = SmartCardRec.GetCaptionDriverCard(dataCard);
            string suffixFileName = (idntInf != null) ? idntInf.ToString() : "";
            string filePath;
            res = SaveSmartCardToFile(dataCard, suffixFileName, out filePath);
            e.Result = filePath;
            ReportProgress(100, new PCSC_WorkerInfo(ReaderName, "Complete ..."));
            return res;
        }
        private int IdentificationSmartCard(string rName, out SmartCardRec.block_record[] smartCard)
        {
            int res = 0;
            smartCard = SmartCardRec.GetIndentificationTemplate();
            for (var i = 0; i < smartCard.Length; i++)
            {
                SmartCardRec.block_record blockItm = smartCard[i];
                GlobalLog.trace("IdentificationSmartCard", String.Format("block name: {0} ", blockItm.name));
                byte[] data;
                try
                {

                    m_pcscPtr.GetFile(rName, blockItm.idBlock, blockItm.sizeBlock, blockItm.sizeRecord, out data);
                }
                catch (Exception ex)
                {
                    GlobalLog.error(ex.Message);
                    res = -1;
                    break;
                }
                blockItm.data = data;
            }
            return res;
        }

        //читаем данные с карточки водителя
        public int ReadDriverCard(string rName, out SmartCardRec.block_record[] driverCard)
        {
            byte[] blockIdentity = new byte[] {0x05, 0x20};
            int res = 0;
            driverCard = (TypeCard == SmartCardRec.TYPE_WITH_GOST) ? SmartCardRec.GetDriverCardTemplate() : SmartCardRec.GetDriverCardTemplateESTR();
            for (var i = 0; i < driverCard.Length; i++)
            {
                //progress
                ReportProgress(currProgress_ + (i * chankProgress_) / driverCard.Length, new PCSC_WorkerInfo(ReaderName, "Read data..."));

                SmartCardRec.block_record blockItm = driverCard[i];
                GlobalLog.trace("ReadDriverCard", String.Format("block name: {0} ", blockItm.name));
                byte[] data = null;
                //читаем подпись
                if (blockItm.isSgn)//(blockItm is SmartCardRec.block_signature)
                {
                    if (m_pcscPtr.DataPIN.NeedCheck())
                    {
                        try
                        {
                            m_pcscPtr.GetSignature(rName, blockItm.idBlock, blockItm.sizeBlock, blockItm.sizeRecord, out data);
                        }
                        catch (Exception ex)
                        {
                            GlobalLog.error(ex.Message);
                            res = -1;
                            break;
                        }
                        /*
                        if (m_pcscPtr.State != MediatorPCSC.ST_READY)
                        {
                            m_pcscPtr.Stop();
                            m_pcscPtr.Start();
                        }
                        /**/
                    }
                    else 
                    {
                        //для подписи формируем данные - мусор
                        SmartCardRec.block_record blockItmFnd = SmartCardRec.GetBlock4Id(driverCard, blockItm.idBlock);
                        if (blockItmFnd != null)
                        {
                            SmartCardRec.GetSizeData4Data(blockItmFnd.data, 64, out data);
                        }
                    }
                }
                else
                {//читаем данные
                    try
                    {
                        m_pcscPtr.GetFile(rName, blockItm.idBlock, blockItm.sizeBlock, blockItm.sizeRecord, out data);
                    }
                    catch (Exception ex)
                    {
                        GlobalLog.error(ex.Message);
                        res = -1;
                        break;
                    }
                }
                if (blockItm.getSizeBlock != null) //считываем данные блоками
                {
                    var blkSize = blockItm.getSizeBlock(data);
                    if (blkSize > 0)
                    {
                        blkSize += blockItm.sizeBlock;
                        byte[] blkData;
                        try
                        {
                            m_pcscPtr.GetFile(rName, blockItm.idBlock, blkSize, 0, out blkData);
                        }
                        catch (Exception ex)
                        {
                            GlobalLog.error(ex.Message);
                            res = -1;
                            break;
                        }
                        data = blkData;

                    }
                }
                blockItm.data = data;
                if (SmartCardRec.compareByteArrays(blockItm.idBlock, blockIdentity) && !blockItm.isSgn)
                {//блок идентификации
                    SmartCardRec.IdentificationInfo idntInf = SmartCardRec.GetCaptionDriverCard(driverCard);
                    string strIdnt = String.Format("{0}\n{1}\n{2}\n", idntInf.cardNumber, idntInf.holderFirstNames, idntInf.holderSurname);
                    ReportProgress(5, new PCSC_WorkerInfo(ReaderName, PCSC_WorkerInfo.INITIALIZE, strIdnt));
                }
            }
            return res;
        }

        private string ValidPathString(string sourseStr)
        {
            //очищаем строку от символов ненужных в имени файла
            Func<string, char, string> fClearChar = (string str, char s) =>
            {
                while (true)
                {
                    int pos = str.IndexOf(s);
                    if (pos < 0) break;
                    str = str.Remove(pos, 1);
                }
                return str;
            };
            sourseStr = fClearChar(sourseStr, ' ');
            char[] f = Path.GetInvalidFileNameChars();
            foreach (char s in f)
            {
                sourseStr = fClearChar(sourseStr, s);
            }
            return sourseStr;
        }

        private int SaveSmartCardToFile(SmartCardRec.block_record[] smartCard, string caption, out string filePath)
        {
            //формируем имя файла текущая дата + caption
            filePath = "";
            int res = 0;
            DateTime nowDate = DateTime.Now; //new DateTime(DateTimeKind.Utc);
            string captionTrim = ValidPathString(caption);
            string validFileName = String.Format("{0}{1}{2}.ddd", StoragePath, nowDate.ToString("yyyyMMd_HHmmss"), captionTrim);
            try
            {
                using(FileStream fstream = new FileStream(validFileName, FileMode.OpenOrCreate))
                {
                    for (var i = 0; i < smartCard.Length; i++)
                    {
                        //progress
                        ReportProgress(currProgress_ + (i * chankProgress_) / smartCard.Length, new PCSC_WorkerInfo(ReaderName, "Write 2 file..."));

                        SmartCardRec.block_record blockItm = smartCard[i];
                        if ((blockItm.idBlock.Length != 2) || (blockItm.data == null) || (blockItm.data.Length < 1))
                        {
                            GlobalLog.print(String.Format("SaveSmartCardToFile skip block id:{0}", BitConverter.ToString(blockItm.idBlock)));
                            continue;
                        }
                        byte sgn = blockItm.isSgn ? (byte)0x81 : (byte)0x00;
                        //Метка(3 byte)
                        fstream.Write(new byte[] { blockItm.idBlock[0], blockItm.idBlock[1], sgn}, 0, 3);
                        //Длина(2 byte)
                        ushort sizeData = (ushort)blockItm.data.Length;
                        byte[] sizeDataByte = BitConverter.GetBytes(sizeData);
                        fstream.Write(new byte[] { sizeDataByte[1], sizeDataByte[0] }, 0, 2);
                        //Значение(...)
                        fstream.Write(blockItm.data, 0, blockItm.data.Length);
                        GlobalLog.print(String.Format("SaveSmartCardToFile save block id:{0}, size:{1}", BitConverter.ToString(blockItm.idBlock), blockItm.data.Length));
                    }
                    fstream.Close();
                }
                filePath = validFileName;
            }
            catch (Exception ex)
            {
                GlobalLog.print(String.Format("SaveSmartCardToFile path:{0} ; error: {1}", validFileName, ex.Message));
                res = -1;
            }
            return res;
        }
    }
}
