using System;
using PCSC;

[assembly: CLSCompliant(true)]

namespace MonitorReaderEvents
{
    public class Program
    {
        private static LoggerI _loggerI;
        public static void Main(string[] args) {
		    // Load configuration from file if no other arguments
            if (args.Length <= 1)
            {
            }

            Disp.InitCommunicator("D:\\project\\tacho\\pcsc-sharp-master\\Examples\\TachoSourse\\config.tachoCard");
            Disp _disp = new Disp();
            _disp.Init();

            GlobalLog.communicator = Disp.communicator;
            Program._loggerI = GlobalLog.Instance;
            GlobalLog.trace("main", "Start");

            ProgramConsole app = new ProgramConsole(_disp);
            app.main(args);


           
            _disp.Done();
            // Let the program run until the user presses a key
            Disp.DisposeCommunicator();
            GlobalLog.trace("main", "Finished");
        }
    }
}