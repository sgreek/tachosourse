﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//console
using System.Globalization;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Microsoft.Win32;

//timer
using System.Threading;

using System.IO;

//open folder
using System.Windows.Forms;

//windows loop
using System.Windows.Interop;

using System.Text.RegularExpressions;

namespace TachoSourceApp
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AllocConsole();

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool FreeConsole();

        [DllImport("kernel32", SetLastError = true)]
        static extern bool AttachConsole(int dwProcessId);

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        //4 windows loop
        private HwndSource hwndSource;
        private bool CheckModeApp(string item, ref string mode)
        {
            //режим работы приложения
            string itemUppr = item.ToUpper();
            int posMode = itemUppr.IndexOf("CONSOLE");
            if (posMode < 0)
            {
                posMode = itemUppr.IndexOf("APP");
                if (posMode >= 0) 
                {
                    mode = "APP";
                    return true;
                }
            }
            else
            {
                mode = "CONSOLE";
                return true;
            }
            return false;
        }
        private bool CheckIceConfigApp(string item, ref string val)
        {
            return false;
        }

        private bool CheckParamDDDFiles4Send(string item, ref uint param)
        { //параметры отправки param:

            const string DEL_SENDING = "DEL_SENDING";   // удалять отправленное
            const string SUBFOLDER = "SUBFOLDER";       // искать файлы в подпапках
            string itemUppr = item.ToUpper();
            int posMode = itemUppr.IndexOf(DEL_SENDING);
            if (posMode >= 0)
            {
                param |= SendingStatus.DEL_AFTER_SENDING;
                return true;
            }
            else {
                posMode = itemUppr.IndexOf(SUBFOLDER);
                if (posMode >= 0)
                {
                    param |= SendingStatus.SUB_FOLDER;
                    return true;
                }
            }

            return false;
        }

        private static bool CheckExistParam(string item, string prmName, ref string resVal)
        {
            string itemUppr = item.ToUpper();
            int pos = itemUppr.IndexOf(prmName);
            if (pos > -1)
            {
                int indx = prmName.Length + 1;//+1 for delimeter ("-", ":", "_")
                int cnt = item.Length - indx;
                resVal = item.Substring(indx, cnt);
                return true;
            }
            return false;
        }

        //private SynchronizationContext uiContext_;
        private string fileConfIce = "";
        public string nameExeApp = "";
        public string userPathApp = "";
        private string pathWithDDDFiles4Send = "";  //папка для отправки всех содержащихся в ней ddd-файлов
        private uint paramSend = 0;                 //параметры отправки ddd-файлов: удалить после отправки; искать файлы в подпапках

        private string authSTR_User = "";
        private string authSTR_Passwrd = "";
        private string authSTR_Server = "";

        //конструктор главного окна, инициализируем все объекты
        public MainWindow()
        {
            //параметры переданные приложению
            string mode = "app";                // "console";
            string[] args = Environment.GetCommandLineArgs();

            string pathApp = (args.Length > 0) ? System.IO.Path.GetDirectoryName(args[0]) : "";
            nameExeApp = (args.Length > 0) ? System.IO.Path.GetFileNameWithoutExtension(args[0]) : "TachoMon";
            fileConfIce = (pathApp.Length==0) ? "config.tachoCard" : String.Format("{0}\\config.tachoCard", pathApp);
            userPathApp = String.Format("{0}\\{1}\\", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), nameExeApp);
            #if DEBUG
            System.Windows.MessageBox.Show(String.Format("fileConfIce - {0};\n userPathApp - {1};\n nameExeApp - {2};\n pathApp - {3}", fileConfIce, userPathApp, nameExeApp, pathApp), "Внимание...", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            #endif
            if (!Directory.Exists(userPathApp))
            {
                Directory.CreateDirectory(userPathApp);
            }

            try
            {
                //папка для отправки DDD файлов
                string DDD4SEND = "DDD4SEND";

                string KEY_USER = "USER";
                string KEY_PASSWORD = "PASSWORD";
                string KEY_SERVER = "SERVER";

                for (int i = 0; i < args.Length; i++)
                {
                    if (CheckModeApp(args[i], ref mode)) continue;
                    if (CheckIceConfigApp(args[i], ref fileConfIce)) continue;
                    if (CheckParamDDDFiles4Send(args[i], ref paramSend)) continue;
                    if (CheckExistParam(args[i], DDD4SEND, ref pathWithDDDFiles4Send)) continue;
                    if (CheckExistParam(args[i], KEY_USER, ref authSTR_User))
                    {
                        if (authSTR_User.Length == 0)
                        {
                            authSTR_User = " ";
                        }
                        continue;
                    }
                    if (CheckExistParam(args[i], KEY_PASSWORD, ref authSTR_Passwrd)) continue;
                    if (CheckExistParam(args[i], KEY_SERVER, ref authSTR_Server)) continue;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("Check \"LineArgs\" function was error:{0}", ex.Message));
            }

       

            //инфраструктура - консоль или окно
            InitializeComponent();
            if (mode == "CONSOLE")
            {
                try
                {
                    this.Visibility = System.Windows.Visibility.Hidden;

                    //Get a pointer to the forground window.  The idea here is that
                    //IF the user is starting our application from an existing console
                    //shell, that shell will be the uppermost window.  We'll get it
                    //and attach to it
                    IntPtr ptr = GetForegroundWindow();
                    int u;
                    GetWindowThreadProcessId(ptr, out u);
                    Process process = Process.GetProcessById(u);
                    if (process.ProcessName == "cmd") //Is the uppermost window a cmd process?
                    {
                        AttachConsole(process.Id);
                        //we have a console to attach to ..
                        Console.WriteLine("It looks like you started me from an existing console.");
                    }
//                    else
                    {
                        //no console AND we're in console mode ... create a new console.
                        AllocConsole();

                        Console.WriteLine(@"It looks like you double clicked me to start
                            AND you want console mode.  Here's a new console.");

                        TachoSource.ProgramConsole pConsole = new TachoSource.ProgramConsole(fileConfIce, userPathApp, nameExeApp);
                        pConsole.SetParam(pathWithDDDFiles4Send, paramSend, authSTR_User, authSTR_Passwrd, authSTR_Server);
                        pConsole.main(args);
                    }
                }
                finally
                {
                    FreeConsole();
                    this.Close();
                }
            }
            else
            {
                #if DEBUG
                System.Windows.MessageBox.Show("Call func Init");
                #endif


                //TimerCallback tm = new TimerCallback(__ => this.Init());
                //System.Threading.Timer timer = new System.Threading.Timer(tm, null, 200, 0);


                InitImage();
                this.Init();


                //LoadImage();
           
            }

            // Let the program run until the user presses a key
            GlobalLog.trace("main", "Finished");
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            // Для перехвата сообщений в WPF
            // создаем экземпляр HwndSource
            hwndSource = PresentationSource.FromVisual(this) as HwndSource;
            // и устанавливаем перехватчик
            hwndSource.AddHook(WndProc);
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == ConnectionUSBDev.WM_DRAWCLIPBOARD)
            {
                // обрабатываем сообщение
            }

            if ((msg == ConnectionUSBDev.WM_DEVICECHANGE) && (bool)chbAutoUpdateUsbDev.IsChecked)
            {
                if (wParam.ToInt32() == ConnectionUSBDev.DBT_DEVNODES_CHANGED)
                {
                    GlobalLog.trace("main", "Список USB устройств ПК изменён");
                    this.UpdateReaders();
                }
                else if (wParam.ToInt32() == ConnectionUSBDev.DBT_DEVICEARRIVAL || wParam.ToInt32() == ConnectionUSBDev.DBT_DEVICEREMOVECOMPLETE)
                {
                    var dbhARRIVAL = (MsgConnectUSBDev.DEV_BROADCAST_HDR)Marshal.PtrToStructure(lParam, typeof(MsgConnectUSBDev.DEV_BROADCAST_HDR));
                    if (dbhARRIVAL.dbch_devicetype == ConnectionUSBDev.DBT_DEVTYP_VOLUME)
                    {
                        var dbv = (MsgConnectUSBDev.DEV_BROADCAST_VOLUME)Marshal.PtrToStructure(lParam, typeof(MsgConnectUSBDev.DEV_BROADCAST_VOLUME));

                        int DriveLetter = 0;
                        // Далее ищем установленный бит и получаем нужную букву
                        while ((dbv.dbcv_unitmask & (1 << DriveLetter)) != dbv.dbcv_unitmask && DriveLetter != 32)
                            DriveLetter++;

                        // Буква USB dev
                        var label = (char)('A' + DriveLetter);
                        string status = "Неопределено";
                        switch (wParam.ToInt32())
                        {
                            case ConnectionUSBDev.DBT_DEVICEARRIVAL:
                                status = "Подключен";
                                break;
                            case ConnectionUSBDev.DBT_DEVICEREMOVECOMPLETE:
                                status = "Отключен";
                                break;
                        }
                        GlobalLog.trace("main", String.Format("USB устройство ПК {0}:\\  {1}", Char.ConvertFromUtf32(label), status));
                        this.UpdateReaders();
                    }
                }
            }
            return IntPtr.Zero;
        }
        public void Init()
        {
            GlobalLog.trace("main", String.Format("Programm`s version: {0}", Applctn.VERSION));

            Applctn applctn = Applctn.Instance;
            //работа с картридером
            applctn.OnCardCompleted += DoCardCompleted;
            applctn.OnCardReadProgress += DoCardReadProgress;
            applctn.OnCardChangeState += DoCardChangeState;
            applctn.OnCardReadIdentity += DoReadIdentity;
            //работа с отправкаой файлов
            // applctn.InitSSF(lvSendFiles);
            applctn.OnSSFMessage += DoSSFMessage;
            applctn.OnSSFProgress += DoSSFProgress;
            applctn.OnSSFCompleted += DoSSFCompleted;
            applctn.Init(fileConfIce, userPathApp, nameExeApp);
            GlobalLog.trace("main", String.Format("Start: fileConfIce - {0}; userPathApp - {1}; nameExeApp - {2}", fileConfIce, userPathApp, nameExeApp));

            //визуальные компоненты
            FunctnNotPrm setAutoSendPrm = SetAutoSendPrm;
            chbAutoSending.Dispatcher.Invoke(setAutoSendPrm);

            string tmpStrg = Properties.Settings.Default.Folder4dddFiles;
            if (!Directory.Exists(tmpStrg))
            {
                tmpStrg = userPathApp;
                tmpStrg = String.Format("{0}{1}", tmpStrg, "DDDfiles\\");
                Directory.CreateDirectory(tmpStrg);
            }
            if (Directory.Exists(tmpStrg))
            {
                applctn.StorageSendingPath = tmpStrg;
                SetTextElement(this.eFolder4dddFiles, tmpStrg);
            }
            this.UpdateReaders();
            this.CheckExistAuthService();

        }

        //методы для заполнения визуальных элементов
        delegate void FunctnNotPrm();


        private void SetAutoSendPrm()
        {
            Applctn applctn = Applctn.Instance;
            applctn.AutoSendReadingFromCard = (bool)chbAutoSending.IsChecked;
        }

        public void UpdateReaders()
        {
            //обновляем список считывателей
            Applctn applctn = Applctn.Instance;

            string[] listCR = null;
            try
            {
                listCR = applctn.GetListCardReaders();
            }
            catch(Exception ex)
            {
                listCR = null;
                #if DEBUG
                        System.Windows.MessageBox.Show(String.Format("Exception: applctn.GetListCardReaders() - {0}", ex));
                #endif

            }

            System.Windows.Controls.ComboBox comboBox = (System.Windows.Controls.ComboBox)cbCardReaders;
            FunctnNotPrm updateCBReaders = delegate()
            {
                comboBox.Items.Clear();

                if ((listCR != null) && (listCR.Length > 0))
                {
                    for (int i = 0; i < listCR.Length; i++)
                    {
                        comboBox.Items.Add(listCR[i]);
                    }
                    comboBox.SelectedIndex = 0;
                }

            };
            comboBox.Dispatcher.Invoke(updateCBReaders);

            if ((listCR != null) && (listCR.Length > 0))
            {
                SetStateInCardReaders(String.Format("Обнаружено считывателей: {0}", listCR.Length));
            }
            else
            {
                SetStateInCardReaders("Считывателей не найдено");
            }
            if (GetChecked(this.chbAutoUpdateUsbDev))
            {
                //когда нет "автообновления" кнопку не считывания не блокируем
                SetEnable(bCardRead, false);
            }

            if ((listCR != null) && (listCR.Length > 0))
            {
                try
                {
                    applctn.PCSCStart();
                }
                catch (Exception ex)
                {
                    SetStateInCardReaders(String.Format("Ошибка: {0}", ex.Message));
                }
            }

        }
        private bool GetChecked(System.Windows.Controls.CheckBox elmnt)
        {
            bool val = true;
            FunctnNotPrm getValue = delegate()
            {
                val = (bool)elmnt.IsChecked;
            };
            elmnt.Dispatcher.Invoke(getValue);
            return val;
        }

        private void SetEnable(FrameworkElement elmnt, bool val)
        {
            FunctnNotPrm setEnable = delegate() 
            { 
                elmnt.IsEnabled = val;
            };
            elmnt.Dispatcher.Invoke(setEnable);
        }

        private void SetVisible(FrameworkElement elmnt, System.Windows.Visibility val)
        {
            FunctnNotPrm setVisible = delegate()
            {
                elmnt.Visibility = val;
            };
            elmnt.Dispatcher.Invoke(setVisible);
        }

        static public void SetTextElement(System.Windows.Controls.ComboBox elmnt, string msg)
        {
            FunctnNotPrm setTextElement = delegate()
            {
                elmnt.Text = msg;
                elmnt.UpdateLayout();
            };
            elmnt.Dispatcher.Invoke(setTextElement);
        }
        static public void SetTextElement(System.Windows.Controls.TextBlock elmnt, string msg)
        {
            FunctnNotPrm setTextElement = delegate()
            {
                elmnt.Text = msg;
                elmnt.UpdateLayout();
            };
            elmnt.Dispatcher.Invoke(setTextElement);
        }
        static public void SetTextElement(System.Windows.Controls.TextBox elmnt, string msg)
        {
            FunctnNotPrm setTextElement = delegate()
            {
                elmnt.Text = msg;
                elmnt.UpdateLayout();
            };
            elmnt.Dispatcher.Invoke(setTextElement);
        }
        static public void SetTextElement(System.Windows.Controls.PasswordBox elmnt, string msg)
        {
            FunctnNotPrm setTextElement = delegate()
            {
                elmnt.Password = msg;
                elmnt.UpdateLayout();
            };
            elmnt.Dispatcher.Invoke(setTextElement);
        }

        public void SetStateInCardReaders(string msg)
        {
            if (msg.Length == 0) return;
            msg = Regex.Replace(msg, @"\r", "");
            msg = Regex.Replace(msg, @"\n", "");
            SetTextElement(tCardReaderStates, msg);
        }

        public void SetProgressCardReaders(System.Windows.Controls.ProgressBar elmnt, byte progress)
        {
            //прогресс в элементе у считывателя
            FunctnNotPrm setValue = delegate()
            {
                elmnt.Value = progress;
            };

            elmnt.Dispatcher.Invoke(setValue);
        }

        private void ChangeColorCardState(string color)
        {
            Action<Ellipse, string> SetVisible4Mask = (Ellipse itm, string mask) =>
            {

                FunctnNotPrm setValue = delegate()
                {
                    if (itm.Name.IndexOf(mask) > 0)
                    {
                        SetVisible(itm, Visibility.Visible);
                    }
                };
                itm.Dispatcher.Invoke(setValue);
            };

            Ellipse[] objCardState = new Ellipse[] { eStateGreen, eStateRed, eStateYellow, eStateGray };
            foreach(Ellipse itm in objCardState)
            {
                SetVisible(itm, Visibility.Hidden);
            }
            foreach(Ellipse itm in objCardState)
            {
                SetVisible4Mask(itm, color);
            }
        }

        private void DoCardCompleted(object result, Exception error, bool cancelled)
        { 
            if(cancelled){
                SetStateInCardReaders("Считывание прервано");
                return;
            }
            if(error != null)
            {
                SetStateInCardReaders(String.Format("Ошибка:{0}", error));
                ChangeColorCardState("Red");
            }
            else{
                ChangeColorCardState("Yellow");
                SetTextElement(tCardState, String.Format("Progress: {0}%", 100));
                SetStateInCardReaders(String.Format("Завершено:{0}", result));
            }
        }

        private void DoCardReadProgress(int progressPercentage, string msg)
        {
            //pbProgressRead
            SetTextElement(tCardState, String.Format("Progress: {0}% ; {1}", progressPercentage, msg));
            SetProgressCardReaders(pbProgressRead, (byte)progressPercentage);
        }
        private void DoReadIdentity(string indentity)
        {
            //данные идентификации карточки
            SetTextElement(tIndetification, indentity);
        }

        
        private void DoCardChangeState(string card, uint code, string msg)
        { 
             //var onEvent = new Action((itm, value) => { itm.Visibility = value });
            //изменение состояния карты
            ChangeColorCardState("Gray");

            if(GetChecked(this.chbAutoUpdateUsbDev))
            {
                //когда нет автообновления кнопку не считывания не блокируем
                SetEnable(bCardRead, false);
            }

            if ((code & (int)TachoSource.MediatorPCSC.CardStateInfo.Present) != 0)
            {
                //карточка вставлена
                ChangeColorCardState("Green");
                SetEnable(gbCardState, true);
                SetEnable(bCardRead, true);
            }
            else if ((code & (int)TachoSource.MediatorPCSC.CardStateInfo.Empty) != 0)
            {
                //карточка извленчена
                SetEnable(gbCardState, false);
                SetVisible(pbProgressRead, Visibility.Hidden);
            }
            else if ((code & (int)TachoSource.MediatorPCSC.CardStateInfo.InUse) != 0)
            {
                //карта используется
                ChangeColorCardState("Red");
            }

            SetTextElement(tCardState, TachoSource.MediatorPCSC.CardStateInfoDescript(code));
            SetStateInCardReaders(msg);

        }

        //обработка отправки файлов
        public void SetStateFileSending(string msg)
        {
            if (msg.Length == 0) return;
            SetTextElement(tFileSendingStates, msg);
        }

        public void DoSSFMessage(string msg)
        {
            SetStateFileSending(msg);
        }

        public void DoSSFProgress(string file, int progressPercentage, object userState)
        {
            FunctnNotPrm setValue = delegate()
            {
                imgProgress.Source = GetImage4Progress(progressPercentage);
            };
            imgProgress.Dispatcher.Invoke(setValue);
/*
            SendOrPostCallback setValue = (object val) =>
            {
                imgProgress.Source = GetImage4Progress(progressPercentage);
            };
            if (uiContext_ != null) 
            {
                uiContext_.Post(setValue, null);
            }
/**/
            SetStateFileSending(String.Format("{0} {1}:", file, userState));
        }

        public void DoSSFCompleted(string file, object result, Exception error, bool cancelled)
        {
            if (cancelled)
            {
                SetStateFileSending("Отправка прервана");
                return;
            }
            if (error != null)
            {
                string msg = error.Message;
                if ((msg.Length == 0) && (error.InnerException != null))
                { 
                   msg = error.InnerException.Message;
                }
                if (msg.Length == 0)
                {
                    Exception exB = error.GetBaseException();
                    if (exB != null) 
                    {
                        msg = error.Message;
                    }
                }
                if (error is Monitoring.Error)
                {
                    Monitoring.Error iceErr = (Monitoring.Error)error;
                    msg += string.Format(" (code: {0}; msg: {1})", iceErr.code, iceErr.ice_message_);
                }
                SetStateFileSending(String.Format("Ошибка:{0}", msg));
            }
            else
            {
                SetStateFileSending(String.Format("Завершено: {0}", file));
            }
        }


        //---------------------------------------------------------------------------------
        //события от визуальных элементов
        private void bCRUpdate_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UpdateReaders();
        }

        private void bCRUpdate_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            ppAutoUpdateUsbDev.IsOpen = true;
        }


        private void chbAutoUpdateUsbDev_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ppAutoUpdateUsbDev.IsOpen = false;
        }

        private void chbAutoUpdateUsbDev_Click(object sender, RoutedEventArgs e)
        {
            Applctn applctn = Applctn.Instance;
            applctn.SetAutoStateReader((bool)this.chbAutoUpdateUsbDev.IsChecked);
        }

        private void cbCardReaders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox comboBox = (System.Windows.Controls.ComboBox)sender;
            Applctn applctn = Applctn.Instance;
            applctn.CurrentCardReader = (string)comboBox.SelectedItem;
        }

        private void bCardRead_Click(object sender, RoutedEventArgs e)
        {
            SetProgressCardReaders(pbProgressRead, (byte)0);
            SetVisible(pbProgressRead, Visibility.Visible);
            Applctn applctn = Applctn.Instance;

            //
            SetStateInCardReaders("Считывание");
            if ((bool)this.chbCheckPin.IsChecked)
            {
                this.CheckPinWindow((msg, code) => { if (code == 0) { applctn.ReadCard(); } });
            }
            else
            {
                applctn.ResetPin();
                applctn.ReadCard();
            }
        }

        //по отправке
        private bool CheckFileExist(string path, string ext = "")
        {
            //проверка существования файла и указанного расширения
            FileInfo fileInf = null;
            try
            {
                fileInf = new FileInfo(path);
            }
            catch (Exception)
            {
            }
            if ((fileInf == null) || (!fileInf.Exists))
            {
                System.Windows.MessageBox.Show("Укажите существующий файл!!!", "Внимание...", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            if (ext.Length > 0)
            {
                string fileExt = System.IO.Path.GetExtension(path).ToUpper();
                string extUpr = ext.ToUpper();
                int posMode = fileExt.IndexOf(extUpr);
                if (posMode < 0)
                {
                    System.Windows.MessageBox.Show(string.Format("Не верный формат файла. Должен быть:{}", ext), "Внимание...", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return false;
                }
            }
            return true;
        }

        private ConnectionWindow dialogConnection = null;
        private void bConnection_Click(object sender, RoutedEventArgs e)
        {
            dialogConnection = new ConnectionWindow();
/*
            dialogPin.OnResState += (msg, code) => { 
                String.Format("code: {0}; message: {1}", code, msg); 
            };
/**/
            dialogConnection.Owner = this;
            //позиция окна
            var leftField = typeof(Window).GetField("_actualLeft", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            double ll = (double)leftField.GetValue(this);
            var topField = typeof(Window).GetField("_actualTop", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            double tt = (double)topField.GetValue(this);
            dialogConnection.Left = ll + this.Width / 2;
            dialogConnection.Top = tt + this.Height / 2;

            dialogConnection.ShowDialog();
        }
        private void bAddFilesOnSend_Click(object sender, RoutedEventArgs e)
        {
            //добавляем файл на отправку
            string path = eSelect4SendFiles.Text;
            if (path.Length == 0) {
                System.Windows.MessageBox.Show("Укажите файл!!!", "Внимание...", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            //проверим, есть ли сервис авторизации
            //  если есть - стоит ли галочка запоминать подключение
            //      стоит - пробуем подключиться и получить ТокенИдентификатор

            if (CheckFileExist(path, "ddd"))
            {
                Applctn applctn = Applctn.Instance;
                applctn.AddFileOnSend(path);
            }

        }
        private void bSelectFile_Click(object sender, RoutedEventArgs e)
        {
            //выбираем ddd-файл
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "Tachograph files (*.ddd)|*.ddd|All files (*.*)|*.*";
            //openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (openFileDialog.ShowDialog() == true)
            {
                eSelect4SendFiles.Text = openFileDialog.FileName;// File.ReadAllText();
            }

            /*
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog() == true)
            {
                foreach (string filename in openFileDialog.FileNames)
                    lbFiles.Items.Add(Path.GetFileName(filename));
            }
            /**/
        }
        private void bOpenFolder4dddFiles_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            FBD.ShowNewFolderButton = false;
            if(this.eFolder4dddFiles.Text.Length > 0)
            {
                FBD.SelectedPath = this.eFolder4dddFiles.Text;
            }
            
            if (FBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.eFolder4dddFiles.Text = FBD.SelectedPath + "\\";
                Applctn applctn = Applctn.Instance;
                applctn.StorageSendingPath = this.eFolder4dddFiles.Text;
            }
/*
            //выбираем папку для размещения ddd-файлов
            var dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            CommonFileDialogResult result = dialog.ShowDialog();
            if (result == CommonFileDialogResult.Ok)
            {
                this.eFolder4dddFiles.Text = dialog.FileName + "\\";
                Applctn applctn = Applctn.Instance;
                applctn.StorageSendingPath = this.eFolder4dddFiles.Text;
            }
/**/
        }

        private void chbAutoSending_Click(object sender, RoutedEventArgs e)
        {
            //автоматически отправлять файлы со считывателя
            Applctn applctn = Applctn.Instance;
            applctn.AutoSendReadingFromCard = (bool)chbAutoSending.IsChecked;
        }

        private void eFolder4dddFiles_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;

            string path = eFolder4dddFiles.Text;
            if (!Directory.Exists(path))
            {
                System.Windows.MessageBox.Show("Укажите cуществующую папку!!!", "Внимание...", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            char lstChar = path[path.Length-1];
            if ((lstChar != '\\' ) ||( lstChar != '/'))
            {
                path += '\\';
            }
            Applctn applctn = Applctn.Instance;
            applctn.StorageSendingPath = path;
        }

/*

                    // An List of BitmapImage files.
                    List<BitmapImage> images = new List<BitmapImage>();
                    // Current position in the list.
                    private int currImage = 0;
                    private const int MAX_IMAGES = 2;
                    private void LoadImage()
                    {
                            try
                            {
                                //string path = Environment.CurrentDirectory;

                                // Load these images when the window loads. 
                                //images.Add(new BitmapImage(new Uri(string.Format(@"{0}\Images\Deer.jpg", path))));
                                //images.Add(new BitmapImage(new Uri(string.Format(@"{0}\Images\Dogs.jpg", path))));
                                //images.Add(new BitmapImage(new Uri(string.Format(@"{0}\Images\Welcome.jpg", path))));

                                images.Add(new BitmapImage(new Uri(@"/Images/gaudge8_5.png", UriKind.Relative)));
                                images.Add(new BitmapImage(new Uri(@"/Images/open_32.png", UriKind.Relative)));
                                images.Add(new BitmapImage(new Uri(@"/Images/gaudge8_8.png", UriKind.Relative)));

                                //imageHolder.Source = images[currImage];                
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                    }
                    private void Button_Click(object sender, RoutedEventArgs e)
                    {
                        if (--currImage < 0) currImage = MAX_IMAGES;
                        imageHolder.Source = images[currImage];

                        imgProgress.Source = TachoSource.MediatorSendFile.GetImage4Progress(currImage*10);
                    }

/**/

        //должны быть созданы-загружены в основном потоке
        private static List<BitmapImage> images = null;
        public static void InitImage()
        {
            #if DEBUG
                System.Windows.MessageBox.Show("InitImage");
            #endif

            if ((images != null) && (images.Count > 0)) return;
            List<BitmapImage> tmpImages = new List<BitmapImage>();
            try
            {
                tmpImages.Add(new BitmapImage(new Uri(@"/Images/gaudge8_0.png", UriKind.Relative)));
                tmpImages.Add(new BitmapImage(new Uri(@"/Images/gaudge8_1.png", UriKind.Relative)));
                tmpImages.Add(new BitmapImage(new Uri(@"/Images/gaudge8_2.png", UriKind.Relative)));
                tmpImages.Add(new BitmapImage(new Uri(@"/Images/gaudge8_3.png", UriKind.Relative)));
                tmpImages.Add(new BitmapImage(new Uri(@"/Images/gaudge8_4.png", UriKind.Relative)));
                tmpImages.Add(new BitmapImage(new Uri(@"/Images/gaudge8_5.png", UriKind.Relative)));
                tmpImages.Add(new BitmapImage(new Uri(@"/Images/gaudge8_6.png", UriKind.Relative)));
                tmpImages.Add(new BitmapImage(new Uri(@"/Images/gaudge8_7.png", UriKind.Relative)));
                tmpImages.Add(new BitmapImage(new Uri(@"/Images/gaudge8_8.png", UriKind.Relative)));

                images = tmpImages;
                tmpImages = null;

                biConnection = new BitmapImage(new Uri(@"/Images/connection_32.png", UriKind.Relative));
                biNoConnection = new BitmapImage(new Uri(@"/Images/noconnection_32.png", UriKind.Relative));
            }
            catch (Exception ex)
            {
                #if DEBUG
                     System.Windows.MessageBox.Show(ex.Message, "InitImage...", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                #endif
            }
            #if DEBUG
                        System.Windows.MessageBox.Show("InitImage is Ok");
            #endif

        }
        public static BitmapImage GetImage4Progress(int progress)
        {
            int cnt = images != null ? images.Count : 0;
            int COUNT_BLOCK = 8;
            int currBlock = (COUNT_BLOCK * progress) / 100;
            return currBlock < cnt ? images[currBlock] : null;
        }

        //-----------------------------------------------------PIN-------------------------------------

        private PinWindow dialogPin = null;
        private void CheckPinWindow(Applctn.FResAuthState cb)
        {
            dialogPin = new PinWindow();
            dialogPin.OnResState += cb;
            dialogPin.Owner = this;
            //позиция окна
            var leftField = typeof(Window).GetField("_actualLeft", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            double ll = (double)leftField.GetValue(this);
            var topField = typeof(Window).GetField("_actualTop", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            double tt = (double)topField.GetValue(this);
            dialogPin.Left = ll + this.Width/2;
            dialogPin.Top = tt + this.Height/2;

            dialogPin.ShowDialog();
        }
        
        private void Pin_Click(object sender, RoutedEventArgs e)
        {
            if (this.bCheckPin == sender)
            {
                CheckPinWindow((msg, code) => { String.Format("code: {0}; message: {1}", code, msg); });
                return;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //запись настроек
            Properties.Settings.Default.MainWidth = this.Width;
            Properties.Settings.Default.MainHeight = this.Height;

            Properties.Settings.Default.Folder4dddFiles = this.eFolder4dddFiles.Text;
            Properties.Settings.Default.AutoSending = (bool)this.chbAutoSending.IsChecked;
            Properties.Settings.Default.CheckPin = (bool)this.chbCheckPin.IsChecked;

            Applctn applctn = Applctn.Instance;
            Properties.Settings.Default.Profiles = applctn.Profiles.toJSON();

            Properties.Settings.Default.AutoUpdateUsbDev = (bool)this.chbAutoUpdateUsbDev.IsChecked;

            //сохранение настроек
            Properties.Settings.Default.Save();
            applctn.Done();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            #if DEBUG
                System.Windows.MessageBox.Show("Window_Loaded");
            #endif
            //загружаем размеры окна
            this.Width = Properties.Settings.Default.MainWidth;
            this.Height = Properties.Settings.Default.MainHeight;
            //Properties.Settings.Default.Folder4dddFiles
            this.chbAutoSending.IsChecked = Properties.Settings.Default.AutoSending;
            this.chbCheckPin.IsChecked = Properties.Settings.Default.CheckPin;

            Applctn applctn = Applctn.Instance;
            applctn.Profiles.fromJSON(Properties.Settings.Default.Profiles);

            #if DEBUG
                    System.Windows.MessageBox.Show("Window_Loaded after fromJSON");
            #endif

            this.chbAutoUpdateUsbDev.IsChecked = Properties.Settings.Default.AutoUpdateUsbDev;

            //call before applctn.init
            applctn.AutoStateReaderStore = (bool)this.chbAutoUpdateUsbDev.IsChecked;
            #if DEBUG
                        System.Windows.MessageBox.Show("Window_Loaded is Ok");
            #endif

        }

        //-----------------------------------------------------------------
        // Auth service
        static private BitmapImage biConnection = null;
        static private BitmapImage biNoConnection = null;

        private void DoAuthState(string msg, int code)
        {
            FunctnNotPrm ChangeVisible = delegate()
            {
                this.bConnection.Visibility = Visibility.Visible;
                this.lCurrentUser.Visibility = Visibility.Visible;
                this.lCurrentUser.Text = "";
            };

            FunctnNotPrm ImageNoConnection = delegate()
            {
                imgConnection.Source = biNoConnection; 
            };
            FunctnNotPrm ImageConnection = delegate()
            {
                imgConnection.Source = biConnection;
            };

            Applctn applctn = Applctn.Instance;
            imgConnection.Dispatcher.Invoke(ImageNoConnection); 
            if (code == 0)
            {
                //получен результат авторизации
                if (applctn.AuthInfoRes.Token.Length > 0)
                {
                    imgConnection.Dispatcher.Invoke(ImageConnection);

                    SetTextElement(lCurrentUser, applctn.AuthInfoIn.User);
                }
            }
            else if (code == 2)
            {
                //сервис авторизации обнаружен
                tFileSendingStates.Dispatcher.Invoke(ChangeVisible);

                //сервис авторизации обнаружен
                ProfileInfo itmProf = applctn.Profiles.GetCurrent();
                if (itmProf != null)
                {
                    applctn.CreateSession(itmProf.user, itmProf.pswrd, itmProf.server);
                }

            }
            SetStateFileSending(msg);
        }
        private void CheckExistAuthService()
        {

            Applctn applctn = Applctn.Instance;
            applctn.OnAuthState += DoAuthState;
            applctn.AuthInfo();
        }

        private void chbCardTypeESTR_Click(object sender, RoutedEventArgs e)
        {
            //считываем ЕСТР карты
            Applctn applctn = Applctn.Instance;
            applctn.CardType = (byte)((bool)chbCardTypeESTR.IsChecked ? TachoSource.SmartCardRec.TYPE_ESTR : TachoSource.SmartCardRec.TYPE_WITH_GOST);
        }

    }
}
