// **********************************************************************
//
// Copyright (c) 2003-2015 ZeroC, Inc. All rights reserved.
//
// **********************************************************************

#pragma once

#include <Ice/BuiltinSequences.ice>
#include "Monitoring.ice"

module Monitoring
{


/* Error codes */
//enum ErrorCode { EFail, EConnect, ENotFound, EInvalidArg, ENotImpl, EDatabase, EFileError };
/*
const int EFail = -1;
const int EConnect = -2;
const int ENotFound = -3;
const int EInvalidArg = -4;
const int ENotImpl = -5;
const int EDatabase = -6;
const int EWrongFileFormat = -7;
const int EWrongFileAccess = -8;
const int ESaveToDB = -9;

const int TOK = 0;
const int TNOTOK = 1;

exception Error
{
	int code;
    string message;
};
/**/
interface StorageObserver
{
	["cpp:const"] idempotent void changed(int param);
};

const string AuthService = "authService";	//������ �����������

struct FileInfo
{
	int code;						// ��� ������ 0 - OK
	string idO;						// ������������� �������� � ����
	string cardID;					// ������������� �������� � �������� �������� ��������� � ����������
	byte cardType;
	string fileName;				// ��� � ����
	int status;						// �����: ������� - ������� - ���������� � ���� (�.�. ����)
};

//status from FileInfo
const int SttsMask = 0x00ff;		//����� ��������� ���������� ����� � �������
const int SttsDef = 0;				//�������
const int SttsParsed = 1;			//������� ������
const int SttsProcessed = 2;		//
const int SttsAppended = 3;			//��������� �������

const int SttsWithError = 9;			//������
const int SttsParseWithError = 8;		//������
const int SttsSaveDBWithError = 7;		//������


interface FileStorage
{
	["amd"] idempotent string info(optional(1) string param);
	["amd"] idempotent int readFile(string idAT, string name, int numberFirstOfBytes, int numberOfBytesToRead, out Ice::ByteSeq buffer, out int numberOfBytesRead) throws Error;
	["amd"] idempotent int writeFile(string idAT, string name, int chunkSize, Ice::ByteSeq buffer, out int numberOfBytesWritten, out FileInfo info) throws Error;
	["amd"] idempotent FileInfo getInfo(string idAT, string name);
};

};
