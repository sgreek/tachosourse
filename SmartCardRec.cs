﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


// описание структуры элементарного файла(блока) на смарт карте
namespace TachoSource
{
    public partial class SmartCardRec
    {

        //тип карточки -- RFU (зарезервировано для будущего использования) (8..255)
        public static readonly byte СT_RESERVED = 0;     //-- зарезервировано (0),
        public static readonly byte СT_DRIVER = 1;       //-- карточка водителя (1),
        public static readonly byte СT_WORKSHOP = 2;     //-- карточка мастерской (2),
        public static readonly byte СT_CONTROL = 3;      //-- карточка контролера (3),
        public static readonly byte СT_COMPANY = 4;      //-- карточка предприятия (4),
        public static readonly byte СT_MANUFACTURING = 5;//-- карточка завода-изготовителя (5),
        public static readonly byte СT_VEHICLE = 6;      //-- бортовое устройство (6),
        public static readonly byte СT_MOTION_SENSOR = 7;//-- датчик движения (7),

        //сравнивает два массива
        static public bool compareByteArrays(byte[] array1, byte[] array2)
        {
            bool areEqual = array1.SequenceEqual(array2);
            return areEqual;
        }
        // копирует часть массива байт
        public static byte[] arrayCopy(byte[] value, int from, int length)
        {
            byte[] tmp = new byte[length];

            Array.Copy(value, from, tmp, 0, length);
            return tmp;
        }

        // конвертируем byte[] в строку
        public static string convertIntoString(byte[] b)
        {
            System.Text.Encoding enc = System.Text.Encoding.ASCII;
            string myString = enc.GetString(b);

            return myString;
        }

        static string bcd2Str(byte[] bytes)
        {
            //28595 -iso-8859-5 Cyrillic (ISO)
            return Encoding.GetEncoding("iso-8859-5").GetString(bytes).Trim();
        }

        public delegate ushort SizeBlockOfBlock(byte[] data);

        public class block_record : ICloneable
        {
            public byte[] idBlock { get; set; }
            public ushort sizeBlock { get; set; }
            public string name { get; set; }
            public byte[] data { get; set; }

            public ushort sizeRecord { get; set; }

            public SizeBlockOfBlock getSizeBlock;

            public bool isSgn { get; set; }

            public block_record()
            {
                isSgn = false;
            }

            public virtual object Clone()
            {
                return new block_record() { idBlock = this.idBlock, sizeBlock = this.sizeBlock, name = this.name, sizeRecord = this.sizeRecord, getSizeBlock = this.getSizeBlock, isSgn = this.isSgn };
            }
        };

        public static block_record GetBlock4Id(block_record[] list, byte[] blockId)
        {
            block_record findBlock = null;
            if (blockId.Length < 1) return findBlock;
            for (var i = 0; i < list.Length; i++)
            {
                SmartCardRec.block_record blockItm = list[i];
                if(compareByteArrays(blockItm.idBlock, blockId))
                {
                    findBlock = blockItm;
                    break;
                }

            }
            return findBlock;
        }

        public static void GetSizeData4Data(byte[] srsData, uint outSize, out byte[] outData)
        {
            //
            List<byte> data = new List<byte>();
            int nSrsD = srsData.Length;
            for (int i = 0; i < outSize; i++)
            {
                data.Add(i < nSrsD ? srsData[i] :(byte)0);
            }
            outData = data.ToArray();
        }
        public static block_record[] GetIndentificationTemplate()
        {
            /*
            структура файлов любой из СМАРТ карты
            MF 3F00
                EF ICC 0002 - 25
                EF IC 0005 - 8
                DF Tachograph 0500 
                    EF Application_Identification 0501 - 10
                    EF Card_Certificate_GOST C200 - 770
                    EF Key_Identificators C201 - 16
                    EF CA_Certificate_GOST C208 - 655
                    EF Identification 0520 - 143
            */

            block_record[] сardTemplate = 
            {
                //MF 3F00
                //DF Tachograph(ASCII:FF 54 41 43 48 4F) 0500 
                new block_record(){ idBlock = new byte[]{0xFF, 0x54, 0x41, 0x43, 0x48, 0x4F}, sizeBlock=0, name="DF Tachograph"},
                new block_record(){ idBlock = new byte[]{0x05, 0x01}, sizeBlock=10, name="EF Application_Identification"},
            };
            List<block_record> newCard = new List<block_record>();
            for (var i = 0; i < сardTemplate.Length; i++)
            {
                newCard.Add((block_record)сardTemplate[i].Clone());
            }
            return newCard.ToArray();
        }
        public static byte GetCardType(block_record[] list)
        {
            byte typeC = СT_RESERVED;
            //найдём блок "EF Identification" и извлечём из него данные
            block_record rec = GetBlock4Id(list, new byte[] { 0x05, 0x01 });
            //проверяем первый байт структуры, с.м. описание структуры Application_Identification для всех типов
            if ((rec == null) || (rec.data.Length < 1)) return typeC;
            typeC = rec.data[0];
            return typeC;
        }

    }
}
