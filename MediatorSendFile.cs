﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections.ObjectModel;
using System.Collections.Specialized;

using System.Windows.Media;
using System.Windows.Media.Imaging;

using System.Reflection;

using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;


//Sinding
using System.Threading;
using System.Threading.Tasks;

namespace TachoSource
{
    //элемент для сохранения в файл
    [DataContract]
    public class ItemLoadFileState
    {
        [DataMember]
        public string cardID { get; set; }					// идентификатор элемента в таблицах карточек водителей и тахографов
        [DataMember]
        public string fileId { get; set; }				// имя в базе
        [DataMember]
        public int status { get; set; }					    //
        //result code
        public int code = 0;
        public ItemLoadFileState()
        {
            status = 0;
        }
        public ItemLoadFileState(string aCardID, string aFileId)
        {
            cardID = aCardID;
            fileId = aFileId;
        }

        static public ItemLoadFileState FromIce(Monitoring.FileInfo fiObj)
        {
            if (fiObj == null) return null;

            ItemLoadFileState newObj = new ItemLoadFileState(fiObj.cardID, fiObj.fileName);
            newObj.status = fiObj.status;
            newObj.code = fiObj.code;
            return newObj;
        }
        override public string ToString()
        {
            switch (status){
                case Monitoring.SttsDef.value:
                    return String.Format("code: {0}; message: {1}", status, "Default");
                case Monitoring.SttsParsed.value:
                    return String.Format("code: {0}; message: {1}", status, "Parsed");
                case Monitoring.SttsProcessed.value:
                    return String.Format("code: {0}; message: {1}", status, "Processed");
                case Monitoring.SttsAppended.value:
                    return String.Format("code: {0}; message: {1}", status, "Appended");
                case Monitoring.SttsWithError.value:
                    return String.Format("code: {0}; message: {1}", status, "Error");
                case Monitoring.SttsParseWithError.value:
                    return String.Format("code: {0}; message: {1}", status, "ParseWithError");
                case Monitoring.SttsSaveDBWithError.value:
                    return String.Format("code: {0}; message: {1}", status, "SaveDBWithError");

                default: 
                    return String.Format("code: {0}; message: {1}", status, "Unknown");
            }
        }

    }

    //отправляет указанные файлы на сервер
    public class MediatorSendFile
    {

        public readonly string GR_MAIN = "MSF";
        public readonly string GR_TRACE = "MSF_T";


        public enum CodeOfError
        {
            // ошибка конекта к серверу
            ConnectError = -1,
            //     
            InitializeError = -2,
            //     
            WrongState = -3,

            ArgumentError = -4,
            //     No error. (SCARD_S_SUCCESS)
            Success = 0,
        }

        public class M_SSF_Exception : Exception
        {
            public CodeOfError codeOfError { get; set; }
            public M_SSF_Exception() { }

            public M_SSF_Exception(string message, CodeOfError code)
                : base(message) { codeOfError = code; }
            public M_SSF_Exception(string message, System.Exception inner, CodeOfError code)
                : base(message, inner) { codeOfError = code; }

            public M_SSF_Exception(string message)
                : base(message) { codeOfError = CodeOfError.Success; }
            public M_SSF_Exception(string message, System.Exception inner)
                : base(message, inner) { codeOfError = CodeOfError.Success; }
            protected M_SSF_Exception(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
                : base(info, context) { codeOfError = CodeOfError.Success; }
        }
        public delegate void Message(string category, string msg);
        public delegate void SendProgress(string file, int progressPercentage, object userState);
        public delegate void SendCompleted(string file, ServiceSendingFile.ResultInfo result, Exception error, bool cancelled);
        //---------------------------------------------------------------------------
        //отправлеяем файлы в отдельном потоке. чтобы результат принимать в нём же.
        public class File4SendingResult
        {
            public ServiceSendingFile.ResultInfo result = null;
            public Exception error = null;
            public bool cancelled = false;
            public File4Sending ptrOwner = null;
            public File4SendingResult(){}
            public File4SendingResult(ServiceSendingFile.ResultInfo result, Exception error, bool cancelled, File4Sending ptrOwner)
            {
                this.result = result;
                this.error = error;
                this.cancelled = cancelled;
                this.ptrOwner = ptrOwner;
            }

        }

        public class File4Sending
        {
            public enum StatusFile
            {
                Default = 0,
                Error = -1,
                Sending = 1,
                Completed = 2,
                Cancelled = 3,
            }

            //
            public event Message OnMessage;
            public event SendProgress OnSendProgress;
            public event SendCompleted OnSendCompleted;

            private ServiceSendingFile ssfPtr;
            private int progress_ = 0;
            private StatusFile status_ = StatusFile.Default;

            private string fileName_ = ""; //
            private string idAT_ = "";

            public AutoResetEvent waitHandler = new AutoResetEvent(false); //будет в НЕсигнальном состоянии
            //true - объект в сигнальном состоянии
            //false - объект в несигнальном состоянии, и все потоки блокируются методом waitHandler.WaitOne() до ожидания сигнала
            //поток в состояние ожидания, пока объект waitHandler не будет переведен в сигнальное состояние.
            //
            public File4Sending()
            {
            }
            public void Init(string idAT, string file, ServiceSendingFile ssf) 
            {
                idAT_ = idAT;
                fileName_ = file;
                ssfPtr = ssf;
                //работа по отправке данных на сервис
                ssf.OnMessage += (string category, string msg, string fileName) =>
                {
                    if (fileName != fileName_) return;
                    this.Pulse();
                    if (OnMessage != null) OnMessage(category, String.Format("{0}: {1}", fileName_, msg));
                };
                ssf.OnSendProgress += (int progressPercentage, object userState, string fileName) =>
                {
                    if (fileName != fileName_) return;
                    this.Pulse();
                    progress_ = progressPercentage;
                    State = SetState(progress_);
                    if (OnSendProgress != null) OnSendProgress(fileName_, progressPercentage, userState);
                };
                ssf.OnSendCompleted += (ServiceSendingFile.ResultInfo result, Exception error, bool cancelled, string fileName) =>
                {
                    if (fileName != fileName_) return;
                    if (error != null) 
                    {
                        status_ = StatusFile.Error;
                        State = "Error sending";
                    }
                    else if (cancelled == false)
                    {
                        status_ = StatusFile.Cancelled;
                        State = "Cancelled sending";
                    }
                    else {
                        status_ = StatusFile.Completed;
                        State = "Completed sending";
                    }
                    this.Result = new File4SendingResult(result, error, cancelled, this);
                    this.Pulse();
                    if (OnSendCompleted != null) OnSendCompleted(fileName_, result, error, cancelled);
                };
            }

            public bool IsComplited()
            {
                return status_ == StatusFile.Completed;
            }
            public bool IsFinished()
            {
                bool isRun = ((status_ == StatusFile.Default) || (status_ == StatusFile.Sending));
                return !isRun;
            }

            private string SetState(int progress_)
            {
                return String.Format("{0} %", progress_);
            }
            public void Send()
            {
                status_ = StatusFile.Sending;
                ssfPtr.SendFile_Async(idAT_, fileName_);
            }
            public void Pulse()
            {
                this.waitHandler.Set();
            }

            public File4SendingResult Result = new File4SendingResult();
            public string State { get; set; }
            public string FileName { 
                get { return fileName_; } 
            }

        }
        //---------------------------------------------------------------------------
        static File4SendingResult DoSendingFile(File4Sending objFile)
        {
            //для синхронизации контекста
            //задача - вызов закачки, все события пробрасываем подписчикам
            //ожидаем событие(состояние) о завершении закачки

            objFile.Send();
            while (!objFile.IsFinished())
            {
                objFile.waitHandler.WaitOne();
                objFile.waitHandler.Reset();
            }
            return objFile.Result;
        }
        //---------------------------------------------------------------------------
        //public
        public event SendProgress OnSendProgress;
        public event SendCompleted OnSendCompleted;
        public event Message OnMessage;

        private ObservableCollection<File4Sending> _listSFile = new ObservableCollection<File4Sending>();
        private object _lock = new object();

        private ServiceSendingFile ssf = new ServiceSendingFile();

        public ObservableCollection<File4Sending> SendingFileList
        {
            get
            {
                return _listSFile;
            }
        }
        public MediatorSendFile()
        {
        }

        public void Init(Ice.Communicator communicator)
        {
            //OnMessage("SSF", "Init....");
            InitSendingFile(communicator);
        }

        public void Done()
        {
            ssf.Done();
        }

        private void InitSendingFile(Ice.Communicator communicator)
        {
            ssf.OnMessage += (string category, string msg, string fileName) =>
                        {
                            if (fileName.Length > 0)
                            {
                                msg += String.Format("; {0}", fileName);
                            }
                            DoMessage(category, msg);
                        };
            ssf.Init(communicator);
        }

        //добавляем файл для отправки на сервер
        private void RemoveComplitedItem()
        { 
            int cnt = _listSFile.Count-1;
            for (int i = cnt; i >= 0 ; i--)
            {
                File4Sending itm = _listSFile[i];
                if (itm.IsComplited())
                {
                    _listSFile.RemoveAt(i);
                }
            }
        }
        public int IndexOfFile(string fileName)
        {
            int index = -1;
            for(int i = 0; i < _listSFile.Count; i++)
            {
                File4Sending itm = _listSFile[i];
                if(itm.FileName == fileName)
                {
                    index = i;
                    break;
                }
            }
            return index;
        }
        public int Add(string idAT, string file)
        {
            DoMessage(GR_MAIN, "Добавление элемента в список");
            RemoveComplitedItem();
            File4Sending itm = null;
            int res = 0;
            int indxFnd = IndexOfFile(file);
            if (indxFnd > -1)
            {
                itm = _listSFile[indxFnd];

                DoMessage(GR_MAIN, String.Format("Элемент уже в списке отправке: {0}", itm.State));
                //_logger.trace("File4MongoList", String.Format("File exist: name - {0};", name));
            }
            if (itm == null)
            {
                itm = new File4Sending();
                itm.Init(idAT, file, ssf);
                itm.OnMessage += DoMessage;
                itm.OnSendProgress += DoSendProgress;
                //itm.OnSendCompleted += DoSendCompleted;
                _listSFile.Add(itm);

                //_logger.trace("File4MongoList", String.Format("Create file: name - {0};", name));
                Send( file);
            }
            return res;
        }

        private void Send( string file)
        {
            DoMessage(GR_MAIN, "Элемент на отправку");
            int indxFnd = IndexOfFile(file);
            File4Sending itm = indxFnd > -1 ?_listSFile[indxFnd] : null;
            if (itm == null)
            {
                throw new M_SSF_Exception(String.Format("{0} - not found!!!", file), CodeOfError.ArgumentError);
            }
            try
            {
                Task<File4SendingResult> taskSnd = new Task<File4SendingResult>(() => { return DoSendingFile(itm); });
                taskSnd.ContinueWith(x =>
                    {
                        File4SendingResult resObj = taskSnd.Result;
                        if (resObj == null)
                        {
                            GlobalLog.error("RESULT sendingFile is NULL!!!");
                            Console.WriteLine("RESULT sendingFile is NULL!!!");
                        }
                        else
                        {
                            var nameFile = (resObj.ptrOwner == null)
                                ? (resObj.result == null) ?  "unknown" : resObj.result.file
                                : resObj.ptrOwner.FileName;
                            DoSendCompleted(nameFile, resObj.result, resObj.error, resObj.cancelled);
                        }
                    }
                    //, TaskScheduler.FromCurrentSynchronizationContext()
                );
                //TaskScheduler.
                taskSnd.Start();

                //itm.Send();


            }
            catch (Exception ex)
            {
                GlobalLog.error("SSF exception: " + ex);
                DoSendCompleted(itm.FileName, null, ex, false);
            }

        }
        public ItemLoadFileState GetFileInfo(string idAT, string file)
        {
            ItemLoadFileState resObj = null;
            try
            {
                Monitoring.FileInfo objInf = ssf.GetFileInfo(idAT, file);
                resObj = ItemLoadFileState.FromIce(objInf);

            }
            catch (ServiceSendingFile.SSF_Exception ex0)
            {
                resObj = new ItemLoadFileState();
                resObj.code = (int)ex0.codeOfError;
                GlobalLog.error("SSF exception: " + ex0);
                DoMessage("Exception", ex0.Message);
            }
            catch (Exception ex1)
            {
                resObj = new ItemLoadFileState();
                resObj.code = -1;
                GlobalLog.error("SSF exception: " + ex1);
                DoMessage("Exception", ex1.Message);
            }
            return resObj;
        }

        public void DoMessage(string category, string msg)
        {
            GlobalLog.trace(category, msg);
            if (OnMessage != null) OnMessage(category, msg);
        }
        private void DoSendProgress(string file, int progressPercentage, object userState)
        {
            GlobalLog.trace("SSF", String.Format("Обработано {0} %; {1}", progressPercentage, (string)userState));
            if (OnSendProgress != null) OnSendProgress(file, progressPercentage, userState);
        }
        private void DoSendCompleted(string file, ServiceSendingFile.ResultInfo result, Exception error, bool cancelled)
        {
            if (OnSendCompleted != null) OnSendCompleted(file, result, error, cancelled);
            if (cancelled)
                GlobalLog.trace("SSF", "Работа была прервана пользователем!");
            else if (error != null)
                GlobalLog.error("SSF exception: " + error);
            else
                GlobalLog.trace("SSF", "Работа закончена успешно. Результат - " + result);
        }
    }


}
