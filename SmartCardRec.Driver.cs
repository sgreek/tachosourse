﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// описание структуры карточки водителя (со всеми структурами элементарных файлов) 
namespace TachoSource
{
    public partial class SmartCardRec
    {
        public static byte TYPE_WITH_GOST = 0;  //Card_Certificate_GOST - (DF Tachograph 0500 - Выбор по имени (AID))
        public static byte TYPE_ESTR = 1;       //  (DF Tachograph 0500 - Выбор элементарного файла (EF) по идентификатору файла)

        public class IdentificationInfo //Это класс, который хранит данные
        {

            public string cardNumber;
            public string holderSurname;
            public string holderFirstNames;
            public IdentificationInfo() { }
            public IdentificationInfo(string number, string surname, string firstname) { cardNumber = number; holderSurname = surname; holderFirstNames = firstname; }
            public override string ToString() { return String.Format("{0}_{1}_{2}", holderFirstNames, holderSurname, cardNumber); }

            // Явное преобразование типа к string
            public static explicit operator string(IdentificationInfo obj)
            {
                return obj.ToString();
            }
        }

        /*
            EF Identification 143 
                CardIdentification  65
                    CardIssuingMemberState 1 {00}
                    cardNumber 16 {20..20}
                    cardIssuingAuthorityName 36 {20..20}
                    cardIssueDate 4 {00..00}
                    cardValidityBegin 4 {00..00}
                    cardExpiryDate 4 {00..00}
                DriverCardHolderIdentification 78
                    cardHolderName 72
                        holderSurname 36 {00, 20..20}
                        holderFirstNames 36 {00, 20..20}
                    cardHolderBirthDate 4 {00..00}
                    cardHolderPreferredLanguage 2 {20 20}          
         */

        static public IdentificationInfo GetCaptionDriverCard(block_record[] driverCard)
        {
            //формируем надпись
            IdentificationInfo res = null;
            //найдём блок "EF Identification" и извлечём из него данные
            block_record blockIdentity = GetBlock4Id(driverCard, new byte[] {0x05, 0x20});
            if ((blockIdentity != null) && (blockIdentity.sizeBlock == blockIdentity.data.Length)) 
            {
                byte[] cardNumber = arrayCopy(blockIdentity.data, 1, 16);
                byte[] holderSurname = arrayCopy(blockIdentity.data, 65, 36);
                byte[] holderFirstNames = arrayCopy(blockIdentity.data, 65 + 36, 36);

                res = new IdentificationInfo(convertIntoString(cardNumber), bcd2Str(holderSurname), bcd2Str(holderFirstNames));
            }
            return res;
        }

        static public block_record[] GetDriverCardTemplate()
        {
            /*
            структура файлов карты водителя
            MF 3F00
                EF ICC 0002 - 25
                EF IC 0005 - 8
                DF Tachograph 0500 
                    EF Application_Identification 0501 - 10
                    EF Card_Certificate_GOST C200 - 770
                    EF Key_Identificators C201 - 16
                    EF CA_Certificate_GOST C208 - 655
                    EF Identification 0520 - 143
                    EF Card_Download 050E - 4
                    EF Driving_Licence_Info 0521 - 53
                    EF Events_Data 0502 n1 NoOfEventsPerType 6 12 (min max record count in struct)
                    EF Faults_Data 0503 n2 NoOfFaultsPerType 12 24
                    EF Driver_Activity_Data 0504 n6 CardActivityLengthRange 5 544 байта (28 дней * 93 изменения вида деятельности) 13 776 байт (28 дней * 240 изменений вида деятельности)
                    EF Vehicles_Used 0505 n3 NoOfCardVehicleRecords 84 200
                    EF Places 0506 n4 NoOfCardPlaceRecords 84 112
                    EF Current_Usage 0507 - 19
                    EF Control_Activity_Data 0508 - 46
                    EF Specific_Conditions 0522 - 280
            */

            block_record[] driverCardTemplate = 
            {
                //MF 3F00
                new block_record(){ idBlock = new byte[]{0x00, 0x02}, sizeBlock=25, name="EF ICC"},
                new block_record(){ idBlock = new byte[]{0x00, 0x05}, sizeBlock=8, name="EF IC"},
                //DF Tachograph(ASCII:FF 54 41 43 48 4F) 0500 
                new block_record(){ idBlock = new byte[]{0xFF, 0x54, 0x41, 0x43, 0x48, 0x4F}, sizeBlock=0, name="DF Tachograph"},
                new block_record(){ idBlock = new byte[]{0x05, 0x01}, sizeBlock=10, name="EF Application_Identification"},
                new block_record(){ idBlock = new byte[]{0x05, 0x01}, sizeBlock=10, name="EF Application_Identification(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0xC2, 0x00}, sizeBlock=770, name="EF Card_Certificate_GOST"},
                new block_record(){ idBlock = new byte[]{0xC2, 0x01}, sizeBlock=16, name="EF Key_Identificators"},
                new block_record(){ idBlock = new byte[]{0xC2, 0x01}, sizeBlock=16, name="EF Key_Identificators(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0xC2, 0x08}, sizeBlock=655, name="EF CA_Certificate_GOST"},
                new block_record(){ idBlock = new byte[]{0x05, 0x20}, sizeBlock=143, name="EF Identification"},
                new block_record(){ idBlock = new byte[]{0x05, 0x20}, sizeBlock=128, name="EF Identification(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x0E}, sizeBlock=4, name="EF Card_Download"},
                new block_record(){ idBlock = new byte[]{0x05, 0x0E}, sizeBlock=4, name="EF Card_Download(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x21}, sizeBlock=53, name="EF Driving_Licence_Info"},
                new block_record(){ idBlock = new byte[]{0x05, 0x21}, sizeBlock=53, name="EF Driving_Licence_Info(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x02}, sizeBlock=144, name="EF Events_Data", sizeRecord = 24*6},
                new block_record(){ idBlock = new byte[]{0x05, 0x02}, sizeBlock=144, name="EF Events_Data(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x03}, sizeBlock=24, name="EF Faults_Data", sizeRecord = 24},
                new block_record(){ idBlock = new byte[]{0x05, 0x03}, sizeBlock=24, name="EF Faults_Data(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x04}, sizeBlock=13780, name="EF Driver_Activity_Data"},
                //new block_record(){ idBlock = new byte[]{0x05, 0x04}, sizeBlock=4, name="EF Driver_Activity_Data" , getSizeBlock = new SizeBlockOfBlock(SizeBlockOfCardDriverActivity)},
                new block_record(){ idBlock = new byte[]{0x05, 0x04}, sizeBlock=4, name="EF Driver_Activity_Data(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x05}, sizeBlock=6202, name="EF Vehicles_Used"},
                //new block_record(){ idBlock = new byte[]{0x05, 0x05}, sizeBlock=2, name="EF Vehicles_Used", getSizeBlock = new SizeBlockOfBlock(SizeBlockOfCardVehiclesUsed)},
                new block_record(){ idBlock = new byte[]{0x05, 0x05}, sizeBlock=2, name="EF Vehicles_Used(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x06}, sizeBlock=1, name="EF Places", sizeRecord = 10},
                new block_record(){ idBlock = new byte[]{0x05, 0x06}, sizeBlock=1, name="EF Places(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x07}, sizeBlock=19, name="EF Current_Usage"},
                new block_record(){ idBlock = new byte[]{0x05, 0x07}, sizeBlock=19, name="EF Current_Usage(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x08}, sizeBlock=46, name="EF Control_Activity_Data"},
                new block_record(){ idBlock = new byte[]{0x05, 0x08}, sizeBlock=46, name="EF Control_Activity_Data(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x22}, sizeBlock=280, name="EF Specific_Conditions"},
                new block_record(){ idBlock = new byte[]{0x05, 0x22}, sizeBlock=280, name="EF Specific_Conditions(signature)", isSgn=true},
            };
            List<block_record> newCard = new List<block_record>();
            for (var i = 0; i < driverCardTemplate.Length; i++)
            {
                newCard.Add((block_record)driverCardTemplate[i].Clone());
            }
            return newCard.ToArray();
        }

        static public block_record[] GetDriverCardTemplateESTR()
        {
            /*
            структура файлов карты водителя
            MF 3F00
                EF ICC 0002 - 25
                EF IC 0005 - 8
                DF Tachograph 0500 
                    EF Application_Identification 0501 - 10
                    !!!---EF Card_Certificate_GOST C200 - 770
                    EF Key_Identificators C201 - 16
                    !!!---EF CA_Certificate_GOST C208 - 655
                    EF Identification 0520 - 143
                    EF Card_Download 050E - 4
                    EF Driving_Licence_Info 0521 - 53
                    EF Events_Data 0502 n1 NoOfEventsPerType 6 12 (min max record count in struct)
                    EF Faults_Data 0503 n2 NoOfFaultsPerType 12 24
                    EF Driver_Activity_Data 0504 n6 CardActivityLengthRange 5 544 байта (28 дней * 93 изменения вида деятельности) 13 776 байт (28 дней * 240 изменений вида деятельности)
                    EF Vehicles_Used 0505 n3 NoOfCardVehicleRecords 84 200
                    EF Places 0506 n4 NoOfCardPlaceRecords 84 112
                    EF Current_Usage 0507 - 19
                    EF Control_Activity_Data 0508 - 46
                    EF Specific_Conditions 0522 - 280
            */

            block_record[] driverCardTemplateESTR = 
            {
                //MF 3F00
                //!!!---new block_record(){ idBlock = new byte[]{0x00, 0x02}, sizeBlock=25, name="EF ICC"},
                //!!!---new block_record(){ idBlock = new byte[]{0x00, 0x05}, sizeBlock=8, name="EF IC"},
                //DF Tachograph(ASCII:FF 54 41 43 48 4F) 0500 
                new block_record(){ idBlock = new byte[]{0xFF, 0x54, 0x41, 0x43, 0x48, 0x4F}, sizeBlock=0, name="DF Tachograph"},
                //code new block_record(){ idBlock = new byte[]{0x05, 0x00}, sizeBlock=0, name="DF Tachograph"},
                new block_record(){ idBlock = new byte[]{0x05, 0x01}, sizeBlock=10, name="EF Application_Identification"},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x01}, sizeBlock=10, name="EF Application_Identification(signature)", isSgn=true},
                //!!!---new block_record(){ idBlock = new byte[]{0xC2, 0x00}, sizeBlock=770, name="EF Card_Certificate_GOST"},
                //!!!---new block_record(){ idBlock = new byte[]{0xC2, 0x01}, sizeBlock=16, name="EF Key_Identificators"},
                //!!!---new block_record(){ idBlock = new byte[]{0xC2, 0x01}, sizeBlock=16, name="EF Key_Identificators(signature)", isSgn=true},
                //!!!---new block_record(){ idBlock = new byte[]{0xC2, 0x08}, sizeBlock=655, name="EF CA_Certificate_GOST"},
                new block_record(){ idBlock = new byte[]{0x05, 0x20}, sizeBlock=143, name="EF Identification"},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x20}, sizeBlock=128, name="EF Identification(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x0E}, sizeBlock=4, name="EF Card_Download"},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x0E}, sizeBlock=4, name="EF Card_Download(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x21}, sizeBlock=53, name="EF Driving_Licence_Info"},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x21}, sizeBlock=53, name="EF Driving_Licence_Info(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x02}, sizeBlock=144, name="EF Events_Data", sizeRecord = 24*6},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x02}, sizeBlock=144, name="EF Events_Data(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x03}, sizeBlock=24, name="EF Faults_Data", sizeRecord = 24},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x03}, sizeBlock=24, name="EF Faults_Data(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x04}, sizeBlock=13780, name="EF Driver_Activity_Data"},
                //new block_record(){ idBlock = new byte[]{0x05, 0x04}, sizeBlock=4, name="EF Driver_Activity_Data" , getSizeBlock = new SizeBlockOfBlock(SizeBlockOfCardDriverActivity)},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x04}, sizeBlock=4, name="EF Driver_Activity_Data(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x05}, sizeBlock=6202, name="EF Vehicles_Used"},
                //new block_record(){ idBlock = new byte[]{0x05, 0x05}, sizeBlock=2, name="EF Vehicles_Used", getSizeBlock = new SizeBlockOfBlock(SizeBlockOfCardVehiclesUsed)},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x05}, sizeBlock=2, name="EF Vehicles_Used(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x06}, sizeBlock=1, name="EF Places", sizeRecord = 10},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x06}, sizeBlock=1, name="EF Places(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x07}, sizeBlock=19, name="EF Current_Usage"},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x07}, sizeBlock=19, name="EF Current_Usage(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x08}, sizeBlock=46, name="EF Control_Activity_Data"},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x08}, sizeBlock=46, name="EF Control_Activity_Data(signature)", isSgn=true},
                new block_record(){ idBlock = new byte[]{0x05, 0x22}, sizeBlock=280, name="EF Specific_Conditions"},
                //!!!---new block_record(){ idBlock = new byte[]{0x05, 0x22}, sizeBlock=280, name="EF Specific_Conditions(signature)", isSgn=true},
            };
            List<block_record> newCard = new List<block_record>();
            for (var i = 0; i < driverCardTemplateESTR.Length; i++)
            {
                newCard.Add((block_record)driverCardTemplateESTR[i].Clone());
            }
            return newCard.ToArray();
        }



        /*
         записи о деятельности водителя
         CardDriverActivity: = ПОСЛЕДОВАТЕЛЬНОСТЬ {
                activityPointerOldestDayRecord ЦЕЛОЕ ЧИСЛО (0.. CardActivityLengthRange-1) 2 байта,
                activityPointerNewestRecord ЦЕЛОЕ ЧИСЛО (0.. CardActivityLengthRange-1) 2 байта,
                activityDailyRecords ОКТЕТНАЯ СТРОКА (РАЗМЕР(CardActivityLengthRange))
         }
         activityPointerOldestDayRecord - указание на начало блока памяти (число байтов с начала строки) для хранения самой старой ежедневной записи в строке activityDailyRecords.
        */
        static private ushort SizeBlockOfCardDriverActivity(byte[] data)
        {
            //т.е. возвращаем последний номер байта записей activityDailyRecords
            if (data.Length < 4) return 0;
            byte[] posByte = { data[3], data[2] };
            ushort pos = BitConverter.ToUInt16(posByte, 0);
            return pos > 1 ? (ushort)(pos - 1) : (ushort)0;
        }

        /*
        использованное транспортное средство
        CardVehiclesUsed := ПОСЛЕДОВАТЕЛЬНОСТЬ {
            vehiclePointerNewestRecord ЦЕЛОЕ ЧИСЛО (0..NoOfCardVehicleRecords-1),
            cardVehicleRecords УСТАНОВЛЕННЫЙ РАЗМЕР       (NoOfCardVehicleRecords) ЗАПИСИ    CardVehicleRecord
        }
        vehiclePointerNewestRecord - индекс последней обновленной записи, касающейся транспортного средства.
         */
        static private ushort SizeBlockOfCardVehiclesUsed(byte[] data)
        {
            const ushort SIZE_ONE_REC = 24;
            //т.е. возвращаем произведение последней обновлённой записи на размер записи
            if (data.Length < 2) return 0;
            byte[] indexByte = { data[1], data[0] };
            ushort index = BitConverter.ToUInt16(indexByte, 0);
            return (ushort)((index + 1) * SIZE_ONE_REC);
        }

        /*
        информация, в которых начинаются и/или заканчиваются ежедневные периоды работы
        CardPlaceDailyWorkPeriod ::= ПОСЛЕДОВАТЕЛЬНОСТЬ {
            placePointerNewestRecord ЦЕЛОЕ ЧИСЛО (0 .. NoOfCardPlaceRecords-1),
            placeRecords УСТАНОВЛЕННЫЙ РАЗМЕР (NoOfCardPlaceRecords) ЗАПИСИ PlaceRecord
        }
        placePointerNewestRecord - индекс последней обновленной записи данных о месте.         * 
        */
        static private ushort SizeBlockOfCardPlaceDailyWorkPeriod(byte[] data)
        {
            //!!! содержится 0, хотя данные есть
            const ushort SIZE_ONE_REC = 10;
            //т.е. возвращаем произведение последней обновлённой записи на размер записи
            if (data.Length < 1) return 0;
            ushort index = (ushort)data[0];
            return (ushort)((index + 1) * SIZE_ONE_REC);
        }
    }
}
