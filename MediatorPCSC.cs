﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PCSC;
using PCSC.Iso7816;

namespace TachoSource
{
    public class MediatorPCSC
    {
        //
        static public string GetMsgApdu(byte SW1, byte SW2)
        {
            if ((SW1 == 0x90) && (SW2 == 0x00))
            {
                return "Is Ok";
            }
            else if ((SW1 == 0x6A) && (SW2 == 0x82))
            {
                return "file not found";
            }
            else if ((SW1 == 0x6A) && (SW2 == 0x88))
            {
                return "CHV data not found";
                //исходные данные CHV не найдены
            }
            //
            else if ((SW1 == 0x69) && (SW2 == 0x86))
            {
                return "file is not trunsparent";
                // Если выбранный файл не является транспарентным файлом, состояние обработки выдается в виде ‘6986’.
            }
            else if ((SW1 == 0x69) && (SW2 == 0x85))
            {
                return "application not select";
                // Если приложение не выбрано, состояние обработки выдается в виде ‘6985
            }
            else if ((SW1 == 0x69) && (SW2 == 0x83))
            {
                return "CHV data is bloked";
                // данные CHV заблокированы, счетчик оставшихся попыток CHV показывает ноль
            }
            else if ((SW1 == 0x69) && (SW2 == 0x82))
            {
                return "Access is denied";
            }
                //
            else if ((SW1 == 0x6B))
            {
                return "shift > EF size";
            }
            else if ((SW1 == 0x67))
            {
                return "shift + Le > EF size";
            }
            else if ((SW1 == 0x63))
            {
                return "Compare CHV is wrong";
                //сравнение дало неправильные результаты
            }
            else if (((SW1 == 0x64) && (SW2 == 0x00)) || ((SW1 == 0x65) && (SW2 == 0x81)))
            {
                return "Target data is corrupted";
                // Если выбранный элементарный файл считается поврежденным (ошибки в атрибутах файла или целостности записанных данных), состояние обработки выдается в виде ‘6400’ или ‘6581’.
            }
            else
            {
                return "Common error";
            }
        }        //     Error and return codes.
        public enum CodeOfError
        {
            // ошибка конекта к карте
            ConnectError = -1,
            //     
            InitializeError = -2,
            //     
            WrongState = -3,
            //     
            NotBeginTransaction = -4,
            //     
            NotReadBinaryData = -5,
            //     No error. (SCARD_S_SUCCESS)
            Success = 0,
        }

        public enum StateOfPIN
        {
            // 
            Default = 0,
            //     
            Error = -1,
            //     
            HaveResult = 2,
        };
        public class CardDataPIN
        {
            private byte[] pinData = null;
            private string cardName = "";
            private byte[] resData;
            private string msgRes = "";
            public StateOfPIN state = StateOfPIN.Default;
            public string CardName
            {
                get
                {
                    return cardName;
                }
            }
            public byte[] pin
            {
                get
                {
                    return pinData;
                }
            }
            public void Reset()
            {
                state = StateOfPIN.Default;
                cardName = "";
                pinData = null;
                resData = null;
                msgRes = "";
            }
            public void SetResult(string card, byte[] pin, byte[] res, string msg)
            {
                state = StateOfPIN.HaveResult;
                cardName = card;
                pinData = null;
                if (pin!=null) pinData = pin;
                resData = null;
                if (res != null) resData = res;
                msgRes = msg;
            }
            public int GetResult(string card, out int counter, out string msg)
            {
                counter = -1;
                msg = "Fail";
                if (this.state == StateOfPIN.Error)
                {
                    msg = "Error";
                    return -5;
                }
                if (this.state == StateOfPIN.Default)
                {
                    msg = "State is default";
                    return -4;
                }
                if (this.cardName != card){
                    msg = "Cardreader has changed.";
                    return -3;
                }

                if(resData == null || resData.Length < 2){
                    msg = "Data is absent";
                    return -2;
                }
                msg = msgRes;
                byte SW1 = resData[0];
                byte SW2 = resData[1];
                int res = -1;
                if((SW1 == 0x90) && (SW2 == 0x00))
                {
                    res = 0;
                }
                else if(SW1 == 0x63)
                {
                    counter = (SW1 & 0x0F);
                }
                return res;
            }
            public bool NeedCheck()
            {
                return (state != StateOfPIN.Default);
            }

        }
        public class M_PCSC_Exception : Exception
        {
            public CodeOfError codeOfError { get; set; }
            public M_PCSC_Exception () { }

            public M_PCSC_Exception(string message, CodeOfError code)
                : base(message) { codeOfError = code; }
            public M_PCSC_Exception(string message, System.Exception inner, CodeOfError code)
                : base(message, inner) { codeOfError = code; }

            public M_PCSC_Exception(string message)
                : base(message) { codeOfError = CodeOfError.Success; } 
            public M_PCSC_Exception(string message, System.Exception inner)
                : base(message, inner) { codeOfError = CodeOfError.Success; }
            protected M_PCSC_Exception(System.Runtime.Serialization.SerializationInfo info,  System.Runtime.Serialization.StreamingContext context)
                : base(info, context) { codeOfError = CodeOfError.Success; } 
        }

        public static string CardStateInfoDescript(uint code)
        {
            if (code == (uint)CardStateInfo.Unaware) return "Application wants status";
            else
            {
                string txtSt = "";
                if ((code & (int)CardStateInfo.Ignore) != 0) txtSt = "Ignore this reader ";
                if ((code & (int)CardStateInfo.Changed) != 0) txtSt += "State has changed ";
                if ((code & (int)CardStateInfo.Unknown) != 0) txtSt += "Reader unknown ";
                if ((code & (int)CardStateInfo.Unavailable) != 0) txtSt += "Status unavailable ";
                if ((code & (int)CardStateInfo.Empty) != 0) txtSt += "Card removed ";
                if ((code & (int)CardStateInfo.Present) != 0) txtSt += "Card inserted ";
                if ((code & (int)CardStateInfo.AtrMatch) != 0) txtSt += "ATR matches card ";
                if ((code & (int)CardStateInfo.Exclusive) != 0) txtSt += "Exclusive Mode ";
                if ((code & (int)CardStateInfo.InUse) != 0) txtSt += "In use ";
                if ((code & (int)CardStateInfo.Mute) != 0) txtSt += "Unresponsive card ";
                if ((code & (int)CardStateInfo.Unpowered) != 0) txtSt += "Unpowered card ";
                return (txtSt.Length>0) ? txtSt : "unknown state";
            }
        }
        public enum CardStateInfo
        {
            //[Description("Application wants status")]
            Unaware = 0,
            //[Description("Ignore this reader")]
            Ignore = 1,
            //[Description("State has changed")]
            Changed = 2,
            //[Description("Reader unknown")]
            Unknown = 4,
            //[Description("Status unavailable")]
            Unavailable = 8,
            //[Description("Card removed")]
            Empty = 16,
            //[Description("Card inserted")]
            Present = 32,
            //[Description("ATR matches card")]
            AtrMatch = 64,
            //[Description("Exclusive Mode")]
            Exclusive = 128,
            //[Description("In use")]
            InUse = 256,
            //[Description("Unresponsive card")]
            Mute = 512,
            //[Description("Unpowered card")]
            Unpowered = 1024,
        }
        public class M_PCSC_StateInfo //Это класс, который хранит данные
        {
            public string readerName;
            public uint code;
            public string message;
            public M_PCSC_StateInfo() { }
            public M_PCSC_StateInfo(string reader, uint cd, string msg) { readerName = reader; code = cd; message = msg; }
        }
        //состояние считывателя
        //
        //нет выбранного считывателя
        //готов к считыванию
        //читает данные
        public const int ST_ERROR = -3;
        public const int ST_NO_READER = -2;
        public const int ST_NOT_INITIALIZE = -1;
        public const int ST_DEFAULT = 0;
        public const int ST_CARD_REMOVED = 2;
        public const int ST_READING = 3;
        public const int ST_DISCONNECTED = 4;
        public const int ST_READY = 10;
        public const int ST_CARD_INSERTED = 12;
        public const int ST_CONNECTED = 13;

        public readonly string SENDER_NAME = "MediatorPCSC";

        private int state_ = ST_NOT_INITIALIZE;

        private string[] readerNames_;
        private SCardMonitor monitor_ = null; //объект работы с картами
        private bool autoState_ = true;

        //подключаемся к конкретному ридеру из списка readerNames_
        private SCardContext context_ = null;
        private SCardReader rfidReader_ = null;
        private CardDataPIN pinInfo_ = new CardDataPIN();

        //события
        //изменение состояния тип: код состояния и сообщение
        public delegate void PCSCChangeState(object sender, M_PCSC_StateInfo info);
        public delegate void PCSCMessage(string category, string msg);

        public event PCSCChangeState ChangeState;
        public event PCSCMessage Message;
        /*
         Получаем список считывателей
        */
        public string[] GetReaders()
        {
            readerNames_ = null;
            using (var context = new SCardContext())
            {
                    context.Establish(SCardScope.System);
                    readerNames_ = context.GetReaders();
            }
            if (readerNames_ == null || readerNames_.Length == 0)
            {
                using (var context = new SCardContext())
                {
                    context.Establish(SCardScope.User);
                    readerNames_ = context.GetReaders();
                }
            }

            if (readerNames_ == null || readerNames_.Length == 0)
            {
                state_ = ST_NO_READER;
                Message(SENDER_NAME, "There are currently no readers installed");
                return null;
            }
            return readerNames_;
        }
        //Пин проверяется в два этапа. Устанавливаются данные. Потом проверка. Чтобы при повторном конекте использовать результать предыдущей попытки
        //т.е. не делать неверной "лишней" проверки. для визуального интерфейса удобно разделить операции проверки и считывания данных (из-за конекта к карте)
        public CardDataPIN DataPIN
        {
            get
            {
                return pinInfo_;
            }
        }
        public void ResetPIN()
        { 
            //Сбрасываем проверку пина
            if (pinInfo_ != null) 
            {
                pinInfo_.Reset();
            }
        }
        public void CheckPin(string readerName, byte[] valPin)
        {
            try
            {
                this.ConnectCard(readerName);

                string msg = "Error";
                byte[] resData = null;
                CompearePIN(readerName, valPin, out resData, out msg);
                pinInfo_.SetResult(readerName, valPin, resData, msg);
            }
            catch (Exception ex)
            {
                pinInfo_.state = StateOfPIN.Error;
                state_ = ST_ERROR;
                throw new M_PCSC_Exception(ex.Message, ex);
            }
            finally
            {
                this.DisonnectCard();
            }

        }

        public void EnterPin()
        {
            try
            {
                string msg = "";
                byte[] resData = null;

                CompearePIN(pinInfo_.CardName, pinInfo_.pin, out resData, out msg);
                pinInfo_.SetResult(pinInfo_.CardName, pinInfo_.pin, resData, msg);
            }
            catch (Exception ex)
            {
                pinInfo_.SetResult(pinInfo_.CardName, pinInfo_.pin, null, "Error");
                pinInfo_.state = StateOfPIN.Error;
                state_ = ST_ERROR;
                throw new M_PCSC_Exception(ex.Message, ex);
            }
        }

        public int State
        {
            get
            {
                return state_;
            }
        }

        public bool AutoState
        {
            get
            {
                return autoState_;
            }
            set
            {
                autoState_ = value;
                if (monitor_ == null) return;
                try
                {
                    if (value == true)
                    {
                        Start();
                    }
                    else
                    {
                        Stop();
                    }
                }
                catch (Exception ex)
                {
                    Message(SENDER_NAME, String.Format("Set property reader autostate: {0}", ex));
                }

            }
        }
        /*
         Инициализируем объект работы с картами 
        */
        public void Initialize()
        {
            // Create a monitor object with its own PC/SC context. 
            // The context will be released after monitor.Dispose()

            if (monitor_ != null) return;
            try
            {
                monitor_ = new SCardMonitor(new SCardContext(), SCardScope.System);

                // Point the callback function(s) to the anonymous & static defined methods below.
                monitor_.CardInserted += (sender, args) => DisplayEvent("CardInserted", args);
                monitor_.CardRemoved += (sender, args) => DisplayEvent("CardRemoved", args);
                monitor_.Initialized += (sender, args) => DisplayEvent("Initialized", args);
                monitor_.StatusChanged += StatusChanged;
                monitor_.MonitorException += MonitorException;
            }
            catch (Exception ex)
            {
                monitor_ = null;
                Message(SENDER_NAME, String.Format("Can not initialize: {0}", ex));
            }
        }

        public void Start()
        {
            Initialize();
            if (monitor_ == null) {
                throw new M_PCSC_Exception("Can not initialize", CodeOfError.InitializeError);
            } 
            if (readerNames_ == null || readerNames_.Length == 0)
            {
                GetReaders();
            }
            try
            {
               if (autoState_ == false) return;
               monitor_.Start(readerNames_);

            }
            catch (Exception ex)
            {
                state_ = ST_ERROR;
                throw new M_PCSC_Exception(ex.Message, ex);
            }
        }

        public void Stop()
        {
            // Stop monitoring
            state_ = ST_DEFAULT;
            if (monitor_ == null) return;
            monitor_.Cancel();
        }

        public void Done()
        {
            // Dispose monitor resources (SCardContext)
            // The context will be released after monitor.Dispose()
            Message(SENDER_NAME, "Dispose monitor resources.");
            if (monitor_ != null)
            {
                monitor_.Dispose();
                monitor_ = null;
            }
            state_ = ST_NOT_INITIALIZE;
        }

        public void ConnectCard(string readerName)
        {
            if(context_ != null) return;
            context_ = new SCardContext();
            context_.Establish(SCardScope.System);

            rfidReader_ = new SCardReader(context_);
            var sc = rfidReader_.Connect(readerName, SCardShareMode.Shared, SCardProtocol.Any);
            if (sc != SCardError.Success)
            {
                throw new M_PCSC_Exception(String.Format("Could not connect to reader {0}:\n{1}", readerName, SCardHelper.StringifyError(sc)), CodeOfError.ConnectError);
            }
            state_ = ST_CONNECTED;
        }

        public void DisonnectCard()
        {
            if (rfidReader_ == null) return;
            if (rfidReader_.IsConnected) rfidReader_.Disconnect(SCardReaderDisposition.Reset);

            rfidReader_.Dispose();
            rfidReader_ = null;

            context_.Dispose();
            context_ = null;
            state_ = ST_DISCONNECTED;
        }
        

        //события которые получаем от Монитора который следит за состоянием
        private void DisplayEvent(string eventName, CardStatusEventArgs unknown)
        {
            if (eventName == "Initialized")
            {
                state_ = ST_READY;
            }
            else if (eventName == "CardInserted")
            {
                state_ = ST_CARD_INSERTED;
            }
            else if (eventName == "CardRemoved")
            {
                state_ = ST_CARD_REMOVED;
            }
            ChangeState(this, new M_PCSC_StateInfo(unknown.ReaderName, (uint)unknown.State, eventName));

            string msg = string.Format("Event for reader: {0}; ATR: {1}; State: {2}", unknown.ReaderName, BitConverter.ToString(unknown.Atr ?? new byte[0]), unknown.State);
            Message(SENDER_NAME, msg);
        }

        private void StatusChanged(object sender, StatusChangeEventArgs args)
        {
            ChangeState(this, new M_PCSC_StateInfo(args.ReaderName, (uint)args.NewState, ""));
            string msg = string.Format("StatusChanged Event for reader: {0}; ATR: {1}; Last state: {2}; New state: {3}", args.ReaderName, BitConverter.ToString(args.Atr ?? new byte[0]), args.LastState, args.NewState);
            Message(SENDER_NAME, msg);
        }

        private void MonitorException(object sender, PCSCException ex)
        {
            state_ = ST_ERROR;
            string msg = string.Format("Monitor exited due an error: {0}", SCardHelper.StringifyError(ex.SCardError));
            Message(SENDER_NAME, msg);
        }

        //выполнение команды 
        delegate void FunctnNotPrm();
        private void ExecuteCommand(string readerName, FunctnNotPrm funct)
        {

            ConnectCard(readerName);
            if (rfidReader_ == null)
            {
                throw new M_PCSC_Exception("Error inicialize", CodeOfError.InitializeError);
            }

            if (state_ < ST_READY)
            {
                throw new M_PCSC_Exception("Wrong state", CodeOfError.WrongState);
            }
            state_ = ST_READING;
            try
            {
                var sc = rfidReader_.BeginTransaction();
                if (sc != SCardError.Success)
                {
                    state_ = ST_ERROR;
                    throw new M_PCSC_Exception("Could not begin transaction.", CodeOfError.NotBeginTransaction);
                }
                //----------------------------------------------------------------------------------------------
                funct();
                //----------------------------------------------------------------------------------------------
                state_ = ST_READY;
            }
            finally
            {
                rfidReader_.EndTransaction(SCardReaderDisposition.Leave);
            }

        }
        
        public void CompearePIN(string readerName, byte[] valPin, out byte[] resData, out string msgOut)
        {
            string msg = "";
            byte[] pinData = new byte[8];
            for (int i = 0; i < 8; i++)
            {
                pinData[i] = (valPin.Length > i) ? valPin[i] : (byte)0xFF;
            }

            ConnectCard(readerName);
            if (rfidReader_ == null)
            {
                throw new M_PCSC_Exception("Error inicialize", CodeOfError.InitializeError);
            }

            if (state_ < ST_READY)
            {
                throw new M_PCSC_Exception("Wrong state", CodeOfError.WrongState);
            }
            state_ = ST_READING;
            try
            {
                /*
                var sc = rfidReader_.BeginTransaction();
                if (sc != SCardError.Success)
                {
                    state_ = ST_ERROR;
                    throw new M_PCSC_Exception("Could not begin transaction.", CodeOfError.NotBeginTransaction);
                }
                /**/

                List<byte> data = new List<byte>();
                ResponseApdu responseApdu2;
                var apdu2 = new CommandApdu(IsoCase.Case4Short, rfidReader_.ActiveProtocol)
                {
                    CLA = 0x00,
                    INS = 0x20,
                    P1 = 0x00,
                    P2 = 0x00,
                    Data = pinData  // 
                };

                Transmit(rfidReader_, apdu2, out responseApdu2);
                msgOut = GetMsgApdu(responseApdu2, null);
                Message(SENDER_NAME, msg);
                data.Add(responseApdu2.SW1);
                data.Add(responseApdu2.SW2);
                if ((responseApdu2.SW1 != 0x90) && (responseApdu2.SW2 != 0x00))
                {
                    var chunk = responseApdu2.GetData();
                    int sizeChunk = chunk.Length;
                    int j = 0;
                    while (j < sizeChunk) { data.Add(chunk[j++]); }
                }
                resData = data.ToArray();

                state_ = ST_READY;
            }
            finally
            {
                //rfidReader_.EndTransaction(SCardReaderDisposition.Leave);
            }
            /*
           
                        FunctnNotPrm fCompearePIN = delegate()
                        {
                            List<byte> data = new List<byte>();
                            ResponseApdu responseApdu2;
                            var apdu2 = new CommandApdu(IsoCase.Case4Short, rfidReader_.ActiveProtocol)
                            {
                                CLA = 0x00,
                                INS = 0x20,
                                P1 = 0x00,
                                P2 = 0x00,
                                Data = pinData  // 
                            };

                            Transmit(rfidReader_, apdu2, out responseApdu2);
                            msg = GetMsgApdu(responseApdu2, null);
                            Message(SENDER_NAME, msg);
                            data.Add(responseApdu2.SW1);
                            data.Add(responseApdu2.SW2);
                            if ((responseApdu2.SW1 != 0x90) && (responseApdu2.SW2 != 0x00))
                            {
                                var chunk = responseApdu2.GetData();
                                int sizeChunk = chunk.Length;
                                int j = 0;
                                while (j < sizeChunk) { data.Add(chunk[j++]); }
                            }
                            outData = data.ToArray();
                        };
                        ExecuteCommand(readerName, fCompearePIN);
                        resData = outData;
                        msgOut = msg;
            /**/
        }

        //получение подписи указанного блока
        public void GetSignature(string readerName, byte[] blockId, ushort blockSize, ushort sizeRecord, out byte[] blockData)
        {
            blockData = null;
            ConnectCard(readerName);
            if (rfidReader_ == null)
            {
                throw new M_PCSC_Exception("Error inicialize", CodeOfError.InitializeError);
            }

            if (state_ < ST_READY)
            {
                throw new M_PCSC_Exception("Wrong state", CodeOfError.WrongState);
            }
            state_ = ST_READING;
            try
            {
                /*
                var sc = rfidReader_.BeginTransaction();
                if (sc != SCardError.Success)
                {
                    state_ = ST_ERROR;
                    throw new M_PCSC_Exception("Could not begin transaction.", CodeOfError.NotBeginTransaction);
                }
                /**/

                //var res = SelectFile(rfidReader_, blockId);
                //if ((res == 0) && (blockSize > 0))
                {
                    try
                    {
                        ReadSignature(rfidReader_, blockId, blockSize, sizeRecord, out blockData);
                    }
                    catch (Exception ex)
                    {
                        state_ = ST_ERROR;
                        throw new M_PCSC_Exception("Could not read binary.", ex, CodeOfError.NotReadBinaryData);
                    }
                }

                state_ = ST_READY;
            }
            finally
            {
                //rfidReader_.EndTransaction(SCardReaderDisposition.Leave);
            }
/*
            byte[] sigData = null;
            FunctnNotPrm fGetSignature = delegate()
            {
                var res = SelectFile(rfidReader_, blockId);
                if ((res == 0) && (blockSize > 0))
                {
                    try
                    {
                        ReadSignature(rfidReader_, blockId, blockSize, sizeRecord, out sigData);
                    }
                    catch (Exception ex)
                    {
                        state_ = ST_ERROR;
                        throw new M_PCSC_Exception("Could not read binary.", ex, CodeOfError.NotReadBinaryData);
                    }
                }
            };
            ExecuteCommand(readerName, fGetSignature);
            blockData = sigData;
/**/
        }


        //получение данных указанного блока
        public void GetFile(string readerName, byte[] blockId, ushort blockSize, ushort sizeRecord, out byte[] blockData)
        {
            blockData = null;
            ConnectCard(readerName);
            if (rfidReader_ == null)
            {
                throw new M_PCSC_Exception("Error inicialize", CodeOfError.InitializeError);
            }

            if (state_ < ST_READY)
            {
                throw new M_PCSC_Exception("Wrong state", CodeOfError.WrongState);
            }
            state_ = ST_READING;
            try
            {
                /*
                var sc = rfidReader_.BeginTransaction();
                if (sc != SCardError.Success)
                {
                    state_ = ST_ERROR;
                    throw new M_PCSC_Exception("Could not begin transaction.", CodeOfError.NotBeginTransaction);
                }
                /**/

                var res = (blockSize == 0) ? SelectFolder(rfidReader_, blockId) : SelectFile(rfidReader_, blockId);
                if ((res == 0) && (blockSize > 0))
                {
                    try
                    {
                        ReadBinary(rfidReader_, blockId, blockSize, sizeRecord, out blockData);
                    }
                    catch (Exception ex)
                    {
                        state_ = ST_ERROR;
                        throw new M_PCSC_Exception("Could not read binary.", ex, CodeOfError.NotReadBinaryData);
                    }
                }

                state_ = ST_READY;
            }
            finally
            {
                //rfidReader_.EndTransaction(SCardReaderDisposition.Leave);
            }

/*
            byte[] fileData = null;
            FunctnNotPrm fGetFile = delegate()
            {
                var res = ((blockSize == 0) && (blockId.Length > 2)) ? SelectFolder(rfidReader_, blockId) : SelectFile(rfidReader_, blockId);
                if ((res == 0) && (blockSize > 0))
                {
                    try
                    {
                        ReadBinary(rfidReader_, blockId, blockSize, sizeRecord, out fileData);
                    }
                    catch (Exception ex)
                    {
                        state_ = ST_ERROR;
                        throw new M_PCSC_Exception("Could not read binary.", ex, CodeOfError.NotReadBinaryData);
                    }
                }
            };
            ExecuteCommand(readerName, fGetFile);
            blockData = fileData;
/**/
        }


        private string GetMsgApdu(ResponseApdu resApdu, byte[] blockId)
        {
            return (blockId==null)
                    ?GetMsgApdu(resApdu.SW1, resApdu.SW2)
                    :string.Format("blockId - {0}; msg - {1}", BitConverter.ToString(blockId), GetMsgApdu(resApdu.SW1, resApdu.SW2));
        }

        //выбор раздела
        private int SelectFolder(SCardReader rfidReader, byte[] blockId)
        {
            //(blockId.Length > 2) - Direct selection by DF name (data field=DF name) http://www.cardwerk.com/smartcards/smartcard_standard_ISO7816-4_6_basic_interindustry_commands.aspx#table58
            //Выбор EF по текущему каталогу DF: 00 A4 02 0C 02 00 05
            //Выбор по имени (AID):             00 A4 04 0C 06 FF 54 41 43 48 4F
            var apdu = new CommandApdu(IsoCase.Case3Short, rfidReader.ActiveProtocol)
            {
                CLA = 0x00,
                Instruction = InstructionCode.SelectFile,
                P1 = (byte)((blockId.Length > 2) ? 0x04 : 0x02), 
                P2 = 0x0C,      //Ответ не требуется
                Data = blockId
            };

            ResponseApdu responseApdu;
            Transmit(rfidReader, apdu, out responseApdu);

            Message(SENDER_NAME, GetMsgApdu(responseApdu, blockId));

            return ((responseApdu.SW1 == 0x90) && (responseApdu.SW2 == 0x00))
                   ? 0
                   : ((responseApdu.SW1 == 0x6A) && (responseApdu.SW2 == 0x82))
                     ? 1
                     : ((responseApdu.SW1 == 0x00) && (responseApdu.SW2 == 0x00))
                      ? 2
                      : (responseApdu.SW1 == 0x61)
                        ? 3
                        : (responseApdu.SW1 == 0x62)
                           ? 4
                           : (responseApdu.SW1 == 0x63)
                             ? 5
                             : -1;
        }

        //выбор блока
        private int SelectFile(SCardReader rfidReader, byte[] blockId)
        {
            //00 A4 02 0C 02 00 05
            var apdu = new CommandApdu(IsoCase.Case3Short, rfidReader.ActiveProtocol)
            {
                CLA = 0x00,
                Instruction = InstructionCode.SelectFile,
                P1 = 0x02,      //Выбор EF по текущему каталогу DF
                P2 = 0x0C,      //Ответ не требуется
                Data = blockId
            };

            ResponseApdu responseApdu;
            Transmit(rfidReader, apdu, out responseApdu);

            Message(SENDER_NAME, GetMsgApdu(responseApdu, blockId));

            return ((responseApdu.SW1 == 0x90) && (responseApdu.SW2 == 0x00))
                   ? 0
                   : ((responseApdu.SW1 == 0x6A) && (responseApdu.SW2 == 0x82))
                     ? 1
                     : ((responseApdu.SW1 == 0x00) && (responseApdu.SW2 == 0x00))
                      ? 2
                      : (responseApdu.SW1 == 0x61)
                        ? 3
                        : (responseApdu.SW1 == 0x62)
                           ? 4
                           : (responseApdu.SW1 == 0x63)
                             ? 5
                             : -1;
        }

        private void ReadSignature(SCardReader rfidReader, byte[] blockId, ushort blockSize, ushort sizeRecord, out byte[] blockData)
        {
            //PERFORM HASH OF FILE(0x00, 0x2A, 0x9E, 0x9A, 0x80) - для хеширования зоны данных выбранного в данный момент бинарного элементарного файла.
            blockData = null;
            ResponseApdu responseApdu1;
            var apdu = new CommandApdu(IsoCase.Case1, rfidReader.ActiveProtocol)
            {
                CLA = 0x80,
                INS = 0x2A,
                P1 = 0x90,
                P2 = 0x00
            };
            Transmit(rfidReader, apdu, out responseApdu1);
            Message(SENDER_NAME, GetMsgApdu(responseApdu1, blockId));
            if ((responseApdu1.SW1 != 0x90) && (responseApdu1.SW2 != 0x00))
            {
                return;
            }

            //PSO: Compute Digital Signature (расчет цифровой подписи)
            List<byte> data = new List<byte>();
            ResponseApdu responseApdu2;
            var apdu2 = new CommandApdu(IsoCase.Case2Short, rfidReader.ActiveProtocol)
            {
                CLA = 0x00,
                INS = 0x2A,
                P1 = 0x9E,
                P2 = 0x9A,
                Le = 0x40  // the ID tag size
            };

            Transmit(rfidReader, apdu2, out responseApdu2);
            Message(SENDER_NAME, GetMsgApdu(responseApdu2, blockId));
            if ((responseApdu2.SW1 != 0x90) && (responseApdu2.SW2 != 0x00))
            {
                var chunk = responseApdu2.GetData();
                int sizeChunk = chunk.Length;
                int j = 0;
                while (j < sizeChunk) { data.Add(chunk[j++]); }
                if (data.Count > 0)
                {
                    blockData = data.ToArray();
                }
            }

        }

        //считывание данных
        private void ReadBinary(SCardReader rfidReader, byte[] blockId, ushort blockSize, ushort sizeRecord, out byte[] blockData)
        {
             const byte MAX_SIZE_BLOCK = 126;
             blockData = null;
            //если blockSize > 126 - считываем информацию блоками по 126 байтов иначе blockSize
            //Если команда проходит, карта выдает ‘9000’.
            //Если размер данных, подлежащих извлечению, не соответствует размеру EF (Сдвиг + Le > размера EF), 
            //состояние обработки выдается в виде ‘6700’ или ‘6Cxx’, где 'xx' указывает точную длину.

             ushort cnt = (blockSize > MAX_SIZE_BLOCK) ? (ushort)(blockSize / MAX_SIZE_BLOCK) : (ushort)0;
             byte chunckSize = (blockSize > MAX_SIZE_BLOCK) ? MAX_SIZE_BLOCK : (byte)blockSize;  
             if ((blockSize % MAX_SIZE_BLOCK) != 0) cnt++;
             List<byte> data = new List<byte>();
             for (var i = 0; i < cnt; i++)
             {
                 ushort pos = (ushort)(i * chunckSize);
                 byte size = ((pos + chunckSize) > blockSize) ? (byte)(blockSize - pos) : chunckSize;
                 var bSize = BitConverter.GetBytes(size);
                 var bPos = BitConverter.GetBytes(pos);
                 ResponseApdu responseApdu;
                 var apdu = new CommandApdu(IsoCase.Case2Short, rfidReader.ActiveProtocol)
                 {
                     CLA = 0x00,
                     Instruction = InstructionCode.ReadBinary,
                     P1 = bPos[1],
                     P2 = bPos[0],
                     Le = size  // the ID tag size
                 };

                 Transmit(rfidReader, apdu, out responseApdu);
                 Message(SENDER_NAME, GetMsgApdu(responseApdu, blockId));

                 if ((responseApdu.SW1 == 0x90) && (responseApdu.SW2 == 0x00))
                 {
                        var chunk = responseApdu.GetData();
                        int sizeChunk = chunk.Length;
                        int j = 0;
                        while (j < sizeChunk){ data.Add(chunk[j++]); }
                 }                
                 else
                 {
                     break;
                 }
             }
             //если sizeRecord> 0 - пробуем прочитать ещё порции записей до ошибки 
             if (sizeRecord > 0)
             {
                 byte noRec = 0;
                 byte size = (byte)sizeRecord;
                 while (true)
                 {
                     ushort pos = (ushort)(blockSize + noRec * sizeRecord);
                     var bPos = BitConverter.GetBytes(pos);
                     var bSize = BitConverter.GetBytes(size);
                     noRec++;
                     ResponseApdu responseApdu;
                     var apdu = new CommandApdu(IsoCase.Case2Short, rfidReader.ActiveProtocol)
                     {
                         CLA = 0x00,
                         Instruction = InstructionCode.ReadBinary,
                         P1 = bPos[1],
                         P2 = bPos[0],
                         Le = size  // the ID tag size
                     };

                     Transmit(rfidReader, apdu, out responseApdu);
                     Message(SENDER_NAME, GetMsgApdu(responseApdu, blockId));

                     if ((responseApdu.SW1 == 0x90) && (responseApdu.SW2 == 0x00))
                     {
                         var chunk = responseApdu.GetData();
                         int sizeChunk = chunk.Length;
                         int j = 0;
                         while (j < sizeChunk) { data.Add(chunk[j++]); }
                     }
                     else
                     {
                         break;
                     }

                 }
             }
            if (data.Count > 0)
            {
                blockData = data.ToArray();
            }

        }

        //передача команды карточке
        private void Transmit(SCardReader rfidReader, CommandApdu apdu, out ResponseApdu responseApdu)
        {
            responseApdu = null;

                Message(SENDER_NAME, "Retrieving the UID .... ");

                var receivePci = new SCardPCI(); // IO returned protocol control information.
                var sendPci = SCardPCI.GetPci(rfidReader.ActiveProtocol);

                var receiveBuffer = new byte[256];
                var command = apdu.ToArray();
                Message(SENDER_NAME, String.Format("Command: {0}", BitConverter.ToString(command)));

                var sc = rfidReader.Transmit(
                    sendPci,            // Protocol Control Information (T0, T1 or Raw)
                    command,            // command APDU
                    receivePci,         // returning Protocol Control Information
                    ref receiveBuffer); // data buffer

                if (sc != SCardError.Success)
                {
                    Message(SENDER_NAME, "Error: " + SCardHelper.StringifyError(sc));
                }

                responseApdu = new ResponseApdu(receiveBuffer, IsoCase.Case2Short, rfidReader.ActiveProtocol);
                Message(SENDER_NAME, String.Format("SW1: {0:X2}, SW2: {1:X2}\nUid: {2}",
                    responseApdu.SW1,
                    responseApdu.SW2,
                    responseApdu.HasData ? BitConverter.ToString(responseApdu.GetData()) : "No uid received"                    
                ));

        }
    }
}

/*
 * SW1 SW2 Meaning

62 81 Returned data may be corrupted. 
62 82 The end of the file has been reached before the end of reading. 
62 84 Selected file is not valid. 
65 01 Memory failure. There have been problems in writing or reading 
the EEPROM. Other hardware problems may also bring this error. 
68 00 The request function is not supported by the card. 
6A 00 Bytes P1 and/or P2 are incorrect. 
6A 80 The parameters in the data field are incorrect. 
6A 82 File not found. 
6A 83 Record not found. 
6A 84 There is insufficient memory space in record or file. 
6A 87 The P3 value is not consistent with the P1 and P2 values. 
6A 88 Referenced data not found. 
6C XX Incorrect P3 length.
*/