﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace TachoSourceApp
{
    static class ConnectionUSBDev
    {
        public const int WM_DRAWCLIPBOARD = 0x0308;


        public const int WM_DEVICECHANGE = 0x0219;
        /// <summary> Подключение устройства </summary>
        public const int DBT_DEVICEARRIVAL = 0x8000;
        /// <summary> Отключение устройства </summary>
        public const int DBT_DEVICEREMOVECOMPLETE = 0x8004;
        public const uint DBT_DEVTYP_VOLUME = 0x00000002;
        //A device has been added to or removed from the system.
        public const uint DBT_DEVNODES_CHANGED = 0x0007;
    }
    class MsgConnectUSBDev
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct DEV_BROADCAST_HDR
        {
            public uint dbch_size;
            public uint dbch_devicetype;
            public uint dbch_reserved;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DEV_BROADCAST_VOLUME
        {
            public uint dbcv_size;
            public uint dbcv_devicetype;
            public uint dbcv_reserved;
            public uint dbcv_unitmask;
            public ushort dbcv_flags;//public System.UInt16
        }
    }
    
}
