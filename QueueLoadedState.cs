// **********************************************************************
//
// ��������� ��������� ����� �� ������������ ������
//
// 
// 
//
// **********************************************************************

using System;
using System.Threading;
using System.Collections;

using System.IO;

namespace TachoSource
{
    public delegate void FileLoadedState(string fileName, CallbackEntry itemLoaded, ItemLoadFileState info);

    public enum StatusProcess
        {
            // 
            Error = -1,
            //     
            Default = 0,
            //     
            NeedCheck = 1,
            //
            TriesCountIsEnd = 2,
            //>IsOk - break process
            IsOk = 20,
            Finish = 21,
        };
    public class CallbackEntry 
    {
        public static readonly int RETRY_COUNT = 10;
        public static readonly int TIMER_DELAY = 3000; //miliSecond
        public static readonly int TIMER_INTERVAL = TIMER_DELAY*10; //miliSecond
        public static readonly DateTime BEGIN_DT = new DateTime(1970, 1, 1, 0, 0, 0);
        public static string Seconds2HH_MM_SS(int val)
        {
            int waitHour = val/3600;
            int waitMinute = (val - (waitHour*3600))/60;
            int waitSecond = val - (waitHour*3600) - waitMinute*60;
            string hourStr = waitHour < 9 ? String.Format("0{0}", waitHour) : String.Format("{0}", waitHour);
            string minuteStr = waitMinute < 9 ? String.Format("0{0}", waitMinute) : String.Format("{0}", waitMinute);
            string secondStr = waitSecond < 9 ? String.Format("0{0}", waitSecond) : String.Format("{0}", waitSecond);
            return String.Format("{0}:{1}:{2}", hourStr, minuteStr, secondStr);
            
        }
        public FileLoadedState cb;

        //--------------------------
        public string fileName;                 //4 if (cb != null) cb(fileName, this, state);
        public ItemLoadFileState state = null;  //4 if (cb != null) cb(fileName, this, state);

        public StatusProcess status = StatusProcess.Default;
        public int tries = 0;
        public string fileId;
        public int timeStamp = 0;          //TotalSeconds
        protected Disp dispPtr_ = null;


            public void Init(Disp dispPtr, FileLoadedState cbP, string fileId, string fileName)
            {
                dispPtr_ = dispPtr;
                this.cb = cbP;
                this.fileId = fileId;
                this.fileName = fileName;
                this.NextTries();

            }
            public long Work()//(MngrFileStorage mngrMS)
            {
                int code = Monitoring.TOK.value;
                this.NextTries();
                
                //???��������� �������: ����� �� ����� ������ ��� �������
                try
                {
                    if (this.NeedRequest())
                    {
                        state = dispPtr_.GetFileInfo(fileId);
                    }
                    code = state == null ? 0 : state.code;
                }
                catch (Exception objEx) 
                {
                    code = Monitoring.EFail.value;
                }
                UpdateStatus(code, state);
                //if (cb != null) cb(fileName, this, state);
                //����� BlockingCollection
                return code;
            }
            public string GetId()
            {
                return this.fileName;
            }
            public bool IsSucceded()
            {
                return status > StatusProcess.IsOk;
            }
            public bool NeedRequest()
            { 
                bool res = fileName.Length > 0;
                if (!res) return res;
                //has error
                res = !(this.status < StatusProcess.Default);
                if (!res) return res;

                res = (this.tries <= RETRY_COUNT);
                if (!res) return res;

                //is finish
                res = !(status >= StatusProcess.IsOk);
                return res;
            }

            public int Wait2TimeStamp()
            {
                TimeSpan t = (DateTime.Now - CallbackEntry.BEGIN_DT);
                int currTime = (int)t.TotalSeconds;
                int waitMSec = this.timeStamp - currTime;
                return waitMSec == 0 ? -1 : waitMSec;
            }
            private void NextTries()
            {
                if (this.timeStamp == 0)
                {
                    TimeSpan t = (DateTime.Now - CallbackEntry.BEGIN_DT);

                    this.timeStamp = Convert.ToInt32(t.TotalSeconds);
                }
                this.timeStamp += ((int)Math.Pow(2, this.tries) * (TIMER_INTERVAL / 1000));
                this.tries++;
            }

            private void UpdateStatus(int code, ItemLoadFileState state)
            {
                //���� ��� ������ � ���������� - ����� ����� ��������
                if (code < 0)
                {
                    this.status = StatusProcess.Error;
                }
                else
                {
                    int curStatus = (state == null) ? Monitoring.SttsDef.value : state.status;
                    bool isError1 = ServiceSendingFile.FlagInStateStorageItem(Monitoring.SttsWithError.value, curStatus);
                    bool isError2 = ServiceSendingFile.FlagInStateStorageItem(Monitoring.SttsParseWithError.value, curStatus);
                    bool isError3 = ServiceSendingFile.FlagInStateStorageItem(Monitoring.SttsSaveDBWithError.value, curStatus);
                    if (isError1 || isError2 || isError3)
                    {
                        this.status = StatusProcess.Error;
                    }
                    else
                    {
                        bool isOk = ServiceSendingFile.FlagInStateStorageItem(Monitoring.SttsAppended.value, curStatus);
                        this.status = isOk ? StatusProcess.Finish : StatusProcess.NeedCheck;

                    }
                }
                if (this.tries > RETRY_COUNT)
                {
                    this.status = StatusProcess.TriesCountIsEnd;
                }
            }
    };
    public class QueueLoadedState
    {
        //����������� �� ������ ������� ��������� ��������
        public static readonly int QUEUE_LENGHT_LIMIT = 5;
        public AutoResetEvent waitHandler = new AutoResetEvent(false); //����� � ������������ ���������
        //true - ������ � ���������� ���������
        //false - ������ � ������������ ���������, � ��� ������ ����������� ������� waitHandler.WaitOne() �� �������� �������
        //����� � ��������� ��������, ���� ������ waitHandler �� ����� ��������� � ���������� ���������.
        //Reset() ���������� ������ ������������ � ������������ ���������.


        class CallbackEntryComparer : IComparer
        {
            public int Compare(object o1, object o2)
            {
                CallbackEntry p1 = o1 as CallbackEntry;
                CallbackEntry p2 = o2 as CallbackEntry;
                if ((p1 != null) && (p2 != null))
                {
                    return CompareItem(p1, p2);
                }
                else 
                {
                    return 0;
                }
            }
            public int CompareItem(CallbackEntry p1, CallbackEntry p2)
            {
                //<0 ������, ������� ������ ������ ���������� ����� ��������, ������� ���������� � �������� ���������
                // 0 ������, ��� ������� �����
                int pStts1 = p1.NeedRequest()
                            ? (int)p1.status
                            : (int)StatusProcess.Finish;
                int pStts2 = p1.NeedRequest()
                            ? (int)p2.status
                            : (int)StatusProcess.Finish;
                return pStts1 == pStts2
                    ? p1.timeStamp == p2.timeStamp 
                        ? 0
                        : p1.timeStamp < p2.timeStamp 
                            ? -1 
                            : 1
                    : pStts1 < pStts2 ? -1 : 1;

            }

        }

        /**/

        public QueueLoadedState()
        {
        }

        public void Join()
        {
            thread_.Join();
        }

        public void Start()
        {
            thread_ = new Thread(new ThreadStart(Run));
            thread_.Start();
        }

        public void Run()
        {
            while (!_done)
            {
                    try
                    {
                        int waitMSec = -1; //0 - ��������� ���, ���;  > 0 ��� �������� �����; < 0 ���������
                        //CallbackEntry itmState = null;
                        String itmId = "";
                        //���� �� �������� ��� ������� ���������
                        Console.Out.WriteLine(String.Format("0 waitMSec: {0};", waitMSec));
                        lock (this)
                        {
                            if (_callbacks.Count == 0)
                            {
                                waitMSec = 0;
                                Console.Out.WriteLine(String.Format("1 waitMSec: {0};", waitMSec));
                            }
                            else
                            {
                                _callbacks.Sort(new CallbackEntryComparer());
                                //
                                // Get next work item.
                                //
                                CallbackEntry entry = (CallbackEntry)_callbacks[0];
                                itmId = entry.GetId();
                                Console.Out.WriteLine(String.Format("1_2 waitMSec: {0};", waitMSec));
                                if (!entry.NeedRequest())
                                {
                                    waitMSec = 0;
                                    Console.Out.WriteLine(String.Format("1_3 waitMSec: {0};", waitMSec));
                                }
                                else 
                                {
                                    waitMSec = entry.Wait2TimeStamp() * 1000;

                                    Console.Out.WriteLine(String.Format("1_4 waitMSec: {0};", waitMSec));
                                    //itmState = entry;
                                }
                                Console.Out.WriteLine(String.Format("2 waitMSec: {0};", waitMSec));

                                if (waitMSec < 0)//������
                                {
                                    long rsCode = entry.Work();//MngrFileStorage.GetInstance(_idDB));
                                    GlobalLog.trace("Work", String.Format("item: {0}; resCode:  {1}, done: {2}", itmId, rsCode, _done));


                                    _countWaitState = this.GetCountWaitState();
                                    //���� �� BlockingCollection
                                    if (entry.cb != null) entry.cb(entry.fileName, entry, entry.state);

                                    Console.Out.WriteLine(String.Format("3 waitMSec: {0};", waitMSec));

                                    if (!entry.NeedRequest())
                                    {
                                        _callbacks.RemoveAt(0);
                                        Console.Out.WriteLine(String.Format("Item was removed from queue: item {0}. Count`s item: {1}", itmId, _callbacks.Count));
                                    }
                                }

                            }
                        }

                        Console.Out.WriteLine(String.Format("3 waitMSec: {0};", waitMSec));

                        if (waitMSec >= 0)//�������� ��������� ��������
                        {
                            Console.Out.WriteLine(String.Format("Wait item: {0}; time interval for request state:  {1}", itmId, ((waitMSec == 0) ? "unknown" : CallbackEntry.Seconds2HH_MM_SS(waitMSec / 1000)) ));
                            if (waitMSec == 0)
                            {
                                this.waitHandler.WaitOne();
                            }
                            else 
                            {
                                this.waitHandler.WaitOne(waitMSec);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        GlobalLog.error(String.Format("Exception in work: {0};", ex.Message));
                    }
            }
        }
        public void AddItem(Disp dispPtr, FileLoadedState cb, string fileId, string fileName)
        {
                if (!_done)
                {
                    lock (this)
                    {
                        // Add the work item.
                        CallbackEntry entry = new CallbackEntry();
                        entry.Init(dispPtr, cb, fileId, fileName);

                        _countWaitState = this.GetCountWaitState();
                        _callbacks.Add(entry);
                        if (_countWaitState == 0)
                        {
                            //Monitor.Pulse(this);
                            this.Pulse();
                        }

                    }
                }
                else
                {
                    //
                    // Destroyed, throw exception.
                    //
                    //cb.ice_exception(new Monitoring.Error());
                }
        }

        public int CountWaitState
        {
            //���-�� ��������� ��������� ��������� ���������
            get 
            {
                return _countWaitState;
            }
        }


        /*
            public void Add(AMD_SvrKeyActivate_changePubKey cb, ClientInfo client, long param, RequestInfo rqst, int delay)
            {
                lock(this)
                {
                    if(!_done)
                    {
                        // Add the work item.
                        CallbackEntry entry = new CallbackEntry();
                        entry.cb = cb;
                        entry.delay = delay;
                        entry.client = client;
                        entry.param = param;
                        entry.rqst = rqst;

                        if(_callbacks.Count == 0)
                        {
                            Monitor.Pulse(this);
                        }
                        _callbacks.Add(entry);
                    }
                    else
                    {
                        //
                        // Destroyed, throw exception.
                        //
                        cb.ice_exception(new RequestCanceledException());
                    }
                }
            }
        /**/

        public void destroy()
        {
            _done = true;
            //Monitor.Pulse(this);
            this.Pulse();
            if (thread_ != null)
            {
                thread_.Abort();
                thread_ = null;
            }

        }
        private void Pulse()
        {
            this.waitHandler.Set();
        }
        private int GetCountWaitState()
        {
            int res = 0;
            foreach (CallbackEntry cbItm in _callbacks)
            {
                if (cbItm.NeedRequest()) res++;
            }
            return res;
        }

        //����� BlockingCollection
        private ArrayList _callbacks = new ArrayList();
        private bool _done = false;
        private Thread thread_;
        private int _countWaitState = 0;
    }
}