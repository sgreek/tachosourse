// **********************************************************************
// Monitoring statistic service interface
// Copyright (c) 2015
// **********************************************************************

#ifndef MONSTAT_ICE
#define MONSTAT_ICE

#include "monitoring.ice"

module Monitoring
{
	// StatOptions parameters
	const short StatParamRecalc =	0x1000;
	const short StatParamErase =	0x2000;
	const short StatParamDelay =	0x4000;

    struct StatOptions
	{
		int count;					// Maximum count elements
		int offset;					// First element offset
		int param;					// Any additional parameters, see StatParamXXX
		double time;				// Requested time or zero
		IntArray items;				// Requested items or empty for full
		ParamArray params;			// Requested properties or empty for full
		ParamArray events;			// Requested events or empty for full
	};

    struct StatResult
	{
		int errcode;				// Error code or 0
		string errmsg;				// Error message
		int param;					// Any additional parameters
		double time;				// Result time or zero
		ItemArray items;			// Result items array
	};

    struct StatInfoBlock
	{
		double time;
		double fixtime;
		int code;
	};
	sequence<StatInfoBlock> StatInfoBlocks;
    struct StatInfoItem
	{
		int code;
		StatInfoBlocks blocks;
	};
	sequence<StatInfoItem> StatInfoItems;
    struct StatInfo
	{
		int errcode;				// Error code or 0
		string errmsg;				// Error message
		int param;					// Any additional parameters
		double time;				// Result time or zero
		StatInfoItems items;		// Result items array
	};

    interface Statistics
	{
        ["ami", "amd", "cpp:const"] idempotent string info(optional(1) string param);
        ["ami", "amd"] idempotent StatResult objects(string obj, optional(1) StatOptions opt);	
        ["ami", "amd"] idempotent StatResult query(string obj, double time, double interval, optional(1) StatOptions opt);	
        ["ami", "amd"] idempotent StatInfo stat(string obj, double time, double interval, optional(1) StatOptions opt);	
	};
};

#endif
