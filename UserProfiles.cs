﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace TachoSourceApp
{

    [DataContract]
    public class ProfileInfo
    {
        public static uint ACTIVE = 1;   //
        public static uint DEFAUL = 0;  //
        [DataMember]
        public string server { get; set; }
        [DataMember]
        public string user { get; set; }
        [DataMember]
        public string pswrd { get; set; }
        [DataMember]
        public uint active { get; set; } //1 - active
        [DataMember]
        public bool toStore { get; set; }

        public ProfileInfo(string aServer, string aUser, string aPswrd)
        {
            server = aServer;
            user = aUser;
            pswrd = aPswrd;
            active = DEFAUL;
            toStore = false;
        }

    }
    public class UserProfiles
    {
        private List<ProfileInfo> List = new List<ProfileInfo>();

        public string toJSON()
        {
            string str = "";
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(IEnumerable<ProfileInfo>));

            if (List.Count > 0)
            {
                //using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(str)))
                using (MemoryStream ms = new MemoryStream())
                {
                    try
                    {
                        List<ProfileInfo> profiles = new List<ProfileInfo>();
                        foreach(var itm in List)
                        {
                            if (!itm.toStore) continue;
                            profiles.Add(itm);
                        }
                        if (profiles.Count > 0)
                        {
                            jsonFormatter.WriteObject(ms, profiles);

                            str = Encoding.Default.GetString((ms.ToArray()));

                            /*
                            using (StreamReader sr = new StreamReader(ms))
                            {
                                str = sr.ReadToEnd();
                            }
                            /**/
                        }
                    }
                    catch (Exception ex)
                    {
                        GlobalLog.error(String.Format("UserProfile toJSON: {0}", ex.Message));
                        str = ""; 
                    }

                }
            }
            return str;
        }

        public uint fromJSON(string jsonStr)
        {
            uint res = 0;
            if (jsonStr == null || jsonStr.Length == 0)
            {
                GlobalLog.print("UserProfile fromJSON: jsonStr is empty!");
                return res;
            }
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(ProfileInfo[]));
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonStr)))
            {
                ProfileInfo[] profiles = null;
                try
                {
                    profiles = (ProfileInfo[])jsonFormatter.ReadObject(ms);
                }
                catch(Exception ex)
                {
                    GlobalLog.error(String.Format("UserProfile fromJSON: {0}", ex.Message));
                    profiles = null;
                }
                if (profiles != null)
                {
                    List.Clear();
                    List.AddRange(profiles);
                }
            }

            return res;
        }

        public ProfileInfo AddProfile(string user, string pswrd, string server, bool toStore)
        {
            ProfileInfo fndProf = this.GetProfile(user);
            if (fndProf == null)
            {
                fndProf = new ProfileInfo(server, user, pswrd);
                List.Add(fndProf);
            }
            fndProf.pswrd = pswrd;
            fndProf.server = server;
            fndProf.toStore = toStore;
            return fndProf;
        }

        //mad.ExistProfile = function(dataProf){

        public ProfileInfo GetProfile(string user)
        {
            ProfileInfo fndProf = null;
            int indxFnd = List.FindIndex(s => s.user == user);
            if (indxFnd > -1)
            {
                fndProf = List[indxFnd];
            }
            return fndProf;
        }

        public ProfileInfo SetCurrent(string user)
        {
            ProfileInfo fndProf = this.GetProfile(user);
            if (fndProf != null)
            {
                ResetCurrent();
                fndProf.active = ProfileInfo.ACTIVE;
            }
            return null;
        }

        public ProfileInfo GetCurrent()
        {
            ProfileInfo fndProf = null;
            int indxFnd = List.FindIndex(s => s.active == ProfileInfo.ACTIVE);
            if (indxFnd > -1)
            {
                fndProf = List[indxFnd];
            }
            return fndProf;
        }

        public ProfileInfo[] GetList()
        {
            return List.ToArray();
        }

        //private
        private void ResetCurrent()
        {
            foreach (var itm in List)
            {
                itm.active = ProfileInfo.DEFAUL;
            }
        }


    }
}
