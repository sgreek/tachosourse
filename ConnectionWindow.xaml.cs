﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//timer
using System.Threading;
using System.Threading.Tasks;

namespace TachoSourceApp
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    public partial class ConnectionWindow : Window
    {
        public ConnectionWindow()
        {
            InitializeComponent();
        }
        delegate void FunctnNotPrm();
        public event Applctn.FResAuthState OnResState;

        static public void WindowClose(Window elmntWnd)
        {
            //прогресс в элементе у считывателя
            FunctnNotPrm fClose = delegate()
            {
                elmntWnd.Close();
            };

            elmntWnd.Dispatcher.Invoke(fClose);
        }
        public void SetState(string msg, int code)
        {
            if (OnResState != null) OnResState(msg, code);
            MainWindow.SetTextElement(this.tStates, msg);
            
            if (code == 0)
            {
                AddCurrentProfile();
                Applctn applctn = Applctn.Instance;
                applctn.OnAuthState -= DoAuthState;

                //закрываем через одну секунду
                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(1000);
                })
                    .ContinueWith(x =>
                    {
                        WindowClose(this);
                    }, TaskScheduler.FromCurrentSynchronizationContext());
            }
            else { 
            }
        }
        private void UpdateComboBox()
        {
            Applctn applctn = Applctn.Instance;
            //списки "серверов" и "пользователей"
            List<string> listServers = new List<string>();
            List<string> listUsers = new List<string>();
            ProfileInfo[] profiles = applctn.Profiles.GetList();
            foreach (ProfileInfo itm in profiles)
            {
                string itmUser = listServers.Find(s => s == itm.user);
                string itmServer = listServers.Find(s => s == itm.server);
                if (itmUser == null) listUsers.Add(itm.user);
                if (itmServer == null) listServers.Add(itm.server);
            }
            ComboBoxInitList(this.cbServers, listServers);
            ComboBoxInitList(this.cbUsers, listUsers);
        }

        private void DoAuthState(string msg, int code)
        {
            SetState(msg, code);
        }

        private void AddCurrentProfile()
        {
            //добавляем в профиль пользователя прошедшего авторизацию
            bool toStore = false;
            FunctnNotPrm getValue = delegate()
            {
                toStore = (bool)chbStore.IsChecked;
            };
            chbStore.Dispatcher.Invoke(getValue);

            Applctn applctn = Applctn.Instance;
            ProfileInfo itmProf = applctn.Profiles.AddProfile(applctn.AuthInfoIn.User, applctn.AuthInfoIn.Password, applctn.AuthInfoIn.Server, toStore);
            if (itmProf != null)
            {
                applctn.Profiles.SetCurrent(itmProf.user);
            }
            UpdateComboBox();
        }

        private void bAction_Click(object sender, RoutedEventArgs e)
        {
            if (this.bCancel == sender)
            {
                this.Close();
                return;
            }
            if (this.bOk == sender)
            {
                if (this.cbUsers.Text.Length <= 0)
                {
                    this.tStates.Text = "Укажите пользователя!";
                    return;
                }
                Applctn applctn = Applctn.Instance;
                applctn.OnAuthState += DoAuthState;
                applctn.CreateSession(this.cbUsers.Text, this.ePassword.Password, this.cbServers.Text);
            }
        }


        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                bAction_Click(this.bCancel, null);
                return;
            }

        }

        private void ComboBoxInitList(ComboBox cntrl, List<string> list)
        {
            cntrl.Items.Clear();
            foreach(var itm in list)
            {
                cntrl.Items.Add(itm);
            }
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            UpdateComboBox();
            Applctn applctn = Applctn.Instance;
            if ((applctn.AuthInfoIn.User != null) && (applctn.AuthInfoIn.User.Length > 0))
            {
                MainWindow.SetTextElement(this.cbServers, applctn.AuthInfoIn.Server);
                MainWindow.SetTextElement(this.cbUsers, applctn.AuthInfoIn.User);
                if ((applctn.AuthInfoIn.Password != null) && (applctn.AuthInfoIn.Password.Length > 0))
                {
                    MainWindow.SetTextElement(this.ePassword, applctn.AuthInfoIn.Password);
                }
            }

        }

        private void cbUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender == this.cbUsers)
            {
                if (e.AddedItems.Count <= 0) return;
                Applctn applctn = Applctn.Instance;
                string usrTxt = (string)e.AddedItems[0];
                ProfileInfo itmProf = applctn.Profiles.GetProfile(usrTxt);
                if (itmProf != null)
                {
                    MainWindow.SetTextElement(this.cbServers, itmProf.server);
                    MainWindow.SetTextElement(this.ePassword, itmProf.pswrd);
                }
            }
        }

        private void ePassword_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                bAction_Click(this.bOk, null);
                return;
            }

        }

    }
}
