﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TachoSource
{
    public enum AuthOperation
    {
        // инициализация
        Init = 1,
        // подключение
        Info = 2,
        // создание сессии
        SessionCreate = 3,
        // инициализация
        NotFind = 10,
    }

    //подключение
    public class ConnectionInfo
    {
        public string Server { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
    public class SessionInfo
    {
        public int Code { get { return this.code; } }
        public string Token { get { return this.token; } }

        public void SetInfo(int code, string token)
        { 
            this.code = code;
            this.token = token;
        }
        public void Reset()
        {
            this.code = 0;
            this.token = "";
            this._tries++;
        }
                                
        public bool IsLimitTries()
        {
            return this.Tries > 3;
        }

        public int Tries { get {return _tries;} }
        private int _tries = 0;
        private int code = 0;
        private string token = "";
    }
    class ServiceAuthSTR
    {
        public enum CodeOfError
        {
            // ошибка
            Error = -1,
            // ошибка конекта к 
            ConnectError = -2,
            //     
            InitializeError = -3,
            //     
            //     No error. (SCARD_S_SUCCESS)
            Success = 0,
        }
        public class AuthSTR_Exception : Exception
        {
            public CodeOfError codeOfError { get; set; }
            public AuthSTR_Exception() { }

            public AuthSTR_Exception(string message, CodeOfError code)
                : base(message) { codeOfError = code; }
            public AuthSTR_Exception(string message, System.Exception inner, CodeOfError code)
                : base(message, inner) { codeOfError = code; }

            public AuthSTR_Exception(string message)
                : base(message) { codeOfError = CodeOfError.Success; }
            public AuthSTR_Exception(string message, System.Exception inner)
                : base(message, inner) { codeOfError = CodeOfError.Success; }
            protected AuthSTR_Exception(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
                : base(info, context) { codeOfError = CodeOfError.Success; }
        }

        //события
        public delegate void Message(string category, string msg);
        public delegate void Completed(object result, Exception error, string msg, int code);

        public event Message OnMessage;
        public event Completed OnCompleted;


        private Ice.Communicator communicatorPtr;
        private Monitoring.CorePrx _authPtr = null;
        public void Init(Ice.Communicator communicator)
        {
            OnMessage("AuthSTR", "Init....");
            communicatorPtr = communicator;
        }

        public void Done()
        {
            OnMessage("AuthSTR", "Done....");
            _authPtr = null;
        }

        public bool InitService()
        {
            if (_authPtr != null) return true;
            bool res = true;
            string namePrx = communicatorPtr.getProperties().getProperty("Auth.Proxy");
            try
            {
                //_authPtr = Monitoring.CorePrxHelper.uncheckedCast(communicatorPtr.stringToProxy(namePrx));
                _authPtr = Monitoring.CorePrxHelper.checkedCast(communicatorPtr.stringToProxy(namePrx));
            }
            catch (Exception ex)
            {
                res = false;
                OnMessage("AuthSTR", String.Format("Error init AuthSTR: {0} ", ex));
            }
            return res;
        }
        public void Info(out string res)
        {
            res = "";
            bool rInit = InitService();
            if (!rInit) return;

            Ice.Optional<string> data = new Ice.Optional<string>();
            res = _authPtr.info(data);
        }

        public void SessionCreate(ConnectionInfo infoIn, out Monitoring.SessionInfo infoOut)
        {

            infoOut = null;
            bool rInit = InitService();
            if (!rInit)
            {
                //if (OnCompleted != null) OnCompleted(null, new AuthSTR_Exception("SessionCreate: service is`t  initialize", CodeOfError.InitializeError), "", (int)AuthOperation.Info);
                return;
            }

            Ice.Optional<string> data = new Ice.Optional<string>();

            string keyAccept = "";
            if ((infoIn.Server != null) && (infoIn.Server.Length > 0))
            {
                keyAccept = infoIn.Server + ".";
            }
            keyAccept += infoIn.User;

            if ((infoIn.Password != null) && (infoIn.Password.Length > 0))
            { 
                keyAccept += ":" + infoIn.Password;
            }
            infoOut = _authPtr.session(Monitoring.SessionCreate.value, keyAccept, data);
            if (OnMessage != null) OnMessage("SessionCreate", String.Format("code - {0}; token - {1}; name - {2}", infoOut.code, infoOut.token, infoOut.name));

        }

    }
}
