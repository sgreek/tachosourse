﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.ComponentModel;


using System.Collections.ObjectModel;
using System.Collections.Specialized;

//Диспетчер работающий с посредниками
namespace TachoSource
{

    public class Disp
    {
        //события
        //изменение состояния тип: код состояния и сообщение
        public delegate void CardChangeState(object sender, MediatorPCSC.M_PCSC_StateInfo info);
        public delegate void CardReadProgress(int progressPercentage, object userState);
        public delegate void CardCompleted(object result, Exception error, bool cancelled);

        public event CardChangeState OnCardChangeState;
        public event CardReadProgress OnCardReadProgress;
        public event CardCompleted OnCardCompleted;

        public delegate void FSendMessage(string category, string msg);
        public delegate void FSendProgress(string file, int progressPercentage, object userState);
        public delegate void FSendCompleted(string file, ServiceSendingFile.ResultInfo result, Exception error, bool cancelled);

        public event FSendProgress OnSendProgress;
        public event FSendCompleted OnSendCompleted;
        public event FSendMessage OnMessage;

        //
        public delegate void FCheckAuthParams(string idAT, int code, string msg);

        //посредник работающий с картами
        private MediatorPCSC m_pcsc = new MediatorPCSC();
        private PCSCWorker pcscWorker = new PCSCWorker();

        //посредник отправляющий файл
        private MediatorSendFile m_ssf = new MediatorSendFile();
        
        public string[] readerNames;
        public string[] ReaderNames {
            get{
                if (readerNames == null) UpdateReaders();
                return readerNames;
            }
        }

        //.....
        //папка для файлов которые нужно отправить
        private string storageSendingPath_ = "";
        public string StorageSendingPath {
            get
            {
                return storageSendingPath_;
            }
            set
            {
                storageSendingPath_ = value;
                pcscWorker.StoragePath = storageSendingPath_;
            }
        }
        //автоматическая отправка последнего считанного файла
        public bool AutoSendReadingFromCard = false;

        private static Ice.InitializationData initData;
        public static Ice.Communicator communicator;
        public static void InitCommunicator(string fileConfIce, string userPathApp, string nameApp)
        {
            #if DEBUG
                System.Windows.MessageBox.Show("InitCommunicator");
            #endif

            initData = new Ice.InitializationData();
            initData.properties = Ice.Util.createProperties();
            if (fileConfIce.Length > 0)
            {
                #if DEBUG
                   System.Windows.MessageBox.Show(string.Format("InitCommunicator preLoadConfIce: {0}",fileConfIce));
                #endif
                initData.properties.load(fileConfIce);
            }

            //параметры логирования
            string logFile = initData.properties.getProperty("Ice.LogFile");
            if (logFile.Length == 0)
            {
                logFile = string.Format("{0}{1}.log", userPathApp, nameApp);
            }

            #if DEBUG
                System.Windows.MessageBox.Show(String.Format("LogFile: {0}",logFile));
            #endif

            initData.properties.setProperty("Ice.LogFile", logFile);        
            //----------------------------
            communicator = Ice.Util.initialize(initData);
            #if DEBUG
                System.Windows.MessageBox.Show("InitCommunicator is Ok");
            #endif
        }
        public static void DisposeCommunicator()
        {
            if (communicator != null)
            {
                communicator.Dispose();
                communicator = null;
            }
            if (initData != null)
            {
                initData = null;
            }
            
        }


        public void Init(bool autoStateReader)
        {
            string strPath = (initData != null) ?initData.properties.getProperty("FileStorage.PathForDDD") :"";
            if(strPath.Length>0){
                StorageSendingPath = strPath;
            }
            m_pcsc.AutoState = autoStateReader;
            InitPCSC();
            InitSendingFile(communicator);
            AuthInit();
        }

        public void Done()
        {
            m_pcsc.Done();
            if (m_pcsc != null)
            {
                m_ssf.Done();
                m_ssf = null;
            }
            if (servcAuthSTR != null)
            {
                servcAuthSTR.Done();
                servcAuthSTR = null;
            }
        }
        //работа с картридерами
        public void PCSCStart()
        {
            if (m_pcsc.State == MediatorPCSC.ST_ERROR)
            {
                m_pcsc.Done();
            }
            else if (m_pcsc.State < MediatorPCSC.ST_DEFAULT)
            {
                m_pcsc.Stop();
            }

            if (m_pcsc.State >= MediatorPCSC.ST_DEFAULT)
            {
                m_pcsc.Start();
            }
        }
        public void PCSCStop()
        {
            m_pcsc.Stop();
        }

        public void UpdateReaders()
        {
            try
            {
                readerNames = m_pcsc.GetReaders();
            }
            catch (Exception ex)
            {
                readerNames = null;
                GlobalLog.error(String.Format("UpdateReaders:{0}", ex));
            }
            if (readerNames != null)
            {
                GlobalLog.print("Available readers: ");
                for (var i = 0; i < readerNames.Length; i++)
                {
                    GlobalLog.print("[" + i + "] " + readerNames[i]);
                }
            }

        }
        public int CheckPin(string rName, string pinStr, out string resStr)
        {
            resStr = null;
            if ((rName == null) || (rName.Length == 0))
            {
                rName = ((readerNames != null) && (readerNames.Length > 0)) ? readerNames[0] : "";
                if (rName.Length == 0)
                {
                    GlobalLog.warning("Name of card reader is empty");
                    return -1;
                }
            }
            if (pcscWorker.IsBusy)
            {
                if (rName.Length == 0) GlobalLog.warning("Card reader is busy");
                return -1;
            }
            int res = -1;
            int counterPin = -1;
            try
            {
                //преобразуем строку в набор байтов
                List<byte> data = new List<byte>();
                ASCIIEncoding ascii = new ASCIIEncoding();
                byte[] arrPin = ascii.GetBytes(pinStr);
                m_pcsc.CheckPin(rName, arrPin);
            }
            catch (Exception ex)
            {
                readerNames = null;
                GlobalLog.error(String.Format("UpdateReaders:{0}", ex));
            }
            res = m_pcsc.DataPIN.GetResult(rName, out counterPin, out resStr);
            return res;
        }
        public void ResetPin()
        {
            GlobalLog.print("Reset PIN");
            m_pcsc.DataPIN.Reset();
        }

        public void SetAutoStateReader(bool value)
        {
            GlobalLog.print(String.Format("Change reader autostate: {0}", value) );
            UpdateReaders();
            m_pcsc.AutoState = value;
        }

        public int DataSmartCardToDDD(string rName, byte typeCard)
        {
            if ((rName==null) || (rName.Length == 0))
            {
                rName = ((readerNames != null) && (readerNames.Length > 0)) ? readerNames[0] : "";
                if (rName.Length == 0)
                {
                    GlobalLog.warning("Name of card reader is empty");
                    return -1;
                }
            }
            if (pcscWorker.IsBusy)
            {
                if (rName.Length == 0) GlobalLog.warning("Card reader is busy");
                return -1;
            }
            pcscWorker.ReaderName = rName;
            pcscWorker.TypeCard = typeCard;
            pcscWorker.RunWorkerAsync(null);
            return 0;
        }

        //отправка файлов
        public ObservableCollection<MediatorSendFile.File4Sending> SendingFile
        {
            get
            {
                return m_ssf == null ? null : m_ssf.SendingFileList;
            }
        }

        public void AddSendingFile(string fileName)
        {
            if (m_ssf == null) return;
            ResultAuthState resAuth = GetResultAuthInfo();
            if ((resAuth.error == null) && (resAuth.code >= 0))
            {
                try
                {
                    m_ssf.Add(resAuth.idAT, fileName);
                }
                catch (Exception ex)
                {
                    GlobalLog.error(String.Format("Add sending file:{0}", ex.Message));
                }
            }
            else 
            {
                if (OnSendCompleted != null) OnSendCompleted(fileName, null, resAuth.error, false);
            }
        }

        class ResultAuthState
        {
            public string idAT;
            public Exception error = null;
            public int code = 0;
            public string msg = "";
            public ResultAuthState() { }
            public ResultAuthState(string idAT, Exception error, int code, string msg)
            {
                this.idAT = idAT;
                this.error = error;
                this.code = code;
                this.msg = msg;
            }

        }
        private ResultAuthState GetResultAuthInfo()
        {
            string idAT = "";
            bool needAuth = this.IsExistAuthServise();
            if (needAuth)
            {
                if (this.IsAuthSessionCreate())
                {
                    idAT = authInfoRes.Token;
                }
                else
                {
                    if ((authInfoIn.User == null) || (authInfoIn.User.Length == 0))
                    {
                        if (OnMessage!= null) OnMessage("Auth", "Имя пользователя для подключения не задано");
                    }
                    else
                    {
                        this.SessionCreateLast();
                        idAT = authInfoRes.Token;
                    }
                }
            }
            if (needAuth && (idAT.Length == 0))
            {
                var error = new ServiceAuthSTR.AuthSTR_Exception("Авторизация не выполнена!", ServiceAuthSTR.CodeOfError.InitializeError);
                //if (OnSendCompleted != null) OnSendCompleted(fileName, null, error, false);
                return new ResultAuthState(idAT, error, -1, "Авторизация невыполнена!"); 
            }
            return new ResultAuthState(idAT, null, 0, "Авторизация выполнена!"); 
        }

        public ItemLoadFileState GetFileInfo(string file)
        {
            if (m_ssf == null) return null;

            ItemLoadFileState res = null;
            ResultAuthState resAuth = this.GetResultAuthInfo();
            if ((resAuth.error == null) && (resAuth.code >= 0))
            {
                try
                {
                    res = m_ssf.GetFileInfo(resAuth.idAT, file);
                    
                }
                catch (Exception ex)
                {
                    GlobalLog.error(String.Format("GetFileInfo:{0}", ex.Message));
                }
            }
            else
            {
                res = new ItemLoadFileState();
                res.code = resAuth.code;
                //if (OnSendCompleted != null) OnSendCompleted(fileName, null, res.error, false);
            }

            return res;
        }

        //PRIVATE members ------------------------------------------------------------------------------------
        private void InitSendingFile(Ice.Communicator communicator)
        {
            if (m_ssf == null) return;
            m_ssf.Init(communicator);
            m_ssf.OnMessage += (string category, string msg) =>
                {
                    if (OnMessage != null) OnMessage(category, msg);
                };
            m_ssf.OnSendProgress += (string file, int progressPercentage, object userState) =>
                {
                    if(OnSendProgress != null) OnSendProgress(file, progressPercentage,  userState);
                };
            m_ssf.OnSendCompleted += (string file, ServiceSendingFile.ResultInfo result, Exception error, bool cancelled) =>
                {
                    if(OnSendCompleted != null) OnSendCompleted(file, result,  error, cancelled);
                };
        }
        private void InitPCSC()
        {
            //работаем с карточкой в отдельном потоке
            pcscWorker.SetPCSC(ref m_pcsc);
            pcscWorker.RunWorkerCompleted += (object sender, RunWorkerCompletedEventArgs e) =>
            {
                if (OnCardCompleted != null) OnCardCompleted(e.Result, (e.Error != null) ? e.Error : null, e.Cancelled);
                if (e.Cancelled)
                    GlobalLog.trace("PCSCWorker", "Работа была прервана пользователем!");
                else if (e.Error != null)
                    GlobalLog.error("PCSCWorker exception: " + e.Error);
                else
                {
                    string fileDDD = (string)e.Result;
                    if (this.AutoSendReadingFromCard)
                    {
                        AddSendingFile(fileDDD);
                    }
                    GlobalLog.trace("PCSCWorker", "Работа закончена успешно. Результат - " + e.Result);
                }

            };
            pcscWorker.ProgressChanged += (object sender, ProgressChangedEventArgs e) =>
            {
                if (OnCardReadProgress != null) OnCardReadProgress(e.ProgressPercentage, e.UserState);
                GlobalLog.trace("PCSCWorker", "Обработано " + e.ProgressPercentage + "%");
            };


            m_pcsc.Message += (category, msg) =>
            {
                GlobalLog.trace(category, msg);
            };
            m_pcsc.ChangeState += (sender, info) =>
            {
                if (OnCardChangeState != null) OnCardChangeState(sender, info);
                GlobalLog.print(String.Format("state:{0} , message:{1}", info.code, info.message));
            };
            m_pcsc.Initialize();
        }

        //------------------------------------------------------------------

        //события 
        public ConnectionInfo authInfoIn = new ConnectionInfo();
        public SessionInfo authInfoRes = new SessionInfo();

        private ServiceAuthSTR servcAuthSTR = new ServiceAuthSTR();
        private int _stateAuth = 0;


        public delegate void AuthState(int opertn, object result);
        public event AuthState OnAuthState;

        private void AuthInit()
        {
            if (servcAuthSTR == null) return;

            servcAuthSTR.OnMessage += (string category, string msg) =>
            {
                if (OnMessage != null) OnMessage(category, msg);
            };
            servcAuthSTR.OnCompleted += (object result, Exception error, string msg, int code) =>
            {
                //if (OnAuthState != null) OnSendCompleted(file, result, error, cancelled);
            };
            servcAuthSTR.Init(communicator);
        }
        private bool GetInitStateAuthService()
        {
            if (servcAuthSTR == null) return false;
            bool isInit = servcAuthSTR.InitService();
            //результат
            if (isInit)
            {
                _stateAuth = (int)AuthOperation.Init;
            }
            else
            {
                _stateAuth = (-1)*(int)AuthOperation.Init;
                if (OnMessage != null) OnMessage("Auth", "Not init auth service");
                GlobalLog.print("Not init auth service");
            }
            if (OnAuthState != null) OnAuthState((int)AuthOperation.Init, isInit);
            return isInit;
        }
        public bool IsExistAuthServise()
        {
            if (_stateAuth == (int)AuthOperation.NotFind)
            {
                return false;
            }
            else if(_stateAuth > 1){
                return true;
            }
            //запускаем проверку
            this.AuthInfo();
            return (_stateAuth != (int)AuthOperation.NotFind);
        }
        public bool IsAuthSessionCreate()
        {
            return _stateAuth == (int)AuthOperation.SessionCreate;
        }
        public void ResetSession()
        {
            if (authInfoRes.Tries == 0)
            {
                this.AuthDone();
            }
            authInfoRes.Reset();
            _stateAuth = 0;
        }
        private void AuthDone()
        {
            if (servcAuthSTR == null) return;
            servcAuthSTR.Done();
        }

        public void AuthInfo()
        {
            if (servcAuthSTR == null) return;
            if (!GetInitStateAuthService())
            {
                _stateAuth = (int)AuthOperation.NotFind; 
                return;
            }
            string resStr;
            try
            {
                servcAuthSTR.Info(out resStr);
            }
            catch(Exception ex)
            {
                resStr = "";
                GlobalLog.error(String.Format("Error info AuthSTR: {0} ", ex));
            }
            //результат
            if (resStr.Length > 0)
            {
                _stateAuth = (int)AuthOperation.Info;
            }
            else {
                _stateAuth = (int)AuthOperation.NotFind;
            }
            if (OnAuthState != null) OnAuthState((int)AuthOperation.Info, resStr);
        }
        public void SessionCreateLast()
        {
            SessionCreate(authInfoIn.User, authInfoIn.Password, authInfoIn.Server);
        }
        public void SessionCreate(string user, string passwrd, string server)
        {
            if (!GetInitStateAuthService())
            {
                _stateAuth = (int)AuthOperation.NotFind;
                return;
            }
            Monitoring.SessionInfo infoOut = null;
            authInfoRes.Reset();

            authInfoIn.User = user;
            authInfoIn.Password = passwrd;
            authInfoIn.Server = server;
            try
            {
                servcAuthSTR.SessionCreate(authInfoIn, out infoOut);
            }
            catch (Exception ex)
            {
                infoOut = null;
                GlobalLog.error(String.Format("Error info AuthSTR: {0} ", ex));
            }
            if (infoOut != null)
            {
                authInfoRes.SetInfo(infoOut.code, infoOut.token);
            }
            //результат
            if ((authInfoRes.Token != null) && (authInfoRes.Token.Length > 0))
            {
                _stateAuth = (int)AuthOperation.SessionCreate;
            }
            else
            {
                _stateAuth = (-1) * (int)AuthOperation.SessionCreate;
            }

            if (OnAuthState != null) OnAuthState((int)AuthOperation.SessionCreate, authInfoRes);
        }
    }
}
