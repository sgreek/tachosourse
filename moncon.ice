// **********************************************************************
// Monitoring connection interface
// Copyright (c) 2014
// **********************************************************************

#ifndef MONCON_ICE
#define MONCON_ICE

#include "monitoring.ice"
#include "monstrg.ice"

module Monitoring
{
    interface Engine
	{
        ["ami", "amd"] idempotent Storage login(string constr) throws Error;
	};
};

#endif
