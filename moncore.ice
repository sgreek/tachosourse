// **********************************************************************
// Monitoring core service interface
// Copyright (c) 2015
// **********************************************************************

#ifndef MONCORE_ICE
#define MONCORE_ICE

#include "monitoring.ice"
#include "monstat.ice"
#include "monstrg.ice"

module Monitoring
{
	const short SessionQuery = 0;
	const short SessionCreate =	1;
	const short SessionDestroy =2;
	const short SessionUpdate =	3;
	const short SessionClear =	4;
	const short SessionLogin =	5;

    struct SessionInfo
	{
		int code;
		string token;
		string name;
		string data;
		IntArray roles;
	};

    interface Core
	{
		["ami", "amd", "cpp:const"] idempotent string info(optional(1) string param);
		["ami", "amd"] idempotent SessionInfo session(short cmd, string token, optional(1) string data);
		["ami", "amd"] idempotent StorageResult objects(string token, optional(1) StorageQuery qry);
		["ami", "amd"] idempotent StatResult telemetry(string token, int code, double time, double interval, optional(1) StatOptions opt);
	};
};

#endif
