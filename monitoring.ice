// **********************************************************************
// Copyright (c) 2013
// **********************************************************************

#ifndef MONITORING_ICE
#define MONITORING_ICE

module Monitoring
{
	/*************************************************************************************/
	/* Error codes */

	const int EFail = -1;
	const int EConnect = -2;
	const int ENotFound = -3;
	const int EInvalidArg = -4;
	const int ENotImpl = -5;
	const int EDatabase = -6;
	const int EIncomplete = -7;
	const int EInvalidTime = -8;
	const int EStorage = -9;
	const int ESession = -10;
	const int EAuthorize = -11;
	const int EInitialize = -12;
	const int EAccessDenied = -13;
	const int EWrongFileFormat = -21;
	const int EWrongFileAccess = -22;
	const int ESaveToDB = -23;

	const int TOK = 0;
	const int TNOTOK = 1;

	/*************************************************************************************/
	/* State codes */

	const short StateLocked =			0x0001;
	const short StateMotion =			0x0002;
	const short StateIgnition =			0x0011;
	const short StateEngine =			0x0012;
	const short StateDoors =			0x0020;

	const short StateCalcGround =		0x1001;
	const short StateCalcIgnition =		0x1002;
	const short StateCalcEngine =		0x1003;
	const short StateCalcMotion =		0x1004;
	const short StateCalcMoto =			0x1100;

	const short StateErrorPark =		-0x0001;
	const short StateErrorDist =		-0x0002;
	const short StateErrorMoto =		-0x0100;
	const short StateErrorCsmp =		-0x0200;

	/*************************************************************************************/
	/* Event codes */

	const short EventNone =				0;
	const short EventTimer =			0x0010;
	const short EventTimerPark =		0x0011;
	const short EventMove =				0x0021;
	const short EventPark =				0x0022;
	const short EventPowerLow =			0x0031;
	const short EventBatteryLow =		0x0032;

	const short EventCalcMove =			0x1021;
	const short EventCalcPark =			0x1022;
	const short EventCalcMotoOn =		0x1100;
	const short EventCalcMotoOff =		0x1200;
	const short EventCalcLevelIn =		0x1300;
	const short EventCalcLevelOut =		0x1400;

	const short EventCalcFuelIn =		EventCalcLevelIn;
	const short EventCalcFuelOut =		EventCalcLevelOut;

	/*************************************************************************************/
	/* Parameter codes */

	const short ParamNone =				0;
	const short ParamInteger =			800;
	const short ParamBinary =			900;
	const short ParamFloat =			1000;

	// Text parameters
	const short ParamName =				100;
	const short ParamCaption =			101;
	const short ParamAvtoNo =			102;
	const short ParamRegNo =			103;
	const short ParamDescription =		200;
	const short ParamPassword =			300;
	const short ParamSalt =				301;
	const short ParamFilter =			302;

	// Text comma separated
	//const short ParamDataEvents =		800;			
	const short ParamParentCode =		801;

	// Binary parameters
	const short ParamVersion =			900;
	const short ParamConfig =			901;
	const short ParamState =			910;
	const short ParamStatus =			911;

	// Common float parameters
	const short ParamModified =			1000;
	const short ParamTime =				1010;
	const short ParamInterval =			1011;
	const short ParamTimeout =			1012;

	// System parameters
	const short ParamDataPower =		1021;
	const short ParamDataBattery =		1022;
	const short ParamDataGSM =			1023;
	const short ParamDataGPS =			1024;

	// GPS parameters
	const short ParamDataTime =			1100;
	const short ParamDataLat =			1101;
	const short ParamDataLon =			1102;
	const short ParamDataAlt =			1103;
	const short ParamDataHead =			1104;
	const short ParamDataSpeed =		1105;

	// Drive parameters
	const short ParamDataOdo =			1201;
	//const short ParamDataDist =		1202;
	const short ParamDataDriver =		1221;
	const short ParamDataZone =			1222;

	// Binary sensor parameters
	const short ParamDataInput =		1901;
	const short ParamDataOutput =		1902;

	// Other sensor parameter groups
	const short ParamDataAnalog =		2000;
	const short ParamDataDigit =		2010;
	const short ParamDataMoto =			2020;
	const short ParamDataParams =		2100;
	const short ParamDataCan =			2200;
	const short ParamDataTire =			2300;
	const short ParamDataTempr =		2400;
	
	// CAN parameters (ParamDataCan group)
	const short ParamCanOdometer =		2203;
	const short ParamCanFuelTotal =		2204;
	const short ParamCanMotoTotal =		2205;
	const short ParamCanFuelLevel =		2206;

	// Calculation parameters
	const short ParamCalcParkCor =		3010;
	const short ParamCalcParkTime =		3011;
	const short ParamCalcMoveTime =		3012;
	const short ParamCalcDist =			3013;

	const short ParamCalcMoto =			3100;
	const short ParamCalcMotoTime =		3200;
	const short ParamCalcConsump =		3300;
	const short ParamCalcLevel =		3400;
	const short ParamCalcLevelAvg =		3500;
	const short ParamCalcLevelChange =	3600;
	const short ParamCalcEval =			3700;

	const short ParamCalcFuelLevel =	ParamCalcLevel;
	const short ParamCalcFuelLevelAvg =	ParamCalcLevelAvg;
	const short ParamCalcRefuel =		ParamCalcLevelChange;

	// Statistics parameters
	const short ParamStatTime =			9000;
	const short ParamStatPosCount =		9001;
	const short ParamStatFuelChange =	9030;
	const short ParamStatFuelLevel1 =	9031;
	const short ParamStatFuelLevel2 =	9032;
	const short ParamStatFuelIn =		9033;
	const short ParamStatFuelOut =		9034;

	/*************************************************************************************/
	/* Notification codes */

	const int NotifyObjChange =		10;
	const int NotifyObjEvent =		11;
	const int NotifyObjRecalc =		12;

	/*************************************************************************************/
	/* Common declarations */

	sequence<byte> ByteArray;
	sequence<int> IntArray;
	sequence<string> StringArray;
	sequence<short> ParamArray;
	dictionary<short, string> Properties;
	dictionary<short, float> PropertiesFloat;
	dictionary<short, int> PropertiesInt;

    struct Bitwise
	{
		int value;
		int mask;
	};

    exception Error
	{
		int code;
        string message;
    };

	/*************************************************************************************/
	/* Item declarations */

    struct Item
	{
		int code;					// Item code (cotext specific)
		double time;				// Timestamp data store or last modification (unix time in secconds)
		ParamArray states;			// Array of state codes
		ParamArray events;			// Array of event codes
		Properties props;			// String properties
		PropertiesFloat params;		// Float properties
	};

	sequence<Item> ItemArray;

	/*************************************************************************************/

	struct Notification
	{
		int what;			// Notification codes
		int code;			// Context code (object/item) 
		double time;		// Notification time
		string sender;		// Sender identity
		string message;		// Optional context message
		double param1;		// Optional context param1
		double param2;		// Optional context param2
	};

	interface Monitor
	{
		void notify(Notification n);
	};

	/*************************************************************************************/
	
	interface Logger
	{
		void send(string id, string msg, int level);
	};
	
	/*************************************************************************************/
	
	dictionary<string, string> Config;
	
	interface ConfigMonitor
	{
		void notify(string id, Config cfg);
	};
	
	interface ConfigStorage
	{
		Config get(string id);
		void set(string id, Config cfg);
	};
	
	/*************************************************************************************/
};
#endif
