﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

using System.Threading;
//
using System.Collections.ObjectModel;
using System.Collections.Specialized;

using TachoSource;

using System.IO;

//Визуальная часть приложения общается только с диспетчером.
namespace TachoSourceApp
{
    public sealed class Applctn
    {

        // ------------ паттерн Singleton --------------------------
        private static readonly Object s_lock = new Object();
        private static Applctn instance = null;

        private Applctn()
        {
        }

        public static Applctn Instance
        {
            get
            {
                if (instance != null) return instance;
                Monitor.Enter(s_lock);
                Applctn temp = new Applctn();
                Interlocked.Exchange(ref instance, temp);
                Monitor.Exit(s_lock);
                return instance;
            }
        }
        // ------------ паттерн Singleton --------------------------
        // ---------------------------------------------------------

        private Disp _disp = null;
        private string _fileConfIce = "";
        private string _userPathApp = "";
        private string _nameApp = "";
        //версия
        public static readonly string VERSION = "1.3.4";

        ListSendingItem _listFileSending = new ListSendingItem();

        //отправка файлов из указанной папки
        public enum CodeOfSendingFiles
        {
            // ошибка
            Error = -1,

            Complete = 0,
            Ok = 1,
            Progress = 3,
        };
        public delegate void ResSendingFiles(string msg, int param);

        public event ResSendingFiles OnResSendingFiles;

        //изменение состояния тип: код состояния и сообщение
        public delegate void CardChangeState(string card, uint code, string msg);
        public delegate void CardReadProgress(int progressPercentage, string msg);
        public delegate void CardReadIdentity(string msg);
        public delegate void CardCompleted(object result, Exception error, bool cancelled);

        public event CardChangeState OnCardChangeState;
        public event CardReadProgress OnCardReadProgress;
        public event CardReadIdentity OnCardReadIdentity;
        public event CardCompleted OnCardCompleted;

        public delegate void SSFMessage(string msg);
        public delegate void SSFProgress(string file, int progressPercentage, object userState);
        public delegate void SSFCompleted(string file, ServiceSendingFile.ResultInfo result, Exception error, bool cancelled);

        public event SSFMessage OnSSFMessage;
        public event SSFProgress OnSSFProgress;
        public event SSFCompleted OnSSFCompleted;

        //сообытия о провеке пина и авторизации
        public delegate void FResAuthState(string msg, int code);
        public event FResAuthState OnAuthState;
        //список авторизованных пользователей
        public UserProfiles Profiles = new UserProfiles();
        //очередь для проверки состояния загруженных ДДД-файлов
        public QueueLoadedState MngrLoadedFile = new QueueLoadedState();
        public Disp Dispetcher
        {
            get
            {
                InitDispetcher();
                return _disp;
            }
        }
        public void Init(string fileConfIce, string userPathApp, string nameApp)
        {
            _fileConfIce = fileConfIce;
            _userPathApp = userPathApp;
            _nameApp = nameApp;
            InitDispetcher();
            MngrLoadedFile.Start();
            GlobalLog.trace("App", String.Format("Init. Version: {0}", Applctn.VERSION));
        }
        public void PCSCStart()
        {
            GlobalLog.trace("App", "PCSC start");
            if (_disp != null)
            {
                _disp.PCSCStart();
            }
        }

        public void Done()
        {
            GlobalLog.trace("App", "Done");
            MngrLoadedFile.destroy();
            if (_disp != null) 
            {
                _disp.Done();
                _disp = null;
            }
            TachoSource.Disp.DisposeCommunicator();
            string filePath = String.Format("{0}{1}", _userPathApp, ListSendingItem.FILE_NAME);
            ListSendingItem.Save(filePath, _listFileSending.toArray());
        }

        public int CheckPin(string pinStr, out string resStr)
        {
            resStr = null;
            return _disp != null ? _disp.CheckPin(CurrentCardReader, pinStr, out resStr) : -1;
        }
        public void ResetPin()
        {
            if (_disp != null)
            {
                _disp.ResetPin();
            }
        }

        public bool AutoStateReaderStore = false;
        public void SetAutoStateReader(bool value)
        {
            AutoStateReaderStore = value;
            if (_disp != null)
            {
                _disp.SetAutoStateReader(value);
            }
        }
        //
        public string[] GetListCardReaders()
        {
            if (_disp != null)
            {
                _disp.UpdateReaders();
                return _disp.ReaderNames;
            }
            else
            {
                return null;
            }
        }

        public void ReadCard()
        {
            int res = (_disp != null) ? _disp.DataSmartCardToDDD(CurrentCardReader, CardType) : -1;
            if (res < 0)
            {
                if (OnCardCompleted != null) OnCardCompleted("Карта не может быть прочитана", null, false);
            }
        }

        public void AddFileOnSend(string filePath)
        {
            InitDispetcher();
            if (_disp != null)
            {
                //добавляем файл на отправку
                _listFileSending.Add(filePath, SendingStatus.DEFAULT);

                _disp.AddSendingFile(filePath);
            }
            else 
            {
                if (OnSSFMessage != null) OnSSFMessage("Объект для соединения не создан!");
            }
        }

        //методы касающиеся взаимодействия с визуальной частью окна
        public string CurrentCardReader {get; set;}
        //private string _preCardReader;
        public void WndUpdateCardReaders()
        {
            TachoSourceApp.MainWindow mWnd = (TachoSourceApp.MainWindow)TachoSourceApp.App.Current.MainWindow; ///Application.Current.Windows;
            mWnd.UpdateReaders();
        }


        public void SendingAllFiles(string dirName, uint param)
        {
            //добавляем на отправку все файлы из указанной директории
            //param: 1 - удалить файлы после отправки
            if (!Directory.Exists(dirName))
            {
                if (OnSSFMessage != null) OnSSFMessage(String.Format("Folder`s path isn`t exist: ", dirName));
                return;
            }
            uint cnt = 0;
            string msg = "OK";
            try
            {
                if (OnSSFMessage != null) OnSSFMessage("Get list file ...");
                var files = Directory.GetFiles(dirName, "*.*", (param & SendingStatus.SUB_FOLDER) == 0 ? SearchOption.TopDirectoryOnly : SearchOption.AllDirectories).Where(str => str.EndsWith(".ddd") || str.EndsWith(".DDD"));
                foreach (string path in files)
                {
                    if (OnSSFMessage != null) OnSSFMessage(String.Format("Prepare file: {0}", path));
                    _listFileSending.Add(path, param);
                    cnt++;
                }
                CheckAndAddFileForSend();
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            if (OnResSendingFiles != null) OnResSendingFiles(String.Format("Total files ({0}): {1}", cnt, msg), (cnt<1) ?(int)CodeOfSendingFiles.Complete :(int)CodeOfSendingFiles.Ok);
        }
        //Private----------------
        private void InitDispetcher()
        {
            if (_disp != null) return;
            Disp.InitCommunicator(_fileConfIce, _userPathApp, _nameApp);
            _disp = new Disp();
            _disp.Init(this.AutoStateReaderStore);
            //SynchronizationContext sync = null;//SynchronizationContext.Current;
            _disp.OnCardChangeState += (sender, info) =>
                {
                    if (OnCardChangeState != null) OnCardChangeState(info.readerName, info.code, info.message);
                };
            _disp.OnCardReadProgress += (progressPercentage, userState) =>
                {
                    PCSCWorker.PCSC_WorkerInfo usrStage = (PCSCWorker.PCSC_WorkerInfo)userState;
                    if (usrStage.code == PCSCWorker.PCSC_WorkerInfo.INITIALIZE)
                    {
                        if (OnCardReadIdentity != null) OnCardReadIdentity(userState.ToString());
                    }
                    else
                    {
                        if (OnCardReadProgress != null) OnCardReadProgress(progressPercentage, userState.ToString());
                    }
                };
            _disp.OnCardCompleted += (result, error, cancelled) =>
                {
                    if (OnCardCompleted != null) OnCardCompleted(result, error, cancelled);
                };


            _disp.OnMessage += (category, msg) =>
                {
                    if (OnSSFMessage != null) 
                    {
                        //if (category == MediatorSendFile.GR_MAIN) 
                        OnSSFMessage(msg);
                    } 
                };
            _disp.OnSendProgress +=(file, progressPercentage, userState) =>
                {
                    if (OnSSFProgress != null) OnSSFProgress(file, progressPercentage, userState);
                };
            _disp.OnSendCompleted += (file, result, error, cancelled) =>
                {
                    if (OnSSFCompleted != null) OnSSFCompleted(file, result, error, cancelled);
                    //если файл отправлен успешно - проверяем нужно ли удалить этот файл
                    CheckFileForRemove(file, result, error, cancelled);
                    CheckAndAddFileForSend();
                };
            _disp.OnAuthState += DoAuthState;

            GlobalLog.communicator = Disp.communicator;
            var _loggerI = GlobalLog.Instance;
        }

        private void CheckAndAddFileForSend()
        {
            SendingFileInfo itm = _listFileSending.GetItem4Sending();
            if (itm == null)
            {
                int code = this.CountWaitingTask() == 0 ? (int)CodeOfSendingFiles.Complete : (int)CodeOfSendingFiles.Progress;
                if (OnResSendingFiles != null) OnResSendingFiles(String.Format("Files for sending not found! Count - {0}", _listFileSending.ItemCount), code);
            }
            else {
                //если длинна очереди превышает лимит, подождём
                if (MngrLoadedFile.CountWaitState <= QueueLoadedState.QUEUE_LENGHT_LIMIT)
                {
                    itm.ChangeParam(SendingStatus.ADD_SENDING, SendingStatus.IT_SENDING);
                    if (OnResSendingFiles != null) OnResSendingFiles(String.Format("Files add sending: {0}", itm.path), (int)CodeOfSendingFiles.Progress);
                    AddFileOnSend(itm.path);
                }
                else {
                    if (OnResSendingFiles != null) OnResSendingFiles(String.Format("Limit of requet waiting count; Limit - {0}; Waiting - {1}!", QueueLoadedState.QUEUE_LENGHT_LIMIT,  _listFileSending.Count4Send), (int)CodeOfSendingFiles.Progress);
                }
            }
        }
        private void RemoveFileOnDisk(string file, uint param)
        {
            //удаляем на диске
            string resMsg = "";
            int resCode = (int)CodeOfSendingFiles.Progress;
            if ((param & SendingStatus.DEL_AFTER_SENDING) != 0)
            {
                FileInfo fileInf = new FileInfo(file);
                if (fileInf.Exists)
                {
                    try
                    {
                        fileInf.Delete();
                        resMsg = String.Format("File was removed:{0}", file);
                    }
                    catch (Exception ex)
                    {
                        resMsg = String.Format("File delete- file:{0}; error:{1}", file, ex.Message);
                        resCode = (int)CodeOfSendingFiles.Error;
                    }
                }
            }
            if (OnResSendingFiles != null) OnResSendingFiles(resMsg, resCode);
        }
        delegate string getString();
        private void DoFileLoadedState (string fileName, CallbackEntry itemLoaded, ItemLoadFileState info)
        {
            getString getTimeStamp = delegate()
            {
                int valSec = itemLoaded != null
                    ? itemLoaded.IsSucceded() ? 0 : itemLoaded.Wait2TimeStamp() 
                    : 0;
                string waitSec = valSec > 0 ? CallbackEntry.Seconds2HH_MM_SS(valSec) : "";
                return valSec > 0 ? String.Format("{0} + {1}", DateTime.Now.ToString(), waitSec) : "";
            };
            string timeStamp = getTimeStamp();
            //результат проверки состояния загруженного файла в сервис
            SendingFileInfo sfiItm = _listFileSending.Get(fileName);
            Console.WriteLine(String.Format("Number`s state info item: {0}; next {1}", MngrLoadedFile.CountWaitState, timeStamp));
            int code = this.CountWaitingTask() == 0 ? (int)CodeOfSendingFiles.Complete : (int)CodeOfSendingFiles.Progress;
            if (OnResSendingFiles != null) OnResSendingFiles(String.Format("LoadedState: {1}; fileName - {0}", fileName, (info == null) ? "undefined" : info.ToString()), code);

            if (OnSSFMessage != null) OnSSFMessage(String.Format("Состояние: {0}; {1}", (info == null) ? "undefined" : info.ToString(), timeStamp));
            if (sfiItm != null)
            {
                sfiItm.ChangeParam(SendingStatus.IT_STATUS_WAITING, SendingStatus.STATUS_WAS_HAVE);
                sfiItm.state = info;
                //удаляем в списке
                if (itemLoaded.IsSucceded())
                {
                    _listFileSending.Remove(fileName);
                    RemoveFileOnDisk(fileName, sfiItm.param);
                }
            }
            CheckAndAddFileForSend();

        }
        public int CountWaitingTask()
        {
            int cntState = MngrLoadedFile.CountWaitState;
            int cnt4Send = _listFileSending.Count4Send;
            int cntSending = _listFileSending.CountSending;
            string msg = String.Format("Number`s task: , wait send {0}, sending {1}, wait state {2}", cnt4Send, cntSending, cntState);
            Console.WriteLine(msg);
            GlobalLog.print(msg);
            return cntState + cnt4Send + cntSending;
        }
        private void CheckFileForRemove(string fileName, ServiceSendingFile.ResultInfo result, Exception error, bool cancelled)
        {
            if (_disp == null) return;
            SendingFileInfo sfiItm = _listFileSending.Get(fileName);

            if ((error != null) || (cancelled) || (result != null && result.code < 0))
            {
                int code = (result != null && result.code < 0)
                            ? result.code
                            : cancelled
                                    ? (int)CodeOfSendingFiles.Ok
                                    : (int)CodeOfSendingFiles.Complete;
                String msg = String.Format("cancelled - {0}; code - {1}; error - {2}", cancelled, (result == null ? 0 : result.code), (error == null ? "null" : error.ToString()));
                if (code < 0)
                {
                    if (((code == Monitoring.ESession.value) || (code == Monitoring.EAuthorize.value)) && !_disp.authInfoRes.IsLimitTries())
                    {
                        //сбрасываем сессию
                        _disp.ResetSession();
                    }
                    code = this.CountWaitingTask() == 0 ? (int)CodeOfSendingFiles.Complete : (int)CodeOfSendingFiles.Ok;
                }
                    // && authAttemptCount < 3)
                if (OnResSendingFiles != null) OnResSendingFiles(String.Format("Result sending file ({0}): {1}", fileName, msg), code);
                String msg2 =  error == null
                        ? (result == null) ? "ошибка" : String.Format("ошибка {0}", ServiceSendingFile.ErrorMsgPerCode(result.code)) 
                        : error.ToString();
                if (OnSSFMessage != null) OnSSFMessage(String.Format("Результат: {0}", msg2));
                if (sfiItm != null) 
                {
                    sfiItm.ChangeParam(0, SendingStatus.WITH_ERROR);
                }
                return;
            }
            if (sfiItm == null)
            {
                if (OnResSendingFiles != null) OnResSendingFiles(String.Format("File not found: {0}", fileName), (int)CodeOfSendingFiles.Ok);
                return;
            }


            sfiItm.ChangeParam((SendingStatus.IT_SENDING | SendingStatus.ADD_SENDING), SendingStatus.IT_SENDED);

            if (result == null || result.stInfo == null)
            {
                if (OnResSendingFiles != null) OnResSendingFiles(String.Format("Don`t get result for file : {0}", fileName), (int)CodeOfSendingFiles.Ok);
            }
            else
            {
                if (OnSSFMessage != null) OnSSFMessage("Запрос состояния");
                sfiItm.ChangeParam(0, SendingStatus.IT_STATUS_WAITING);
                MngrLoadedFile.AddItem(_disp, DoFileLoadedState, result.stInfo.fileName, fileName);
            }
            
        }

        public ObservableCollection<MediatorSendFile.File4Sending> SendingFile
        {
            get
            {
                return (_disp != null) ?  _disp.SendingFile : null;
            }
        }

        public bool AutoSendReadingFromCard
        {
            get
            {
                return (_disp != null) ? _disp.AutoSendReadingFromCard: false;
            }
            set 
            {
                if (_disp != null) _disp.AutoSendReadingFromCard = value;
            }
        }
        public byte CardType = TachoSource.SmartCardRec.TYPE_WITH_GOST;

        public ConnectionInfo AuthInfoIn
        {
            get
            {
                return (_disp != null) ? _disp.authInfoIn : null;
            }
        }
        public SessionInfo AuthInfoRes
        {
            get
            {
                return (_disp != null) ? _disp.authInfoRes : null;
            }
        }

        public string StorageSendingPath
        {
            get
            {
                return (_disp != null) ? _disp.StorageSendingPath : "";
            }
            set
            {
                if(_disp != null) _disp.StorageSendingPath = value;
            }
        }

        
        public void InitSSF(System.Windows.Controls.ListView lvFiles)
        {
            if (_disp != null)
            {
                lvFiles.ItemsSource = _disp.SendingFile;
            }
        }

        //-----------Auth---------------------------------------------------
        public void AuthInfo()
        {
            if (_disp != null)
            {
                _disp.AuthInfo();
            }
        }

        public void CreateSessionLast()
        {
            if(_disp == null) return;
            if (_disp.authInfoIn.User.Length == 0) 
            {
                if (OnAuthState != null) OnAuthState("No data for create session", -3);
                return;
            }
            _disp.SessionCreateLast();
        }
        public void CreateSession(string user, string passwrd, string server)
        {
            if (_disp == null) return;
            _disp.SessionCreate(user, passwrd, server);
        }
        private void DoAuthState(int opertn, object result)
        {
            if (opertn == (int)AuthOperation.Init)// инициализация
            {
                //результат авторизации
                bool isInit = (bool)result;
                string msg = String.Format("Initialization is {0}", isInit ? "OK" : "Error");
                if (OnAuthState != null) OnAuthState(msg, isInit ? 1 : -1);
            }
            else if (opertn == (int)AuthOperation.Info)
            {
                string strInfo = (string)result;
                string msg = String.Format("Auth service: {0}", strInfo.Length > 0 ? strInfo: "not find");
                if (OnAuthState != null) OnAuthState(msg, strInfo.Length > 0 ? 2 : -2);
            }
            else if (opertn == (int)AuthOperation.SessionCreate)// создание сессии
            {
                SessionInfo authInfo = (SessionInfo)result;

                string msg = String.Format("Get token: {0}", authInfo.Token.Length > 0 ? "OK" : "Session has`t been created");
                if (OnAuthState != null) OnAuthState(msg, authInfo.Token.Length > 0 ? 0 : -3);
            }
        }

        //------------------------------------------------------------------
    }
}

//2016-10-06
//VERSION = "1.3.4";
//1. Init без таймера. С ним почему-то Init вообще не вызывался. (модуль MainWindow)


//2016-10-05
//VERSION = "1.3.3";
//1. Обновление pssc.dll

//2016-09-30
//VERSION = "1.3.2";
//1. Чтение карт ЕСТР - убрал несуществующие разделы и связанные с подписью. А также в выделении каталога в CommandApdu изменил IsoCase.Case4Shor на IsoCase.Case3Shor(иначе выдавал ошибку 67 00)

//2016-09-28
//VERSION = "1.3.1";
//1. добавлен параметр autoState(AutoStateReader), т.е. автоматическое определение состояния картридера

//2016-08-29
//VERSION = "1.3.0";
//1. Чтение карт ЕСТР - Выбор элементарного файла (EF) по идентификатору файла

//2016-08-16
//VERSION = "1.2.0";
//1. Подправлен код запроса состояния закачки
//2. Время ожидания увеличивается по степенному закону и вЫвод вермени ожидания

//2016-07-18
//VERSION = "1.1.1";
//1.Добавлен параметр QueueLoadedState.QUEUE_LENGHT_LIMIT - ограничение на длинну очереди ожидающих проверку
//2.Зависание программы из-за wait  в QueueLoadedState

//2016-07-16
//VERSION = "1.1.0";
//1. Добавлена проверка состояния загрузок после закачки на сервис
//2. Добавлено сохранение в файл списка загрузок после окончания программы (то что не загрузилось)
