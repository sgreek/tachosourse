﻿using System;
using PCSC;
using System.IO;
//sleep
using System.Threading;


using TachoSourceApp;

namespace TachoSource
{

    public class ProgramConsole
    {
        private static void menu()
        {
            Console.Write(
                "usage:\n" +
                "r: start working with card \n" +
                "f: end working with card\n" +
                "g: get data from card and save to file\n" +
                "s: send last file to file service\n" +
                "p: check pin\n" +
                "c: read all cards\n");
            Console.WriteLine(
                "x: exit\n" +
                "?: help\n");
        }

        private string fileConfIce_ = "";
        private string userPathApp_ = "";
        private string nameApp_ = "";
        //
        private string user_ = "";
        private string passwrd_ = "";
        private string server_ = "";

        private string pathWithDDDFiles4Send_ = "";  //папка для отправки всех содержащихся в ней ddd-файлов
        private uint paramSend_ = 0;                 //параметры отправки ddd-файлов 1-удалить после отправки

        public ProgramConsole(string fileConfIce, string userPathApp, string nameApp)
        {
            fileConfIce_ = fileConfIce;
            userPathApp_ = userPathApp;
            nameApp_ = nameApp;

        }
        public void SetParam(string pathWithDDDFiles, uint prmSend, string user, string passwrd, string server)
        {
            pathWithDDDFiles4Send_ = pathWithDDDFiles;
            paramSend_ = prmSend;
            user_ = user;
            passwrd_ = passwrd;
            server_ = server;
        }

        public int main(string[] args)
        {
            Console.WriteLine(String.Format("Programm`s version: {0}", Applctn.VERSION));
            bool willBeSending = pathWithDDDFiles4Send_.Length > 0;
            if (!Directory.Exists(pathWithDDDFiles4Send_))
            {
                Console.WriteLine("WARNING!. Path with ddd-files not found!!!");
                return 1;
            }

            Console.WriteLine("Init main objects");
            Applctn applctn = Applctn.Instance;
            applctn.Init(fileConfIce_, userPathApp_, nameApp_);

            Disp _dispPtr = Applctn.Instance.Dispetcher;

            if (willBeSending)
            {
                bool isDone = false;
                applctn.OnResSendingFiles += (msg, param) =>
                {
                    if (msg.Length > 0) Console.WriteLine(msg);
                    if ((param == (int)Applctn.CodeOfSendingFiles.Complete) || (param < 0))
                    {
                        isDone = true;
                        Console.WriteLine("Application state  - DONE!!!");
                    }
                    else {
                        int cnt = applctn.CountWaitingTask();
                    }
                };

                if (user_ == " ") {
                    Console.WriteLine("WARNING!. Path with ddd-files not found!!!");
                    return 1;
                }
                //заданы данные для авторизации
                if ((user_ != null) && (user_.Length > 0))
                {
                    applctn.OnAuthState += (msg, code) =>
                        {
                            Console.WriteLine(String.Format("AuthState - code: {0}; msg: {1}", code, msg));
                            if (code == 0)
                            {
                                //получен результат авторизации
                                applctn.SendingAllFiles(pathWithDDDFiles4Send_, paramSend_);
                            }
                            else if (code == 2)
                            {
                                //сервис авторизации обнаружен
                                applctn.CreateSession(user_, passwrd_, server_);
                            }
                        };

                    //проверим, есть ли сервис авторизации
                    applctn.AuthInfo();
                }
                else
                {
                    applctn.SendingAllFiles(pathWithDDDFiles4Send_, paramSend_);
                }


                while (!isDone) 
                { 
                    Thread.Sleep(1000); 
                }
                applctn.Done();

            }
            else
            {
                applctn.OnCardChangeState += (card, code, msg) =>
                {
                    Console.WriteLine(String.Format("Change state, card:{0},  code:{1},  msg:{2}", card, code, msg));
                };

                try
                { 
                    DialogCommand();
                }
                finally
                {
                    applctn.Done();
                }
            }
            return 0;
        }

        private void DialogCommand()
        {
                Disp _dispPtr = Applctn.Instance.Dispetcher;
                if (_dispPtr == null)
                {
                    Console.WriteLine("Dispetcher is NULL!!!");
                    return;
                }

                menu();
               
                string line = null;
                do
                {
                    Console.Write("Which command you prefer? ");
                    line = Console.ReadLine();
                    Console.WriteLine("");
                    Console.WriteLine("----------------");
                    if (line == "r")//run
                    {
                        _dispPtr.PCSCStart();
                    }
                    else if (line == "f")//finish
                    {
                        _dispPtr.PCSCStop();
                    }
                    else if (line == "g")
                    {
                        _dispPtr.DataSmartCardToDDD("", TachoSource.SmartCardRec.TYPE_WITH_GOST);
                    }
                    else if (line == "s")//send
                    {
                        string filePath = _dispPtr.StorageSendingPath + "20151027_160450АндрейМихайлович_Минаков_RUD0000020031700.ddd";
                        _dispPtr.AddSendingFile(filePath);
                    }
                    else if (line == "p")//pin
                    {
                        Console.Write("Pin:");
                        string pinStr = Console.ReadLine();
                        string resStr;
                        int val = _dispPtr.CheckPin("", pinStr, out resStr);
                        Console.WriteLine(resStr);
                        Console.WriteLine("");
                    }
                    else if (line == "c")
                    {
                        _dispPtr.UpdateReaders();
                    }
                    else if (line.Equals("?"))
                    {
                        menu();
                    }
                    else
                    {
                        Console.WriteLine("unknown command `" + line + "'");
                        menu();
                    }


                }
                while (!line.Equals("x"));

        }
    }
}