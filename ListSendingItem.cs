﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

using TachoSource;

namespace TachoSourceApp
{
    class SendingStatus
    {
        //----------------------------------------------------------
        public static readonly uint DEFAULT = 0x00;
        //удалять после отправки
        public static readonly uint DEL_AFTER_SENDING = 0x01;
        //считывать ddd-файлы в подпапках
        public static readonly uint SUB_FOLDER = 0x02;
        //файл добавлен на отправку
        public static readonly uint ADD_SENDING = 0x04;
        //файл в отправке
        public static readonly uint IT_SENDING = 0x08;
        //файл отправлен с ошибкой
        public static readonly uint WITH_ERROR = 0x10;
        //файл отправлен 
        public static readonly uint IT_SENDED = 0x20;
        //файл отправлен - ожидание статуса парсинга
        public static readonly uint IT_STATUS_WAITING = 0x40;
        //статус парсинга получен
        public static readonly uint STATUS_WAS_HAVE = 0x80;
    }
    [DataContract]
    public class SendingFileInfo
    {
        [DataMember]
        public string path { get; set; }
        [DataMember]
        public uint param { get; set; }

        [DataMember]
        public ItemLoadFileState state = null;
        public void ChangeParam(uint bitRemove, uint bitAdd)
        {
            this.param &= ~bitRemove;
            this.param |= bitAdd;
        }
    }
    class ListSendingItem
    {
        public static readonly string FILE_NAME = "listSending.json";
        private List<SendingFileInfo> list_ = new List<SendingFileInfo>();

        public SendingFileInfo GetItem4Sending()
        {
            //ищем в списке, файл для отправки
            int indxFnd = list_.FindIndex(s => ((s.param & SendingStatus.ADD_SENDING) != 0)
                            && ((s.param & SendingStatus.WITH_ERROR) == 0));
//                           && ((s.param & SendingStatus.IT_SENDING) == 0)
//                         );
            return indxFnd < 0 ? null : list_[indxFnd];
        }
        public int ItemCount 
        {
            get 
            {
                return list_.Count;
            }
        }

        public int Count4Send
        {
            get
            {
                List<SendingFileInfo> listSend = list_.FindAll(s => ((s.param & SendingStatus.ADD_SENDING) != 0)
                            && ((s.param & SendingStatus.WITH_ERROR) == 0));
 //                           && ((s.param & SendingStatus.IT_SENDING) == 0)
//                         );
                return listSend != null ? listSend.Count : 0;
            }
        }
        public int CountSending
        {
            get
            {
                List<SendingFileInfo> listSending = list_.FindAll(s => ((s.param & SendingStatus.IT_SENDING) != 0)
                            && ((s.param & SendingStatus.WITH_ERROR) == 0));
                //                           && ((s.param & SendingStatus.IT_SENDING) == 0)
                //                         );
                int cntSending = listSending != null ? listSending.Count : 0;
                return cntSending;
            }
        }

        public int GetIndex(string path)
        {
            return list_.FindIndex(s => s.path.StartsWith(path));
        }
        public SendingFileInfo Get(string path)
        {
            int indxFnd = GetIndex(path);
            return indxFnd < 0 ? null : list_[indxFnd];
        }
        public SendingFileInfo Add(string path, uint param)
        {
            int indxFnd = this.GetIndex(path);
            if (indxFnd < 0)
            {
                SendingFileInfo itm = new SendingFileInfo() { path = path, param = (param | SendingStatus.ADD_SENDING) };
                list_.Add(itm);
                return itm;
            }
            else{
                return list_[indxFnd];
            }

        }
        public int Remove(string path)
        {
            int indxFnd = GetIndex(path);
            if (indxFnd > -1)
            {
                list_.RemoveAt(indxFnd);
            }
            return indxFnd;

        }

        public SendingFileInfo[] toArray()
        {
            return list_.ToArray();
        }
        public static void Save(string filePath, SendingFileInfo[] list)
        {

            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(SendingFileInfo[]));

            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                try
                {
                    jsonFormatter.WriteObject(fs, list);
                }
                catch (Exception ex)
                {
                    GlobalLog.error(String.Format("SendingList to file: {0}", ex.Message));
                }

            }
        }

        public static SendingFileInfo[] Load(string filePath)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(SendingFileInfo[]));

            SendingFileInfo[] list = null;

            FileInfo fileInf = null;
            try
            {
                fileInf = new FileInfo(filePath);
            }
            catch (Exception)
            {
            }
            if ((fileInf != null) && (fileInf.Exists))
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    try
                    {
                        list = (SendingFileInfo[])jsonFormatter.ReadObject(fs);
                    }
                    catch (Exception ex)
                    {
                        GlobalLog.error(String.Format("SendingList from file: {0}", ex.Message));
                    }
                }
            }
            return list;
        }
    }
}
