// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
using System.Threading;

static class ConstantsLogger
{
    public const byte TRACE = 2;
    public const byte WARNING = 1;
    public const byte ERROR = 0;
}


public sealed class GlobalLog
{
    public static Ice.Communicator communicator;

    public static byte logLevel = ConstantsLogger.ERROR;
    public static string prefix;
    public static bool toConsole;

    private static readonly object s_lock = new object();
    private static  LoggerI _loggerI = null;

    private static LoggerI CreateLog()
    {
        //
        LoggerI temp = new LoggerI();
        string[] arg = new string[] {  };
        logLevel = (byte)communicator.getProperties().getPropertyAsInt("Logger.Level");
        temp.init(communicator.getLogger(), logLevel, prefix);

        return temp;
    }
    private GlobalLog()
    {
    }

    public static LoggerI Instance
    {
        get
        {
            if (_loggerI != null) return _loggerI;
            Monitor.Enter(s_lock);
            LoggerI temp = CreateLog();
            Interlocked.Exchange(ref _loggerI, temp);
            Monitor.Exit(s_lock);
            return _loggerI;
        }
    }

    public static void print(string message)
    {
        if (logLevel < ConstantsLogger.TRACE) return;

        if (_loggerI == null)
        {
            System.Console.WriteLine("PRINT: " + message);
        }
        else
        {
            if (toConsole) System.Console.WriteLine("PRINT: " + message);
            Monitor.Enter(s_lock);
            _loggerI.print(message);
            Monitor.Exit(s_lock);
        }

    }

    public static void trace(string category, string message)
    {
        if (logLevel < ConstantsLogger.TRACE) return;

        if (_loggerI == null)
        {
            System.Console.WriteLine("TRACE(" + category + "): " + message);
        }
        else
        {
            if (toConsole) System.Console.WriteLine("TRACE(" + category + "): " + message);
            Monitor.Enter(s_lock);
            _loggerI.trace(category, message);
            Monitor.Exit(s_lock);
        }
    }

    public static void warning(string message)
    {
        if (logLevel < ConstantsLogger.WARNING) return;

        if (_loggerI == null)
        {
            System.Console.WriteLine("WARNING: " + message);
        }
        else
        {
            if (toConsole) System.Console.WriteLine("WARNING: " + message);
            Monitor.Enter(s_lock);
            _loggerI.warning(message);
            Monitor.Exit(s_lock);
        }
    }

    public static void error(string message)
    {
        #if DEBUG
                System.Windows.MessageBox.Show(string.Format("global log msg: - {0}", message));
        #endif

        if (logLevel < ConstantsLogger.ERROR) return;

        if (_loggerI == null)
        {
            System.Console.WriteLine("ERROR: " + message);
        }
        else
        {
            if (toConsole) System.Console.WriteLine("ERROR: " + message);
            Monitor.Enter(s_lock);
            _loggerI.error(message);
            Monitor.Exit(s_lock);
        }
    }

}


public class LoggerI : Ice.Logger
{
    private Ice.Logger _logger = null;
    private byte _logLevel = ConstantsLogger.ERROR;
    private string _prefix;
    public bool toConsole;
    public void init(Ice.Logger logger, byte logLevel, string prefix)
    {
        _logLevel = logLevel;
        _logger = logger;
        _prefix = prefix;
    }

    public string getPrefix()
    {
        return _prefix;
    }

    public void print(string message)
    {
        if (_logLevel < ConstantsLogger.TRACE) return;

        if (_logger == null)
        {
            System.Console.WriteLine("PRINT: " + message);
        }
        else
        {
            _logger.print(message);
        }

    }
    
    public void trace(string category, string message)
    {
        if (_logLevel < ConstantsLogger.TRACE) return;

        if (_logger == null)
        {
            System.Console.WriteLine("TRACE(" + category + "): " + message);
        }
        else
        {
            _logger.trace(category, message);
        }
    }
    
    public void warning(string message)
    {
        if (_logLevel < ConstantsLogger.WARNING) return;

        if (_logger == null)
        {
            System.Console.WriteLine("WARNING: " + message);
        }
        else {
            _logger.warning(message);
        }
    }
    
    public void error(string message)
    {
        if (_logLevel < ConstantsLogger.ERROR) return;

        if (_logger == null)
        {
            System.Console.WriteLine("ERROR: " + message);
        }
        else
        {
            _logger.error(message);
        }
    }

    public Ice.Logger cloneWithPrefix(string prefix)
    {
        LoggerI ptr = new LoggerI();
        ptr.init(_logger, _logLevel, prefix);
        return ptr;
    }
}
