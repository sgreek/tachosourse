﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//timer
using System.Threading;
using System.Threading.Tasks;

namespace TachoSourceApp
{
    /// <summary>
    /// Interaction logic for PinWindow.xaml
    /// </summary>
    public partial class PinWindow : Window
    {
        public PinWindow()
        {
            InitializeComponent();
        }

        public event Applctn.FResAuthState  OnResState;
        delegate void FunctnNotPrm();

        static public void WindowClose(Window elmntWnd)
        {
            //прогресс в элементе у считывателя
            FunctnNotPrm fClose = delegate()
            {
                elmntWnd.Close();
            };

            elmntWnd.Dispatcher.Invoke(fClose);
        }
        public void SetState(string msg, int code)
        {
            if (OnResState != null) OnResState(msg, code);
            this.tStates.Text = msg;
            if (code == 0) { 
                //закрываем через одну секунду
                Task.Factory.StartNew(() => {
                    Thread.Sleep(1000);
                })
                    .ContinueWith(x =>
                    {
                        WindowClose(this);
                    }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }
        private void bAction_Click(object sender, RoutedEventArgs e)
        {
            if (this.bCancel == sender)
            {
                this.Close();
                return;
            }
            if (this.bOk == sender)
            {
                if (this.ePin.Text.Length <= 0)
                {
                    this.tStates.Text= "Укажите ПИН!";
                    return;
                }
                if (this.ePin.Text.Length > 7)
                {
                   this.tStates.Text="Ошибка. ПИН должен содержать не более 8 символов!";
                    return;
                }
                Applctn applctn = Applctn.Instance;
                string resStr;
                int checkPin = applctn.CheckPin(this.ePin.Text, out resStr);
                this.SetState(resStr, checkPin);
            }
        }

        private void ePin_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                bAction_Click(this.bOk, null);
                return;
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                bAction_Click(this.bCancel, null);
                return;
            }

        }

    }
}
